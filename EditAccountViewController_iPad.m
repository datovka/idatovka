/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  EditAccountViewController_iPad.m
//  ISDS
//
//  Created by Petr Hruška on 4/18/11.
//

#import "EditAccountViewController_iPad.h"
#import "Accounts.h"

@implementation EditAccountViewController_iPad

- (void)shouldPopViewController {
	[super shouldPopViewController];
	
	[self updateAccount];
	
	if (self.wantsDelete) {
		Accounts* accounts = [Accounts accounts];
		[self.account cleanCache];
		[accounts removeAccount: self.account];
	} else {
		BOOL hasOwnerInfo = [self.account loadOwnerInfo];
		if (!hasOwnerInfo && self.account.active)
			if (![self getOwnerInfo]) return;
	}
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString*)databoxTitle {
	return [self.account boxName];
}

- (IBAction)aliasEditingChanged:(id)sender
{
    UITextView* textView = (UITextView*)sender;
    self.account.alias = textView.text;
}

@end
