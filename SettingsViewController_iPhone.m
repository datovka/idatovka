/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SettingsViewController_iPhone.m
//  ISDS
//
//  Created by Petr Hruška on 2/28/11.
//

#import "SettingsViewController_iPhone.h"
#import "AddAccountViewController_iPhone.h"
#import "EditAccountViewController_iPhone.h"


@implementation SettingsViewController_iPhone

- (void)cleanCache {
	
	UIActionSheet* actionSheet = [[UIActionSheet alloc] 
								  initWithTitle:nil
								  delegate:self
								  cancelButtonTitle: NSLocalizedString(@"Cancel", @"Cancel button title")
								  destructiveButtonTitle: NSLocalizedString(@"Remove", @"Remove button title")
								  otherButtonTitles:nil
								  ];
	
	[actionSheet showInView:self.view];
    [actionSheet release];
}


- (AccountViewController_Shared*)addAccountViewController
{
    return [[[AddAccountViewController_iPhone alloc] initWithNibName:@"AccountViewController~iPhone" bundle:nil] autorelease];
}


- (AccountViewController_Shared*)editAccountViewController:(ISDSAccount*)account
{

    EditAccountViewController_iPhone* vc = [[EditAccountViewController_iPhone alloc] initWithNibName:@"AccountViewController~iPhone" bundle:nil];
    vc.account = account;
    return [vc autorelease];
}


@end
