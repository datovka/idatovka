/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  MessageViewController.m
//  ISDS
//
//  Created by Petr Hruška on 5/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MessageViewController.h"
#import "ContentViewController.h"
#import "Attachment.h"
#import "MsgDownloadCell.h"
#import "NetworkUpdater.h"
#import "ReceivedMessageDownload.h"
#import "SignedSentMessageDownload.h"
#import "ISDSAppDelegate_Shared.h"

#define FILES_OFFSET 5


@implementation MessageViewController

@synthesize senderCell;
@synthesize senderName;
@synthesize senderAddress;

@synthesize recipientCell;
@synthesize recipientName;
@synthesize recipientAddress;

@synthesize subjectCell;
@synthesize subject;

@synthesize dateCell;
@synthesize date;

@synthesize filesCell;
@synthesize progressCell;
@synthesize noFilesCell;
@synthesize downloadFailed;

@synthesize msg;
@synthesize account;
@synthesize fileIcons;

@synthesize resendButton;

@synthesize observedClasses;


- (void)loadFileIcons {
	
	self.fileIcons = nil;
	
	NSString *rootPath = [[NSBundle mainBundle ] bundlePath];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"FileIcons.plist"];
	
	NSData *data = [NSData dataWithContentsOfFile:plistPath];
	if (!data) return;
	NSError* error = nil;
	
	self.fileIcons = [ NSPropertyListSerialization
					  propertyListWithData: data
					  options:NSPropertyListImmutable
					  format: NULL
					  error: &error];
	
	if (error) [error release];
	
}

- (UIImage*)loadFileIcon:(NSString*)attachmentName {
	
	if (self.fileIcons == nil) return nil;
	
	NSString *fileExtension = [[attachmentName pathExtension] lowercaseString];
	NSString* imageName = [self.fileIcons objectForKey: fileExtension];
	if (imageName == nil) imageName = [self.fileIcons objectForKey: @"*"];
	if (imageName == nil) return nil;
	
	NSString *rootPath = [[NSBundle mainBundle ] bundlePath];
    NSString *fileName = [rootPath stringByAppendingPathComponent:imageName];
	UIImage* image = [UIImage imageWithContentsOfFile:fileName];

	return image;
}

- (void)loadProgressCell {
	
	if (!self.progressCell)
		[[NSBundle mainBundle] loadNibNamed:@"MsgDownloadCell" owner:self options:nil];
}


- (void)loadNoFilesCell {

	if (!self.noFilesCell)
		[[NSBundle mainBundle] loadNibNamed:@"NoFilesCell" owner:self options:nil];
	self.noFilesCell.caption.text = NSLocalizedString(@"Error while downloading message", @"Error while downloading message");
}


- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.navigationItem.rightBarButtonItem = self.resendButton;

	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] 
										 initWithTitle:NSLocalizedString(@"Back", @"Back bar button title")
											 style:UIBarButtonItemStylePlain target:nil action:nil];	
	[self loadFileIcons];
}


- (void)viewWillAppear:(BOOL)animated
{    
	[self.account addMsgObserver:self];
	self.downloadFailed = FALSE;
	RequestManager* rm = [RequestManager requestManager];
	self.observedClasses = [NSArray arrayWithObjects:
						[ReceivedMessageDownload observationClass], [SignedSentMessageDownload observationClass], nil];
	[rm addObserver:self forClasses:self.observedClasses];

	NetworkUpdater* networkUpdater = [NetworkUpdater networkUpdater];
	[self.msg loadAttachments];
	if (!self.msg.attachments) {
        [networkUpdater downloadFullMessage:self.msg withAccount:self.account];
    }
}

- (void)viewWillDisappear:(BOOL)animated 
{
	RequestManager* rm = [RequestManager requestManager];
	[rm removeObserver:self forClasses:self.observedClasses];
	[self.account removeMsgObserver:self];
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {

    ISDSAppDelegate_Shared* appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
	if ([appDelegate iPad])
        return YES;
    else
        return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	NSUInteger count = [self.msg attachmentCount];
    return FILES_OFFSET + count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView fileCellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"FileCell";
	NSInteger index = indexPath.row - FILES_OFFSET;
	if (index < 0 || index >= [self.msg attachmentCount]) return nil;
	
	Attachment* file = [self.msg.attachments attachmentAtIndex: index];
	
	FileCell *cell = (FileCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		
		cell = [[[NSBundle mainBundle] loadNibNamed:@"FileCell" owner:nil options:nil] lastObject];
        
        //TODO setup background
	}
    cell.fileName = file.name;
    cell.fileSize = [file.data length];
    cell.fileIcon = [self loadFileIcon:file.name];
	
	return cell;
							  
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
		
	switch (indexPath.row) {
		case 0:
			self.senderName.text = self.msg.sender;
			//			self.senderAddress.text = self.msg.senderAddress ? self.msg.senderAddress : @"";
			return self.senderCell;
		case 1:
			self.recipientName.text = self.msg.recipient;
			//			self.recipientAddress.text = self.msg.recipientAddress ? self.msg.recipientAddress : @"";
			return self.recipientCell;
		case 2:
			self.subject.text = self.msg.annotation;
			return self.subjectCell;
		case 3:
			self.date.text = [ self.msg friendlyDateTime ];
			return self.dateCell;
		case 4:
			if ([self.msg attachmentCount] > 0)
				return self.filesCell;
			else {
				if (self.downloadFailed) {
					[self loadNoFilesCell];
					return self.noFilesCell;
				} else {
					[self loadProgressCell];
					return self.progressCell;
				}
			}
		default:
			return [self tableView:tableView fileCellForRowAtIndexPath:indexPath];
	}
	
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSInteger index = indexPath.row - FILES_OFFSET;
	if (index < 0 || index >= [self.msg attachmentCount]) return;
	
	Attachment *file = [msg.attachments attachmentAtIndex: index];
	ContentViewController *contentViewController = [[ContentViewController alloc] initWithNibName:@"ContentViewController" bundle:nil];
	contentViewController.file = file;
	[self.navigationController pushViewController:contentViewController animated:YES];
	[contentViewController release];
}

- (void)dealloc {
	[senderCell release];
	[senderName release];
	[senderAddress release];
	
	[recipientCell release];
	[recipientName release];
	[recipientAddress release];
	
	[subjectCell release];
	[subject release];
	
	[dateCell release];
	[date release];
	
	[filesCell release];
	[progressCell release];
	
	[msg release];
	[account release];
	[fileIcons release];
	
	[observedClasses release];
	
    [super dealloc];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat x = 0;
	switch (indexPath.row) {
		case 0:
		case 1:
			return 38;
		case 2:
			x = self.subjectCell.frame.size.height;
            return x;
		case 3:
			return 38;
		case 4:
			if ([self.msg attachmentCount] > 0)
				return 38;
			else
				return self.downloadFailed ? 44 : 124;
		default:
			/* FileCell */
			return 72;
	}
	
	return -1;
}

- (void) showCantSendMailAlert {
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Can't send email", @"Can't send email")
													message: NSLocalizedString(@"The device is not configured for sending email", @"The device is not configured for sending email")
												   delegate: self
										  cancelButtonTitle: NSLocalizedString(@"OK", @"OK")
										  otherButtonTitles: nil ];
	[alert show];
	[alert release];
	
}

- (void) setupMailContent:(MFMailComposeViewController*)composer {

	NSString* bodyText = [NSString stringWithFormat:
						  NSLocalizedString(@"Mail Body", @"Mail Body template"),
						  self.msg.sender,
						  self.msg.recipient,
						  [self.msg friendlyDateTime]
						  ];
	[composer setMessageBody:bodyText isHTML:NO];
	[composer setSubject:self.msg.annotation];

	// attachments
	const NSUInteger count = [self.msg attachmentCount];
	for (NSUInteger index = 0; index < count; index++) {
		Attachment* a = [self.msg.attachments attachmentAtIndex:index];
		[composer addAttachmentData:a.data mimeType:[a mimeType] fileName:a.name];
	}
	
}

- (IBAction)resend:(id)sender {
	
	if (![MFMailComposeViewController canSendMail]) {
		[self showCantSendMailAlert];
		return;
	}
	
	MFMailComposeViewController* composer = [[MFMailComposeViewController alloc] init];
	composer.mailComposeDelegate = self;
	[self setupMailContent: composer];
	[self presentModalViewController:composer animated:YES];
	[composer release];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
		  didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	
	[self dismissModalViewControllerAnimated:YES];
}

- (void)messageChanged:(ISDSMessage*)msg withAccount:(ISDSAccount*)account {
	[((UITableView*) self.view) reloadData];
}

- (void)statusUpdate:(enum RMStatus)status forTask:(ISDSTask*)task {
    if (status == RMFailed) {
        self.downloadFailed = TRUE;
        [((UITableView*) self.view) reloadData];
    }
    // other status values will be notified by model observation
}

- (void)progressUpdate:(float)progress estimate:(NSTimeInterval)estimate elapsed:(NSTimeInterval)elapsed {
	self.progressCell.progressBar.progress = progress;
}

@end

