/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  PinViewController_iPad.m
//  ISDS
//
//  Created by Petr Hruška on 4/11/11.
//

#import "PinViewController_iPad.h"


@implementation PinViewController_iPad


- (void)adjustPositionsForOrientation:(UIInterfaceOrientation)orientation
{
    const CGFloat a = 256;
    CGPoint center;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        center = CGPointMake([[UIScreen mainScreen] applicationFrame].size.width/2, a);
        self.icon.center = center;
        self.pinEditViewController.view.frame = self.pinEditView.frame;
    } else if (UIInterfaceOrientationIsLandscape(orientation)) {
        center = CGPointMake(a, a/2 + 70);
        self.icon.center = center;
        self.pinEditViewController.view.frame = CGRectMake(
            320, 
            30,
            self.pinEditView.frame.size.width,
            self.pinEditView.frame.size.height
        );
    }    
}

- (void)viewDidLoad
{
    [self.view addSubview:self.icon];
    [self adjustPositionsForOrientation:UIInterfaceOrientationPortrait];
    [super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration {
    
    [self adjustPositionsForOrientation:orientation];
}


@end
