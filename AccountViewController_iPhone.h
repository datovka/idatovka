/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AccountViewController_iPhone.h
//  ISDS
//
//  Created by Petr Hruška on 4/18/11.
//

#import <Foundation/Foundation.h>
#import "AccountViewController_Shared.h"


@interface AccountViewController_iPhone : AccountViewController_Shared

@property (retain, nonatomic) IBOutlet UILabel *activeAccountTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *authenticationTypeLabel;
@property (retain, nonatomic) IBOutlet UILabel *userIdTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *passwordTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *accountAliasLabel;

@property (nonatomic, strong) IBOutlet UITableViewCell* removeAccountCell;

@end
