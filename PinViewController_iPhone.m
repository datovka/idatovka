/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  PinViewController_iPhone.m
//  ISDS
//
//  Created by Petr Hruška on 4/11/11.
//

#import "PinViewController_iPhone.h"

#define PORTRAIT_PIN_EDIT_VIEW_ORIGIN_Y   116

@implementation PinViewController_iPhone

- (void)adjustPositionsForOrientation:(UIInterfaceOrientation)orientation
{
    CGRect frame = self.pinEditView.frame;

    if (UIInterfaceOrientationIsPortrait(orientation)) {
        self.icon.hidden = NO;
    } else if (UIInterfaceOrientationIsLandscape(orientation)) {
        self.icon.hidden = YES;
        frame.origin.y = 0;
    }
    
    self.pinEditViewController.view.frame = frame;
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration {
    
    [self adjustPositionsForOrientation:orientation];
}


@end
