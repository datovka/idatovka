/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SettingsViewController.m
//  ISDS
//
//  Created by Petr Hruška on 4/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AccountViewController_Shared.h"
#import "ISDSAppDelegate_Shared.h"
#import "RequestManager.h"
#import "NetworkUpdater.h"
#import "Accounts.h"
#import "AuthPickerViewController.h"

#define CLEAR_INDEX 1

#define BUTTON_CONFIG	0
#define BUTTON_MESSAGES	1


@implementation AccountViewController_Shared

@synthesize activeCell;

@synthesize databoxCell;
@synthesize databoxTextField;

@synthesize passwordCell;
@synthesize passwordTextField;

@synthesize aliasSettingsCell;

@synthesize authTypeCell=_authTypeCell;
@synthesize authTypeLabel=_authTypeLabel;

@synthesize account;

@synthesize wantsDelete;

@synthesize doneButton;

@synthesize deleteActionSheet;
@synthesize existsActionSheet;
@synthesize emptyActionSheet;

@synthesize existingAccount;

@synthesize tableView=_tableView;
@synthesize removeButton;
@synthesize parent=_parent;

- (void)dealloc {
	
	[databoxCell release];
	[databoxTextField release];
	
	[passwordCell release];
	[passwordTextField release];
    
	[aliasSettingsCell release];
    
	[activeCell release];
    [_authTypeCell release];
    [_authTypeLabel release];
	
	[account release];
	
	[doneButton release];
	
	[deleteActionSheet release];
	[existsActionSheet release];
	[emptyActionSheet release];
	
	[_tableView release];
	[removeButton release];
	
    [super dealloc];
}

- (void)viewDidUnload {
    
	self.tableView = nil;
	self.removeButton = nil;
    
    self.activeCell = nil;
    self.authTypeCell = nil;
    self.authTypeLabel = nil;
}


- (void)deleteButtonPressed
{	
	UIActionSheet* actionSheet;
	
	ISDSAppDelegate_Shared* appDelegate = [ISDSAppDelegate_Shared theDelegate];
	if ([appDelegate iPad]) {
		actionSheet = [[UIActionSheet alloc] 
					   initWithTitle: NSLocalizedString(@"Remove Account", @"Remove Account title")
					   delegate:self
					   cancelButtonTitle: nil
					   destructiveButtonTitle: NSLocalizedString(@"Remove", @"Remove button title")
					   otherButtonTitles: nil
					   ];		
	} else {
		actionSheet = [[UIActionSheet alloc] 
						initWithTitle: nil
						delegate:self
						cancelButtonTitle: NSLocalizedString(@"Cancel", @"Cancel button title")
						destructiveButtonTitle: NSLocalizedString(@"Remove", @"Remove button title")
						otherButtonTitles: nil
						];		
	}
	self.deleteActionSheet = actionSheet;
	[actionSheet showInView:self.view];
	[actionSheet release];
}

- (IBAction)doneButtonPressed {
	
	BOOL testing = [self.databoxTextField.text hasPrefix:@"*"];
	NSString* databox;
	if (!testing)
		databox = self.databoxTextField.text;
	else
		databox = [self.databoxTextField.text substringFromIndex:1];
	
	if (![databox isEqualToString:self.account.databox] || testing != self.account.testing) { // databox name has changed
		
		Accounts* accounts = [Accounts accounts];
		ISDSAccount* a = [accounts accountById:databox andTesting:testing];
		if (a) {
			NSString* title = [NSString stringWithFormat:
							   NSLocalizedString(@"Account %@ already exists", @"Account XYZ already exists alert title"),
							   a.databox];
			UIActionSheet* actionSheet = [[UIActionSheet alloc] 
										  initWithTitle: title
										  delegate:self
										  cancelButtonTitle: NSLocalizedString(@"Cancel", @"Cancel button title")
										  destructiveButtonTitle: NSLocalizedString(@"Update", @"Update button title")
										  otherButtonTitles: nil
										  ];
			
			self.existsActionSheet = actionSheet;
			self.existingAccount = a;
			[actionSheet showInView:self.view];
			[actionSheet release];
			return;
		}
	}
	
	if ([self.databoxTextField.text isEqualToString:@""]) {		
		if (![self.passwordTextField.text isEqualToString:@""] ||
			([self.account alias] && ![[self.account alias] isEqualToString:@""])) {
		
			UIActionSheet* actionSheet = [[UIActionSheet alloc] 
										  initWithTitle: NSLocalizedString(@"Missing account ID", @"Missing account ID")
										  delegate:self
										  cancelButtonTitle: NSLocalizedString(@"Cancel", @"Cancel button title")
										  destructiveButtonTitle: NSLocalizedString(@"Remove", @"Remove button title")
										  otherButtonTitles: nil
										  ];
			
			self.emptyActionSheet = actionSheet;
			[actionSheet showInView:self.view];
			[actionSheet release];
			return;
		} else {
			self.wantsDelete = YES;
            [self shouldPopViewController];
		}
	}
    
    [self shouldPopViewController];
}

- (void)showErrorAlert:(NSString*)errorDescription {
	UIAlertView* alert = 
	[[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error while accessing databox name", @"Error while accessing databox name")
							   message: errorDescription
							  delegate: self
					 cancelButtonTitle: nil
					 otherButtonTitles: NSLocalizedString(@"Settings", @"Settings"), nil ];
	[alert show];
    [alert release];
}

// returns FALSE when pushed modal view controller
- (BOOL)getOwnerInfo {
	
	NetworkUpdater* updater = [NetworkUpdater networkUpdater];
	if (!updater) {
		[self showErrorAlert:NSLocalizedString(@"Not enough memory", @"Not enough memory")];
		return TRUE;
	}
    
    TokenViewController* tokenViewController = [updater tokenViewControllerForAccount:self.account];
    if (tokenViewController) {
        tokenViewController.delegate = self;
        tokenViewController.whereToPop = self.parent;
        tokenViewController.prompt = [NSString stringWithFormat:NSLocalizedString(@"iDatovka needs download an account name %@", nil), self.account.databox, nil];
        tokenViewController.displayContext = ProgressDisplayContextDefault;
        [self.navigationController pushViewController:tokenViewController animated:YES];
        return FALSE;
    } else {
        [updater updateOwnerInfoForAccount:self.account];
        return TRUE;
    }
}

- (void)updateAccount {
	
	self.account.password = self.passwordTextField.text;
	
	BOOL testing = [self.databoxTextField.text hasPrefix:@"*"];
	NSString* databox;
	if (!testing)
		databox = self.databoxTextField.text;
	else
		databox = [self.databoxTextField.text substringFromIndex:1];
	
	self.account.databox = databox;
	self.account.testing = testing;
	self.account.active = self.activeCell.on;
}

- (void)setupAuthTypeLabel
{
    switch (self.account.authType) {
        case ISDSAccountAuthPass:
            self.authTypeLabel.text = NSLocalizedString(@"Authorize with a password", nil);
            break;
        case ISDSAccountAuthHotp:
            self.authTypeLabel.text = NSLocalizedString(@"Authorize with a key", nil);
            break;
        case ISDSAccountAuthTotp:
            self.authTypeLabel.text = NSLocalizedString(@"Authorize with an sms code", nil);
            break;
    }
}


- (void)viewDidLoad {
	
	ISDSAppDelegate_Shared* appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
	if ([appDelegate iPad]) {
		UIScrollView* sv = (UIScrollView*)self.view;
		sv.scrollEnabled = NO;
	}
    [self setupAuthTypeLabel];
}

- (void)viewWillAppear:(BOOL)animated {

	if (self.account.testing)
		self.databoxTextField.text = [@"*" stringByAppendingString:self.account.databox];
	else
		self.databoxTextField.text = self.account.databox;
	
	self.passwordTextField.text = self.account.password;
    [self setupAuthTypeLabel];
    
	self.activeCell.on = self.account.active;
	self.wantsDelete = NO;
	
	self.title = [self databoxTitle];
	self.navigationItem.leftBarButtonItem = self.doneButton;
}

- (void)viewWillDisappear:(BOOL)animated {
	
	[self updateAccount];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
    return 0; // abstract
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case ACTIVE_INDEX:
            return self.activeCell;
        case DBOXNAME_INDEX:
            return self.databoxCell;
        case PASSWORD_INDEX:
            return self.passwordCell;
        case AUTOALIAS_INDEX:
            return self.aliasSettingsCell;
        case OTP_INDEX:
            return self.authTypeCell;
    }
    
	return nil;
}

- (void)pushAuthPickerViewController
{
    AuthPickerViewController* vc = [[AuthPickerViewController alloc] initWithNibName:@"AuthPickerViewController" bundle:nil];
    vc.account = self.account;
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];
}

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tv deselectRowAtIndexPath:indexPath animated:YES];

    switch (indexPath.row) {
        case ACTIVE_INDEX:
            self.account.active = !self.account.active;
            [self.activeCell setOn:self.account.active animated:YES];
        case OTP_INDEX:
            [self pushAuthPickerViewController];
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {	
	if (textField == self.databoxTextField)
		[self.passwordTextField becomeFirstResponder];
	else
		[textField resignFirstResponder];
	return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
	return NO;
}


#pragma mark -
#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

	if (actionSheet == self.deleteActionSheet) {
		
		self.deleteActionSheet = nil;
		if (buttonIndex == 0) {
			self.wantsDelete = YES;
            [self shouldPopViewController];
		}
	} else if (actionSheet == self.existsActionSheet) {
		
		self.existsActionSheet = nil;
		if (buttonIndex == 0) {
			[self mergeExisting];
            [self shouldPopViewController];
		}
	} else if (actionSheet == self.emptyActionSheet) {
		
		self.emptyActionSheet = nil;
		if (buttonIndex == 0) {
			self.wantsDelete = YES;
            [self shouldPopViewController];
		}
	}
}

#pragma mark - SwitchCellDelegate

- (void)switchCell:(SwitchCell*)cell switchedTo:(BOOL)value
{
    if (cell == self.activeCell) {
        
        self.account.active = value;
        
    }
}


- (void)mergeExisting {	
	BOOL testing = [self.databoxTextField.text hasPrefix:@"*"];
	NSString* databox;
	if (!testing)
		databox = self.databoxTextField.text;
	else
		databox = [self.databoxTextField.text substringFromIndex:1];
	
	self.wantsDelete = YES;
	self.existingAccount.databox = databox;
	self.existingAccount.password = self.passwordTextField.text;
	self.existingAccount.testing = testing;	
	self.existingAccount.active = self.activeCell.on;
}

- (NSString*)databoxTitle {
	return @"";
}

- (void)shouldPopViewController
{
}

#pragma mark - TokenViewControllerDelegate

- (void)tokenViewControllerDone:(TokenViewController*)tokenViewController
{
    [[NetworkUpdater networkUpdater] updateOwnerInfoForAccount:self.account];
}


@end

