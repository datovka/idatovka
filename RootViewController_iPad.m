/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  RootViewController-iPad.m
//  ISDS
//
//  Created by Petr Hruška on 2/17/11.
//

#import "RootViewController_iPad.h"
#import "Accounts.h"
#import "DataboxCell.h"
#import "ISDSAppDelegate_Shared.h"

@implementation RootViewController_iPad

@synthesize detailViewController;
@synthesize selectedRow;
@synthesize inSplitView=_inSplitView;

#pragma mark -
#pragma mark View lifecycle

- (void)showCurrentSelection {
	
	if (self.selectedRow) {
        
        [self.boxesTableView selectRowAtIndexPath:self.selectedRow animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)deselectRow {
	if (self.selectedRow) {
		[self.boxesTableView deselectRowAtIndexPath:self.selectedRow animated:NO];
        self.selectedRow = nil;
	}
}


- (void)setupInitialSelection {
	NSUInteger indexes[2];

	Accounts* accounts = [Accounts accounts];
	if ([accounts activeCount] > 0) {
		indexes[0] = 0;
		indexes[1] = 0;
		self.selectedRow = [NSIndexPath indexPathWithIndexes:indexes length:2];
		NSArray* activeAccounts = [accounts activeAccounts];
		ISDSAccount* activeAccount = [activeAccounts objectAtIndex:0];
		[self.detailViewController showInbox:activeAccount];
	} else {
		self.selectedRow = nil;
		[self.detailViewController showSettings];
	}
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self showCurrentSelection];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}


- (DataboxCell*)getDataboxCell:(UITableView *)tableView {
	DataboxCell* databoxCell = [super getDataboxCell:tableView];
	databoxCell.selectionStyle = UITableViewCellSelectionStyleGray;
	return databoxCell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	self.selectedRow = indexPath;
	
	NSUInteger accountIndex = indexPath.section;
	BOOL incoming = !indexPath.row;
	
	Accounts* accounts = [Accounts accounts];
	NSArray* activeAccounts = [accounts activeAccounts];
	ISDSAccount* account = (ISDSAccount*)[activeAccounts objectAtIndex: accountIndex];
	
	if (incoming)
		[self.detailViewController showInbox:account];
	else
		[self.detailViewController showOutbox:account];

	[self.detailViewController dismissPopover];
}


- (void)dealloc {
	[detailViewController release];
	[selectedRow release];
    [super dealloc];
}


- (IBAction)config:(id)sender {
	
	[self.detailViewController showSettings];
	[self.detailViewController dismissPopover];
    [self deselectRow];
}


- (void)createProgressToolbarItems {
    [super createProgressToolbarItems];
    if (self.inSplitView)
        [self.progressToolbar willShowInSplitView];
    else
        [self.progressToolbar willHideInSplitView];
}


- (void)willShowInSplitView
{
    self.inSplitView = TRUE;
    
    if (![ISDSAppDelegate_Shared ios7OrLater]) {
        self.lastUpdate.textColor = [UIColor darkGrayColor];
    }
    
    [self.progressToolbar willShowInSplitView];
}


- (void)willHideInSplitView
{
    self.inSplitView = FALSE;
    
    if (![ISDSAppDelegate_Shared ios7OrLater]) {
        self.lastUpdate.textColor = [UIColor whiteColor];
    }
    
    [self.progressToolbar willHideInSplitView];
}


#pragma mark -
#pragma mark MsgListObserver methods

- (void)listChanged:(MsgList*)list withAccount:(ISDSAccount*)account {
    [super listChanged:list withAccount:account];
    [self showCurrentSelection];
}

#pragma mark -
#pragma mark ISDSAccountObserver methods

- (void)account:(ISDSAccount*)account changedBecause:(ISDSAccountChangeReason)reasonFlags {

	[super account:account changedBecause:reasonFlags];
    
    // active account list should change only from settings
    // and it should be self.selectedRow = nil when we are in settings
    [self showCurrentSelection];
}


@end

