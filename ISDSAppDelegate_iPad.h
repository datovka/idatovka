/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSAppDelegate_iPad.h
//  ISDS
//
//  Created by Petr Hruška on 2/17/11.
//

#import "ISDSAppDelegate_Shared.h"
#import "RootViewController_iPad.h"
#import "DetailViewController.h"


@interface ISDSAppDelegate_iPad : ISDSAppDelegate_Shared {

}

@property (nonatomic, retain) IBOutlet UISplitViewController* splitViewController;
@property (nonatomic, retain) IBOutlet DetailViewController* detailViewController;

@end
