/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSAppDelegate_iPad.m
//  ISDS
//
//  Created by Petr Hruška on 2/17/11.
//

#import "ISDSAppDelegate_iPad.h"
#import "PinViewController_iPad.h"
#import "Accounts.h"

@implementation ISDSAppDelegate_iPad

@synthesize splitViewController=_splitViewController;
@synthesize detailViewController=_detailViewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
	
    self.window.rootViewController = self.splitViewController;
	[(RootViewController_iPad*)self.rootViewController setupInitialSelection];

    Accounts* accounts = [Accounts accounts];
	if ([accounts activeCount] > 0) {
        [self.splitViewController.view layoutIfNeeded];
        
        [self.detailViewController.popoverController presentPopoverFromBarButtonItem:self.detailViewController.button permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
        [self.splitViewController.view layoutIfNeeded];
    }
	
	[super applicationDidFinishLaunching:application];
}


- (BOOL)iPad {
	return TRUE;
}


- (NSString*)appendArchitecture:(NSString*)nibName {
	return [nibName stringByAppendingString:@"~iPad"];
}


- (void)initPinViewController
{
    self.pinViewController = [[PinViewController_iPad alloc] initWithNibName:@"PinViewController" bundle:nil];
    self.pinViewController.delegate = self;
}

- (void)dealloc
{
    [_splitViewController release];
    [_detailViewController release];
    [super dealloc];
}


@end
