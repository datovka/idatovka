/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  DetailView.m
//  ISDS
//
//  Created by Petr Hruška on 2/17/11.
//

#import "DetailViewController.h"
#import "InboxViewController.h"
#import "OutboxViewController.h"
#import "SettingsViewController_iPad.h"
#import "CustomNavigationController.h"
#import "RootViewController_iPad.h"

@implementation DetailViewController

@synthesize navigationController;
@synthesize navigationSubview;
@synthesize popoverController;
@synthesize button=_button;
@synthesize contentProvider=_contentProvider;
@synthesize rootViewController=_rootViewController;

- (void)announceButtonStatus 
{
    if (self.button) {
    
        if ([self.contentProvider respondsToSelector:@selector(showPopoverButton:)]) {
            [self.contentProvider performSelector:@selector(showPopoverButton:) withObject:self.button];
        }

    } else {
        
        if ([self.contentProvider respondsToSelector:@selector(hidePopoverButton)]) {
            [self.contentProvider performSelector:@selector(hidePopoverButton)];
        }
    }
}

- (void)viewDidUnload {
	[super viewDidUnload];
    self.popoverController = nil;
    self.button = nil;
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.navigationController viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
	[self.navigationController viewWillDisappear:animated];
	[super viewWillDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)removeBoxIfExists
{
	if (self.navigationController) {
        [self.navigationController viewWillDisappear:NO];
		[self.navigationController.view removeFromSuperview];
		self.navigationController = nil;
	}
}

- (void)showBox:(BoxViewController*)newBoxViewController {
    [self removeBoxIfExists];
	
    self.contentProvider = newBoxViewController;
	self.navigationController = [[UINavigationController alloc] initWithRootViewController:newBoxViewController];
	[self.navigationController release];    
	self.navigationController.view.frame = self.navigationSubview.frame;
    
	[self.navigationSubview addSubview:self.navigationController.view];
	[self.navigationController viewWillAppear:FALSE];

    [self announceButtonStatus];
}


- (void)showInbox:(ISDSAccount*)account {
	
	InboxViewController* inboxViewController = [[InboxViewController alloc] initWithNibName:@"BoxViewController" bundle:nil];
	inboxViewController.account = account;
	[self showBox:inboxViewController];
	[inboxViewController release];
}


- (void)showOutbox:(ISDSAccount*)account {
	
	OutboxViewController* outboxViewController = [[OutboxViewController alloc] initWithNibName:@"BoxViewController" bundle:nil];
	outboxViewController.account = account;
	[self showBox:outboxViewController];
	[outboxViewController release];
}


- (BOOL)popoverMode
{
    return self.popoverController != nil;
}

- (void)splitViewController: (UISplitViewController*)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem*)barButtonItem forPopoverController: (UIPopoverController*)pc {

	self.popoverController = pc;
    barButtonItem.title = NSLocalizedString(@"Accounts", @"Accounts view title");
    self.button = barButtonItem;
        
    [self.rootViewController willHideInSplitView];
    
    [self announceButtonStatus];
}


// Called when the view is shown again in the split view, invalidating the button and popover controller.
- (void)splitViewController: (UISplitViewController*)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem {

    barButtonItem.title = NSLocalizedString(@"Accounts", @"Accounts view title");
    self.button = nil;
    
    [self.rootViewController willShowInSplitView];
    
    [self announceButtonStatus];
    self.popoverController = nil;
}


- (void)showSettings {

    [self removeBoxIfExists];
	
	self.contentProvider = [[SettingsViewController_iPad alloc] initWithNibName:@"SettingsViewController" bundle:nil];
	self.navigationController = [[CustomNavigationController alloc] initWithRootViewController:self.contentProvider];
	[self.navigationController release];
	self.navigationController.view.frame = self.navigationSubview.frame;
	[self.navigationSubview addSubview:self.navigationController.view];
	[self.navigationController viewWillAppear:FALSE];
    [self announceButtonStatus];

}

- (void)dismissPopover {
	
	if (self.popoverController != nil) {
		[self.popoverController dismissPopoverAnimated:YES];
	}
}


- (void)dealloc {
	[navigationSubview release];
	[navigationController release];
	[popoverController release];
    [_button release];
    [_contentProvider release];
    [_rootViewController release];
    
    [super dealloc];
}


@end
