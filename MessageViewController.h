/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  MessageViewController.h
//  ISDS
//
//  Created by Petr Hruška on 5/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <MessageUI/MessageUI.h>

#import <UIKit/UIKit.h>
#import "ISDSMessage.h"
#import "FileCell.h"
#import "ISDSAccount.h"
#import "RequestManager.h"
#import "MsgDownloadCell.h"
#import "NoFilesCell.h"

@interface MessageViewController : UITableViewController<MFMailComposeViewControllerDelegate, MsgObserver, RequestManagerObserver> {

	UITableViewCell* senderCell;
	UILabel* senderName;
	UILabel* senderAddress;

	UITableViewCell* recipientCell;
	UILabel* recipientName;
	UILabel* recipientAddress;

	UITableViewCell* subjectCell;
	UILabel* subject;

	UITableViewCell* dateCell;
	UILabel* date;

	UITableViewCell* filesCell;
	MsgDownloadCell* progressCell;
	NoFilesCell* noFilesCell;
	BOOL downloadFailed;

	ISDSMessage* msg;
	ISDSAccount* account;
	
	NSDictionary* fileIcons;
	UIBarButtonItem* resendButton;
	
	NSArray* observedClasses;
}

@property (nonatomic, retain) IBOutlet UITableViewCell *senderCell;
@property (nonatomic, retain) IBOutlet UILabel *senderName;
@property (nonatomic, retain) IBOutlet UILabel *senderAddress;

@property (nonatomic, retain) IBOutlet UITableViewCell *recipientCell;
@property (nonatomic, retain) IBOutlet UILabel *recipientName;
@property (nonatomic, retain) IBOutlet UILabel *recipientAddress;

@property (nonatomic, retain) IBOutlet UITableViewCell *subjectCell;
@property (nonatomic, retain) IBOutlet UILabel *subject;

@property (nonatomic, retain) IBOutlet UITableViewCell *dateCell;
@property (nonatomic, retain) IBOutlet UILabel *date;

@property (nonatomic, retain) IBOutlet UITableViewCell *filesCell;
@property (nonatomic, retain) IBOutlet MsgDownloadCell *progressCell;
@property (nonatomic, retain) IBOutlet NoFilesCell *noFilesCell;
@property (nonatomic, assign) BOOL downloadFailed;

@property (nonatomic, retain) ISDSMessage* msg;
@property (nonatomic, retain) ISDSAccount* account;

@property (nonatomic, retain) IBOutlet NSDictionary *fileIcons;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *resendButton;
@property (nonatomic, retain) NSArray *observedClasses;

- (IBAction) resend:(id) sender;

// UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

@end
