/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AccountViewController_iPhone.m
//  ISDS
//
//  Created by Petr Hruška on 4/18/11.
//

#import "AccountViewController_iPhone.h"
#import "AliasSettingsViewController.h"
#import "LocalizedStrings.h"

#define REMOVE_ACCOUNT_CELL_INDEX   5

@implementation AccountViewController_iPhone

- (void)pushAliasSettingsController {
	AliasSettingsViewController* aliasSettingsViewController = [[AliasSettingsViewController alloc] initWithNibName:@"AliasSettingsViewController" bundle:nil];
	aliasSettingsViewController.account = account;
	[self.navigationController pushViewController:aliasSettingsViewController animated:YES];
	[aliasSettingsViewController release];
}


- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	
    switch (indexPath.row) {
        case AUTOALIAS_INDEX:
            [tv deselectRowAtIndexPath:indexPath animated:YES];
            [self pushAliasSettingsController];
            return;
    }
    
    [super tableView:tv didSelectRowAtIndexPath:indexPath];
}



- (void)dealloc
{
    [_activeAccountTitleLabel release];
    [_authenticationTypeLabel release];
    [_userIdTitleLabel release];
    [_passwordTitleLabel release];
    [_accountAliasLabel release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.activeAccountTitleLabel.text = SmLoc(LS_ACTIVE_ACCOUNT);
    self.authenticationTypeLabel.text = SmLoc(LS_AUTHENTICATION_TYPE);
    self.userIdTitleLabel.text = SmLoc(LS_USER_ID);
    self.passwordTitleLabel.text = SmLoc(LS_PASSWORD);
    self.accountAliasLabel.text = SmLoc(LS_ACCOUNT_ALIAS);
}

- (void)viewDidUnload {
    [self setActiveAccountTitleLabel:nil];
    [self setAuthenticationTypeLabel:nil];
    [self setUserIdTitleLabel:nil];
    [self setPasswordTitleLabel:nil];
    [self setAccountAliasLabel:nil];
    [super viewDidUnload];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return REMOVE_ACCOUNT_CELL_INDEX + 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == REMOVE_ACCOUNT_CELL_INDEX) {
        return self.removeAccountCell;
    } else {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

@end
