/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SettingsViewController_iPad.m
//  ISDS
//
//  Created by Petr Hruška on 2/28/11.
//

#import "SettingsViewController_iPad.h"
#import "AddAccountViewController_iPad.h"
#import "EditAccountViewController_iPad.h"

@implementation SettingsViewController_iPad

- (void)cleanCache {
	
	UIActionSheet* actionSheet = [[UIActionSheet alloc] 
								  initWithTitle:NSLocalizedString(@"Remove Cache", @"Remove cache button")
								  delegate:self
								  cancelButtonTitle: nil
								  destructiveButtonTitle: NSLocalizedString(@"Remove", @"Remove button title")
								  otherButtonTitles:nil
								  ];
	
	[actionSheet showInView:self.view];
    [actionSheet release];
}

- (void)createToolbar
{
    CGRect frame = CGRectMake(0, 0, self.view.frame.size.width - 20, 30);
    if (!self.progressToolbar)
		self.progressToolbar = [[[ProgressToolbar alloc] 
								 initWithText:NSLocalizedString(@"Determining account name", @"Determining account name")
                                 andFrame:frame
								 ] autorelease];
    [self.progressToolbar willShowInSplitView];
}

- (AccountViewController_Shared*)addAccountViewController
{
    return [[[AddAccountViewController_iPad alloc] initWithNibName:@"AccountViewController~iPad" bundle:nil] autorelease];
}


- (AccountViewController_Shared*)editAccountViewController:(ISDSAccount*)account
{
    
    EditAccountViewController_iPad* vc = [[EditAccountViewController_iPad alloc] initWithNibName:@"AccountViewController~iPad" bundle:nil];
    vc.account = account;
    return [vc autorelease];
}

#pragma mark - ContentProvider

- (void)showPopoverButton:(UIBarButtonItem *)button
{
    self.navigationItem.leftBarButtonItem = button;
}

- (void)hidePopoverButton
{
    self.navigationItem.leftBarButtonItem = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return YES;
}

@end
