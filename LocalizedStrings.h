//
//  LocalizedStrings.h
//  ISDS
//
//  Created by Petr on 2/4/13.
//  Copyright (c) 2013 CZ.NIC, z.s.p.o. All rights reserved.
//

#define SmLoc(x) NSLocalizedString(x, nil)

#define LS_ACCOUNT_ALIAS            @"account_alias"
#define LS_ACTIVE_ACCOUNT           @"active_account"
#define LS_AUTHENTICATION_TYPE      @"authentication_type"
#define LS_PASSWORD                 @"password"
#define LS_SEND_LOG                 @"send_log"
#define LS_USER_ID                  @"user_id"
