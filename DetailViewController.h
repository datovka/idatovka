/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  DetailView.h
//  ISDS
//
//  Created by Petr Hruška on 2/17/11.
//

#import <UIKit/UIKit.h>
#import "ISDSAccount.h"
#import "BoxViewController.h"

@class RootViewController_iPad;

@interface DetailViewController : UIViewController<UIPopoverControllerDelegate, UISplitViewControllerDelegate> {

	UINavigationController* navigationController;
	UIPopoverController* popoverController;
	
	UIView* navigationSubview;
}

@property (nonatomic, retain) UINavigationController* navigationController;
@property (nonatomic, retain) IBOutlet UIView* navigationSubview;
@property (nonatomic, retain) UIPopoverController* popoverController;
@property (nonatomic, retain) UIBarButtonItem* button;
@property (nonatomic, retain) UIViewController* contentProvider;
@property (nonatomic, retain) IBOutlet RootViewController_iPad* rootViewController;

- (void)showInbox:(ISDSAccount*)account;
- (void)showOutbox:(ISDSAccount*)account;

- (void)showSettings;
- (void)dismissPopover;
- (BOOL)popoverMode;

@end
