/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SettingsViewController.m
//  ISDS
//
//  Created by Petr Hruška on 7/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#define ACCOUNTS_SECTION            0
#define SETTINGS_SECTION            1


#define PIN_SETTINGS                0
#define AUTORELOAD                  1
#define DELETE_CACHE                2

#define SETTINGS_CELL_COUNT         3

#define DELETECACHE_BUTTON_INDEX    0

#import <MessageUI/MessageUI.h>


#import "SettingsViewController_Shared.h"
#import "ISDSDefaults.h"
#import "ActivatePINViewController.h"
#import "DeactivatePINViewController.h"
#import "GetOwnerInfoFromLogin.h"
#import "ProgressToolbar.h"
#import "Accounts.h"
#import "LocalizedStrings.h"

@interface SettingsViewController_Shared ()<MFMailComposeViewControllerDelegate>

@end


@implementation SettingsViewController_Shared

@synthesize autoUpdateCell;
@synthesize observedClasses;
@synthesize progressToolbar;
@synthesize pendingTasks;


- (void)deselectRow:(NSUInteger)row inSection:(NSUInteger)section {
	NSUInteger indexes[2];
	indexes[0] = section;
	indexes[1] = row;
	NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
	[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)cleanCache {
	// abstract method
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if (buttonIndex == DELETECACHE_BUTTON_INDEX) {
		Accounts* accounts = [Accounts accounts];
		[accounts cleanCache];
	}
	
	[self deselectRow:DELETE_CACHE inSection:SETTINGS_SECTION];
}


- (void)account:(ISDSAccount*)account changedBecause:(ISDSAccountChangeReason)reasonFlags {
	[((UITableView*) self.view) reloadData];
}


- (void)accountChanged:(ISDSAccount*)account {
}


- (void)registerTaskObserver {
	
	RequestManager *rm = [RequestManager requestManager];
	if (!self.observedClasses) {
		self.observedClasses = [NSArray arrayWithObject: [GetOwnerInfoFromLogin observationClass]];	
	}
	
	[rm addObserver:self forClasses:self.observedClasses];
}

- (void)unregisterTaskObserver {
	
	RequestManager *rm = [RequestManager requestManager];
	[rm removeObserver:self forClasses:self.observedClasses];
}


- (AccountViewController_Shared*)addAccountViewController
{
    return nil; // abstract method
}


- (AccountViewController_Shared*)editAccountViewController:(ISDSAccount*)account
{
    return nil; // abstract method
}



#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	self.title = NSLocalizedString(@"Application Settings", @"Application settings title");
	self.autoUpdateCell.delegate = self;
	
	ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
	self.autoUpdateCell.on = defaults.autoUpdate;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[((UITableView*) self.view) reloadData];
	Accounts* accounts = [Accounts accounts];
	[accounts saveAsNeeded];
	[accounts addAccountObserver:self];
	[self registerTaskObserver];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
	Accounts* accounts = [Accounts accounts];
	[self unregisterTaskObserver];
	[accounts removeAccountObserver:self];
}


#pragma mark RequestManagerObserver

- (void)createToolbar
{
	if (!self.progressToolbar)
		self.progressToolbar = [[[ProgressToolbar alloc] 
								 initWithText:NSLocalizedString(@"Determining account name", @"Determining account name")
								 ] autorelease];
}


- (void)showToolbar {
    [self createToolbar];
	if (self.toolbarItems != self.progressToolbar.items) {
		[self setToolbarItems:self.progressToolbar.items animated:YES];
		self.navigationController.toolbarHidden = NO;
	}
    CGRect frame = CGRectMake(0, 0, self.view.frame.size.width - 20, 30);
    [self.progressToolbar setFrame:frame];
}


- (void)hideToolbar {
	if (self.toolbarItems) {
		self.navigationController.toolbarHidden = YES;
		self.toolbarItems = nil;
	}
}


- (void)statusUpdate:(enum RMStatus)status forTask:(ISDSTask*)task {

	switch (status) {
        case RMStarted:
            break;
		case RMQueued:
			self.pendingTasks = self.pendingTasks + 1;
			break;
		case RMDone:
		case RMCancelled:
		case RMUnregistered:
        case RMFailed:
			self.pendingTasks = self.pendingTasks - 1;
			if (!self.pendingTasks) [self hideToolbar];
			break;
	}
}

- (void)progressUpdate:(float)progress estimate:(NSTimeInterval)estimate elapsed:(NSTimeInterval)elapsed {

//	NSLog(@"progressUpdate: %0.2f", progress);

	if (estimate > PROGRESSBAR_THRESHOLD || elapsed > PROGRESSBAR_THRESHOLD) [self showToolbar];
	self.progressToolbar.bar.progress = progress;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	switch (section) {
		case ACCOUNTS_SECTION:
			return [[Accounts accounts] count] + 1;
		case SETTINGS_SECTION:
			return SETTINGS_CELL_COUNT;
	}
	
	return 0;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	switch (section) {
		case ACCOUNTS_SECTION:
			return NSLocalizedString(@"Accounts", @"Accounts section label");
		case SETTINGS_SECTION:
			return NSLocalizedString(@"Options", @"Options section label");
	}
	
	return nil;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	ISDSAccount* account;
	Accounts* accounts = [Accounts accounts];
	ISDSDefaults* defaults;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	switch (indexPath.section) {
		case ACCOUNTS_SECTION:
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

			if (indexPath.row < [accounts count]) {
				account = [accounts accountAtIndex:indexPath.row];
				cell.textLabel.text = [account boxName];
			} else
				cell.textLabel.text = NSLocalizedString(@"Add Account", "Add account button");
			break;
		case SETTINGS_SECTION:
			switch (indexPath.row) {
				case PIN_SETTINGS:
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					defaults = [ISDSDefaults sharedDefaults];
					if (defaults.pin)
						cell.textLabel.text = NSLocalizedString(@"Disable PIN protection", @"Disable PIN protection menu item");
					else
						cell.textLabel.text = NSLocalizedString(@"Enable PIN protection", @"Enable PIN protection menu item");
					break;
				case AUTORELOAD:
					defaults = [ISDSDefaults sharedDefaults];
					self.autoUpdateCell.on = defaults.autoUpdate;
					cell = self.autoUpdateCell;

					break;
				case DELETE_CACHE:
					cell.accessoryType = UITableViewCellAccessoryNone;
					cell.textLabel.text = NSLocalizedString(@"Remove Cache", @"Remove cache button");
					break;
			}
			break;
	}
	
	return cell;
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:^{}];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	Accounts* accounts = [Accounts accounts];
    ISDSAccount* account;
	AccountViewController_Shared *accountViewController;
	UIViewController *pinViewController;

	ISDSDefaults* defaults;

	switch (indexPath.section) {
		case ACCOUNTS_SECTION:
			if (indexPath.row < [accounts count]) {

                account = [accounts accountAtIndex:indexPath.row];
				accountViewController = [self editAccountViewController:account];
				
			} else {
				accountViewController = [self addAccountViewController];
			}
            accountViewController.parent = self;
            [self.navigationController pushViewController:accountViewController animated:YES];
			break;
		case SETTINGS_SECTION:
			switch (indexPath.row) {
				case PIN_SETTINGS:
					defaults = [ISDSDefaults sharedDefaults];
					
					if (defaults.pin == nil)
						pinViewController = [[ActivatePinViewController alloc] initWithNibName:@"ActivatePinViewController" bundle:nil];
					else
						pinViewController = [[DeactivatePinViewController alloc] initWithNibName:@"DeactivatePinViewController" bundle:nil];
					
					[self.navigationController pushViewController:pinViewController animated:YES];
					[pinViewController release];
					break;
				case AUTORELOAD:
                    defaults = [ISDSDefaults sharedDefaults];
                    defaults.autoUpdate = !defaults.autoUpdate;
					[self.autoUpdateCell setOn:defaults.autoUpdate animated:YES];
                    
					break;
				case DELETE_CACHE:
					[self cleanCache];
					break;
			}
			break;
	}
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[autoUpdateCell release];
	[observedClasses release];
	[progressToolbar release];
    [super dealloc];
}

- (void)switchCell:(SwitchCell*)cell switchedTo:(BOOL)value {
		
	ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
	defaults.autoUpdate = self.autoUpdateCell.on;
	
}



@end

