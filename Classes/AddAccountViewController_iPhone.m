/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AddAccountViewController_iPhone.m
//  ISDS
//
//  Created by Petr Hruška on 7/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AddAccountViewController_iPhone.h"
#import "Accounts.h"


@implementation AddAccountViewController_iPhone

- (void)viewDidLoad {
    [super viewDidLoad];

	self.account = [[ISDSAccount alloc] init];
	[self.account release];
}

- (void)shouldPopViewController {
	
	[super shouldPopViewController];

	[self updateAccount];
		
	if (!self.wantsDelete) {
		Accounts* accounts = [Accounts accounts];
		[accounts addAccount: self.account];
		if (self.account.active) {
            if (![self getOwnerInfo]) return;
        }
	}
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString*)databoxTitle {
	
	if (self.account) {
		NSString* name = [self.account boxName];
		if (![name isEqualToString:@""]) return [self.account boxName];
	}
	
	return NSLocalizedString(@"New Account", @"New account title");
}

@end
