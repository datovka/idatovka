/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSObject.h
//  ISDS
//
//  Created by Petr Hruška on 8/18/10.
//

/*

 Ucelem ISDSObject je poskytnout metody pro urceni adresare a jmena souboru
 s daty a implementaci nekterych spolecnych casti kodu pro ukladani a 
 nacitani z disku. Zaroven dava urcity ramec pro memory management.
 
 Objekt muze byt ve trech stavech:
 
 1. Neexistuje, nadrazena struktura ma misto objektu [NSNull null]. Priklad
 jsou prilohy zpravy, o jejichz stazeni zatim nikdo nepokousel nebo obecneji
 tuto prilohu jeste nikdo nepotreboval. Objekt muze existovat na disku z 
 predchoziho spusteni. Druhym duvodem pro existenci takovych (ne)objektu
 muze byt jejich likvidace pri nedostatku pameti.
 
 2. self != nil, Objekt ma platna data.
 
 3. self = nil, Tento stav signalizuje, ze objekt nema cenu zkouset nacitat 
 z disku, narozdil od neexistujiciho objektu.
 
 Zivotni cyklus objektu
 
 Typicky objekt ma dva konstruktory: pro konstrukci pomoci dat ze site a 
 pomoci dat z disku. Konstruktor konstruujici z disku vola metodu updateFromFile,
 objekt se tedy pokusi nacist svoje data z disku hned pri svem vzniku. Je 
 dulezite, aby pri volani updateFromFile predka byly nastaveny objekty pro 
 odvozeni jmena souboru. Podle vysledku nacitani objekt bud vrati nil nebo sam sebe.
 
 Neni-li objekt nil, zustava validni az do konce sveho zivota. V pripade,
 ze se nepodari aktualizovat data, zustavaji v platnosti stara data.
 
 Je-li objekt nil, muze se stat validnim volanim konstruktoru typu 
 createFromNetwork, ktera zaroven zapise nova data na disk volanim writeToFile.
 Pokud writeToFile selze, nepovazuje se to za chybu.
 
 Konzistence dat
 
 V pripade, ze se (napriklad kvuli nedostatku pameti) nepodari aktualizovat 
 datove struktury objektu podle dat stazenych ze site, nesmi se stav objektu
 zmenit. V takovem pripade se nemeni ani stav potomku. Pritom rodic neodpovida
 za uspesnost zmen u potomku, tj. muze se stat ze svoje atributy uspesne 
 aktualizuje, ale u potomku se to nepodari (napriklad kvuli nedostatku pameti).
 Konzistence dat je zajistena tak, ze neaktualizovani potomci jsou nastaveni na
 NSNull. Update ze site by mel probehnout nasledovne:
 1. vytvoreni kopie dat objektu (pritom se nestara o potomky), nepovede-li se -> konec
 2. pokus o update svych dat na disku
 3. vyreseni potomku
	  a. smazani z disku a dealokace zaniknuvsich
      b. update stavu prezivsich (nemusi probehnout, necha se tak jak je - i na disku)
      c. vyroba novych potomku (nemusi se povest -> NSNull)
 
 
 Memory warning
 
 Vzhledem k tomu, ze vsechny objekty, ktere jsou ve validnim stavu lze 
 nacist z disku nebo site, lze celou strukturu dat okamzite dealokovat v pripade
 nedostatku pameti. 
 
 Informovani uzivatele o chybach
 
 Je-li aplikace aktivni, mel by byt uzivatel informovany o vsech 
 chybach, ktere mohou znamenat, ze se k nemu nedostane nejaka zprava.
 
 */

#import <Foundation/Foundation.h>

@interface ISDSObject : NSObject {

	ISDSObject* parent;
}

@property (nonatomic, assign) ISDSObject *parent;

- (id)initWithParent:(ISDSObject*)parent;

- (BOOL)writeToFile:(NSDictionary*)plist;
- (BOOL)updateFromFile;

/* including directory contents */
- (BOOL)removeFileAndDir;

/* these are abstract methods */

// initialize from property list
- (BOOL)updateFromPlist:(NSDictionary*)plist;

// directory, including terminating slash
- (NSString*)dir;

// file with data of the object
- (NSString*)filename;



@end
