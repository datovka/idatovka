/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SignedSentMessageDownload.m
//  InputTest
//
//  Created by Petr Hruška on 7/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SignedSentMessageDownload.h"

#import "XMLSoapEnvelopeElement.h"
#import "XMLBodyElement.h"
#import "XMLSignedSentMessageDownloadResponseElement.h"
#import "XMLDmStatusElement.h"
#import "XMLDmStatusCodeElement.h"
#import "XMLDmStatusMessageElement.h"

#import "DER.h"
#import "SentMessageDownload.h"

@implementation SignedSentMessageDownload

@synthesize signature;
@synthesize msgId;
@synthesize message;

- (id)initWithMessageId:(NSString*)_msgId {
	
	const NSUInteger elementCount = 7;
	
	NSMutableArray* elems = [NSMutableArray arrayWithCapacity:elementCount];
	if (!elems) {
		[self dealloc];
		return nil;
	}
	
	@try {
	
		[elems addObject:[[XMLSoapEnvelopeElement alloc] init]];
		[elems addObject:[[XMLBodyElement alloc] init]];
		[elems addObject:[[XMLSignedSentMessageDownloadResponseElement alloc] init]];
		[elems addObject:[[XMLDmStatusElement alloc] init]];

		XMLSignatureElement* signatureElement = [[XMLSignatureElement alloc] init];
		signatureElement.delegate = self;
		[elems addObject:signatureElement];
	
		XMLDmStatusCodeElement* statusCodeElement = [[XMLDmStatusCodeElement alloc] init];
		statusCodeElement.delegate = self;
		[elems addObject: statusCodeElement];

		XMLDmStatusMessageElement* statusMessageElement = [[XMLDmStatusMessageElement alloc] init];
		statusMessageElement.delegate = self;
		[elems addObject:statusMessageElement];
	}
	
	@catch (NSException* exception) {
//   STRANGE COMPILE ERROR!	@catch (NSInvalidArgumentException* exception) {
		/* argument of addObject was probably nil */
		[self dealloc];
		return nil;
	}
	
	self = [super initWithCapacity:5 andElements:elems];
	if (self == nil) {
		return self;
	}
	self.msgId = _msgId;
	return self;
}

- (void)dealloc {
	[signature release];
	[msgId release];
	[message release];
	
	[super dealloc];
}

- (void)parse:(NSData*)data {

	[super parse: data];
	if (self.errorDescription) return;
	if (!self.signature) {
		self.errorDescription = NSLocalizedString(@"Cannot parse message", @"Cannot parse message");
		return;
	}
	
	DER* der = [[DER alloc] initWithData:self.signature];
	if (!der) {
		self.errorDescription = NSLocalizedString(@"Invalid message format", @"Invalid message format");
		return;
	}
	NSData* msg = der.msg;
	if (!msg) {
        [der release];
		self.errorDescription = @"Chyba formátu zprávy";
		return;
	}
    [[msg retain] autorelease];
    [der release];
    
    
	self.message = [[SentMessageDownload alloc] init];
	if (!self.message) {
		self.errorDescription = NSLocalizedString(@"Not enough memory", @"Not enough memory");
		return;
	}
	[self.message release];
	[self.message parse:msg];
}

- (NSData*) soapRequest {
	NSString* pattern = [SoapMethod requestPattern];
	NSString* req = [NSString stringWithFormat: 
					 @"<ns1:SignedSentMessageDownload><dmID>%@</dmID></ns1:SignedSentMessageDownload>",
					 self.msgId
					 ];

	NSString* s = [NSString stringWithFormat: pattern, req];
	NSData* d = [s dataUsingEncoding:NSUTF8StringEncoding];
	return d;
}

- (NSString*)path {
	return  @"/DS/dz";
}

- (NSString*)alertTitle:(ISDSAccount*)account {
	return NSLocalizedString(@"Error while downloading message", @"Alert title");
}

@end
