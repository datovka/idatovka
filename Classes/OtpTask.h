/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  OtpConnection.h
//  ISDS
//
//  Created by Petr Hruška on 7/6/11.
//

#import <Foundation/Foundation.h>
#import "ISDSTask.h"
#import "OtpStatus.h"

@protocol OtpTaskDelegate <ISDSTaskDelegate>

@end

@interface OtpTask : ISDSTask {

}

@property (nonatomic, retain) OtpStatus* status;
@property (nonatomic, assign) id<OtpTaskDelegate> otpDelegate;

- (NSString*)host;
- (NSString*)base64Message:(NSHTTPURLResponse*)response withDefault:(NSString*)defaultMessage;

- (id)initWithOtpStatus:(OtpStatus*)status;
- (NSString*)urlString;
- (NSString*)otpPassword;

@end
