/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  PinEditViewController.h
//  ISDS
//
//  Created by Petr Hruška on 10/18/10.
//

#import <UIKit/UIKit.h>

@protocol PinEditViewControllerDelegate

- (void)lastDigitEntered:(id)sender;

@end

@interface PinEditViewController : UIViewController {

	UITextField *hiddenTextField;
	UILabel *captionLabel;
	UILabel *adviceLabel;
	id<PinEditViewControllerDelegate> delegate;
    NSString* _caption;
	
}

@property (nonatomic, retain) IBOutlet UITextField *hiddenTextField;
@property (nonatomic, retain) IBOutlet UILabel *captionLabel;
@property (nonatomic, retain) IBOutlet UILabel *adviceLabel;

@property (nonatomic, retain) NSString* caption;
@property (nonatomic, retain) NSString* advice;
@property (nonatomic, readonly) NSString* pin;
@property (nonatomic, assign) id<PinEditViewControllerDelegate> delegate;
@property (nonatomic, assign) BOOL keyboardVisible;

- (IBAction)textFieldValueChanged:(id)sender;
- (IBAction)textFieldEditingDidEnd:(id)sender;
- (NSString*)pin;
- (void)cleanPin;
- (void)showKeyboard;
- (void)hideKeyboard;

+ (NSTimeInterval)pinTimeout;
+ (void)incrementFailureCount;
+ (void)resetFailureCount;
+ (NSInteger)failureCount;

@end
