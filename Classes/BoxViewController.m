/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  BoxViewController.m
//  ISDS
//
//  Created by Petr Hruška on 6/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BoxViewController.h"
#import "ISDSAppDelegate_Shared.h"
#import "MessageViewController.h"
#import "NetworkUpdater.h"

#define BUTTON_CONFIG	0
#define BUTTON_MESSAGES	1


@implementation BoxViewController

@synthesize account;
@synthesize messageToPush=_messageToPush;

- (NSString*)boxType {
	return @"";
}

- (NSString*)boxTitle {
	ISDSAppDelegate_Shared* appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
	if ([appDelegate iPad]) {
		NSString* boxName = [self.account boxName];
		NSString* boxType = [self boxType];
		return [NSString stringWithFormat:@"%@ - %@", boxType, boxName];
	} else
		return [self.account boxName];
}


- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = [self boxTitle];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	
	[((UITableView*) self.view) reloadData];
	[self registerObserver];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
	[self unregisterObserver];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	ISDSAppDelegate_Shared* appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
	if ([appDelegate iPad]) 
        return YES;
    else
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	MsgList* list = [self getMsgList];
    return [list count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"MsgCell";
    
    MsgCell *cell = (MsgCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
		cell = [[[NSBundle mainBundle] loadNibNamed:@"MsgCell" owner:nil options:nil] lastObject];
    }
	
	MsgList* list = [self getMsgList];
	ISDSMessage* msg = [list messageAtIndex: indexPath.row];
	[cell setupFromMessage:msg];
	return cell;
}


- (void)pushMessageViewController:(ISDSMessage*)msg
{
	MsgList* list = [self getMsgList];	
    ISDSAppDelegate_Shared* appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
    NSString* nibName = [appDelegate appendArchitecture:@"MessageViewController"];
	MessageViewController *messageViewController = [[MessageViewController alloc] initWithNibName:nibName bundle:nil];
	messageViewController.msg = msg;
	messageViewController.account = account;
	messageViewController.title = [NSString stringWithFormat:@"%lu(%lu)", (unsigned long)list.count, (unsigned long)list.unread];
	[self.navigationController pushViewController:messageViewController animated:YES];
	[messageViewController release];    
}


- (void)showMessage:(ISDSMessage*)msg
{	
	[msg markRead];

    [msg loadAttachments];
    if (!msg.attachments) {
        NetworkUpdater* networkUpdater = [NetworkUpdater networkUpdater];
        TokenViewController* tokenViewController = [networkUpdater tokenViewControllerForAccount:self.account];
        if (tokenViewController) {
            tokenViewController.delegate = self;
            tokenViewController.popAnimated = NO;
            self.messageToPush = msg;
            tokenViewController.prompt = NSLocalizedString(@"iDatovka needs download a message", nil);
            [self.navigationController pushViewController:tokenViewController animated:YES];            
            return;
        }
        
    }
    [self pushMessageViewController:msg];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{	
	MsgList* list = [self getMsgList];
	ISDSMessage* msg = [list messageAtIndex: indexPath.row];
	[self showMessage:msg];
}


- (void)dealloc {
    [_messageToPush release];
    [account release];
    [super dealloc];
}


- (MsgList*)getMsgList {
	return nil;
}


-(void)registerObserver {
    [self.account addObserver:self];
}


-(void)unregisterObserver {
    [self.account removeObserver:self];
}


- (void)listChanged:(MsgList*)list withAccount:(ISDSAccount*)account {
	 [((UITableView*) self.view) reloadData];
}

- (void)account:(ISDSAccount*)account changedBecause:(ISDSAccountChangeReason)reasonFlags
{
    if (reasonFlags & ISDSAccountChangeReasonBoxName)
        self.title = [self boxTitle];
}

#pragma mark - ContentProvider

- (void)showPopoverButton:(UIBarButtonItem *)button
{
    self.navigationItem.leftBarButtonItem = button;
}

- (void)hidePopoverButton
{
    self.navigationItem.leftBarButtonItem = nil;
}

#pragma mark - TokenViewControllerDelegate

- (void)tokenViewControllerDone:(TokenViewController*)tokenViewController
{
    [self pushMessageViewController:self.messageToPush];
    self.messageToPush = nil;
}

@end

