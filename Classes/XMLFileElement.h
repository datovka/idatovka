/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLFileElement.h
//  ISDS
//
//  Created by Petr Hruška on 8/11/10.
//

#import <Foundation/Foundation.h>
#import "XMLElement.h"

@protocol XMLFileElementDelegate

- (void)addAttachmentWithName:(NSString*)name andContent:(NSData*)content;

@end

@interface XMLFileElement : XMLElement {

	id<XMLFileElementDelegate> delegate;
	
	NSString* name;
	NSData* content;
	
}

@property (nonatomic, assign) id<XMLFileElementDelegate> delegate;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSData* content;

@end
