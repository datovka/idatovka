/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSConnection.h
//  InputTest
//
//  Created by Petr Hruška on 7/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoapMethod.h"
#import "ISDSTask.h"

@class ISDSAccount;

@interface SOAPTask : ISDSTask {
	
}

@property (nonatomic, retain) NSMutableData* data;
@property (nonatomic, retain) SoapMethod* method;

- (id)initWithAccount:(ISDSAccount*)account andMethod:(SoapMethod*)method;
+ (NSString*)observationClass;

@end