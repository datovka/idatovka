/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  CustomNavigationController.m
//  ISDS
//
//  Created by Petr Hruška on 12/1/10.
//

#import "CustomNavigationController.h"
#import "ISDSAppDelegate_Shared.h"


@implementation CustomNavigationController

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	ISDSAppDelegate_Shared* appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
	if ([appDelegate iPad]) 
        return YES;
    else {
        UIViewController* topViewController = [self topViewController];
        return [topViewController shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
    }
}


@end
