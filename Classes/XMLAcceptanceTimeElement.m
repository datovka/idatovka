/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLAcceptanceTime.m
//  InputTest
//
//  Created by Petr Hruška on 8/5/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "XMLAcceptanceTimeElement.h"
#import "XMLReturnedMessageElement.h"

@implementation XMLAcceptanceTimeElement

@synthesize delegate;

- (BOOL) matchNamespace:(NSString*)namespaceURI element:(NSString*)elementName andAttributes:(NSDictionary*)attributes {
	return
	([namespaceURI isEqualToString: @"http://isds.czechpoint.cz/v20/SentMessage"] ||
	[namespaceURI isEqualToString: @"http://isds.czechpoint.cz/v20"]) &&	 
	[elementName isEqualToString: @"dmAcceptanceTime"];
}

- (void)pop {
	[self.delegate setRawAcceptanceTime:self.accumulator];
	[super pop];
}

@end
