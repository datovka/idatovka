/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  DER.m
//  InputTest
//
//  Created by Petr Hruška on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DER.h"

#define ASN_TERMINATION        ((u8) 0x00)
#define ASN_BOOLEAN            ((u8) 0x01)
#define ASN_INTEGER            ((u8) 0x02)
#define ASN_BITSTRING          ((u8) 0x03)
#define ASN_STRING             ((u8) 0x04)
#define ASN_NULL               ((u8) 0x05)
#define ASN_OBJECT_ID          ((u8) 0x06)
#define ASN_UTC_TIME           ((u8) 0x17)
#define ASN_CONSTRUCTED_STRING ((u8) 0x24)
#define ASN_SEQUENCE           ((u8) 0x30)
#define ASN_SET				   ((u8) 0x31)
#define ASN_82                 ((u8) 0x82)
#define ASN_A0                 ((u8) 0xa0)
#define ASN_A3                 ((u8) 0xa3)

#define ASN_INDEFINITE_LENGTH  ((u8) 0x80)



@implementation DER

@synthesize msg;
@synthesize index;
@synthesize indexOfInterest;

- (int)errorMessage:(NSString*) m {
	NSLog(@"%@", m);
	return -1;
}

- (void)foundData:(NSData*)data {
	self.msg = data;
}

- (int)parseLength:(const u8*)bytes from:(int)offset to:(int)limit result:(int*)len {

	if (offset >= limit) return [self errorMessage:@"Unexpected end of data, length field expected"];

	int i = offset;
	u8 b1 = bytes[i++];
	
	if (b1 < 128) {
		*len = b1;
		return i;
	}
	
	int count = b1 - 128;
	int r = 0;
	while (count && i < limit) {
		r = (r << 8) + bytes[i++];
		count--;
	}
	if (count) return [self errorMessage:@"Length field too long."];
	*len = r;
	return i;
}

- (int)parseSequence:(const u8*)bytes from:(int)offset to:(int)limit {
	if (offset >= limit) return [self errorMessage:@"Unexpected end of data, sequence expected."];
	int i = offset + 1;
		
	if (i >= limit) return [self errorMessage:@"Sequence length expected, but end of data found."];
	int len = -1;
	if (bytes[i] != ASN_INDEFINITE_LENGTH) {
		i = [self parseLength:bytes from:i to:limit result:&len];
		if (i < 0) return i;
		limit = i + len;
	} else 
		i += 1;

	NSUInteger j = 0;
	while (i < limit) {
		
		if (len < 0) {
			if (limit - i < 2) return [self errorMessage:@"Missing sequence termination."];
			if (bytes[i] == ASN_TERMINATION && bytes[i + 1] == ASN_TERMINATION) {
				i += 2;
				break;
			}
		}
		
		self.index = [self.index indexPathByAddingIndex:j];

		i = [self parse:bytes from:i to:limit];
		if (i < 0) return i;
		
		self.index = [self.index indexPathByRemovingLastIndex];
		
		j++;
	}
	
	return i <= limit ? i : [self errorMessage:@"Sequence overflow."];
}

- (int)parseObject:(const u8*)bytes from:(int)offset to:(int)limit {
	if (offset >= limit) return -1;
	
	int len;
	int i = offset + 1;
	i = [self parseLength:bytes from:i to:limit result:&len];
	if (i < 0) return i;
	
	i += len;
	if (i > limit) return [self errorMessage:@"OID too long."];;
	return i;
}


- (int)parseString:(const u8*)bytes from:(int)offset to:(int)limit data:(NSData**)data {
	
	if (offset >= limit) return -1;
	if (bytes[offset] != ASN_STRING) return -1;
	
	int i = offset + 1;
	int len;
	i = [self parseLength:bytes from:i to:limit result:&len];
	if (i < 0) return i;
	
	if (i + len > limit) return -1;
	
	if (data) *data = [NSData dataWithBytes:&bytes[i] length:len];
	i += len;
	return i;
}

- (int)parseConstructedString:(const u8*)bytes from:(int)offset to:(int)limit {
	
	if (offset >= limit) return -1;
	if (bytes[offset] != ASN_CONSTRUCTED_STRING) return -1;

	int i = offset + 1;
	if (i >= limit) return -1;
	if (bytes[i] != ASN_INDEFINITE_LENGTH) return -1;
	i += 1;
	
	BOOL gathering = [self.index compare:self.indexOfInterest] == NSOrderedSame;
	NSMutableData* r = gathering ? [NSMutableData data] : nil;
	
	while (i < limit) {
		
		if (limit - i < 2) return -1;
		if (bytes[i] == ASN_TERMINATION && bytes[i + 1] == ASN_TERMINATION) {
			i += 2;
			break;
		}
		
		NSData* data;
		i = [self parseString:bytes from:i to:limit data:&data];
		if (i < 0) return i;
		[r appendData:data];
	}
		
	if (i > limit) return -1;
	
	[self foundData:r];
	return i;
}

- (int)parse:(const u8*)bytes from:(int)offset to:(int)limit {
	
	int i = offset;
	
	u8 identifier = bytes[i];
	switch (identifier) {
		case ASN_A0:
		case ASN_A3:
		case ASN_SEQUENCE:
			i = [self parseSequence:bytes from:i to:limit];
			if (i < 0) return [self errorMessage:@"Error while parsing sequence."];
			break;
		case ASN_OBJECT_ID:
		case ASN_BITSTRING:
		case ASN_UTC_TIME:
		case ASN_INTEGER:
		case ASN_BOOLEAN:
		case ASN_NULL:
		case ASN_SET:
		case ASN_82:
			i = [self parseObject:bytes from:i to:limit];
			if (i < 0) return [self errorMessage:@"Error while parsing OID."];
			break;
		case ASN_STRING:
			i = [self parseString:bytes from:i to:limit data:NULL];
			if (i < 0) return [self errorMessage:@"Error while parsing string."];
			break;
		case ASN_CONSTRUCTED_STRING:
			i = [self parseConstructedString:bytes from:i to:limit];
			if (i < 0) return [self errorMessage:@"Error while parsing constructed string."];
			break;
		default:
			return [self errorMessage:[NSString stringWithFormat:@"Unknown identifier %02x at offset %d", identifier, i]];
	}
	if (i > limit) return -1;
	return i;
}
	
- (id)initWithData:(NSData*)data {
	
	const NSUInteger cIndexesOfInterest[5] = {1, 0, 2, 1, 0};
		
	if ((self = [super init])) {
		
		self.index = [[NSIndexPath alloc] init];
		if (self.index == nil) {
			[self dealloc];
			return nil;
		}
		
		self.indexOfInterest = [NSIndexPath indexPathWithIndexes:(NSUInteger*)cIndexesOfInterest length:5];
		
		const u8* bytes = (const u8*) [data bytes];
		int i = [self parse:bytes from:0 to:[data length]];
			
		if (i < 0) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (void)dealloc {
	[msg release];
	[index release];
	[indexOfInterest release];
	[super dealloc];
}
	
	

@end
