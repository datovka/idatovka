/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLMessage.m
//  ISDS
//
//  Created by Petr Hruška on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "XMLMessage.h"

@implementation XMLMessage

@synthesize stack;
@synthesize elements;
@synthesize errorDescription;

- (id)initWithCapacity:(NSUInteger)capacity andElements:(NSMutableArray*)elems {
	
	if ((self = [super init])) {
		self.elements = elems;
		self.stack = [NSMutableArray arrayWithCapacity:capacity];
		
		unknownElement = [[XMLElement alloc] init];
		if (!unknownElement) {
			[self dealloc];
			return nil;
		}
	}
	return self;
	
}

- (void)parse:(NSData*)data {
	
	self.errorDescription = nil;
	NSXMLParser* parser = [[[NSXMLParser alloc] initWithData:data] autorelease];
	[parser setShouldProcessNamespaces:YES]; 
	parser.delegate = self;
	if (![parser parse]) {
		if (self.errorDescription) return;
		self.errorDescription = NSLocalizedString(@"Unexpected data format", nil);
	}
}

- (void)dealloc {
	[stack release];
	[elements release];
	[unknownElement release];
	[errorDescription release];
	[super dealloc];
}


// NSXMLParser delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {

	XMLElement* parent = [self topElement];

	XMLElement* element = nil;
	for (XMLElement* e in self.elements) {
		if ([e matchNamespace:namespaceURI element:elementName andAttributes:attributeDict] && [e canPush:parent]) {
			element = e;
			break;
		}
	}
	if (!element)
		element = unknownElement;
	
	[self.stack addObject:element];
	[element push:parent];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	
	XMLElement* e = [self topElement];
	if (e && ![e matchNamespace:namespaceURI andElement:elementName]) {
		NSLog(@"Invalid enclosing element %@", qName);
		self.errorDescription = NSLocalizedString(@"Invalid message format", @"Invalid message format");
		[parser abortParsing];
		return;
	}
	
	[self.stack removeLastObject];
	[e pop];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	XMLElement* e = [self topElement];
	[e appendChunk:string];
}

- (void)parser:(NSXMLParser *)parser foundIgnorableWhitespace:(NSString *)whitespaceString {
}

- (XMLElement*)topElement {
	return (XMLElement*)[self.stack lastObject];
}

@end
