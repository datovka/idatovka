/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  RootViewController.m
//  ISDS
//
//  Created by Petr Hruška on 4/14/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "RootViewController_iPhone.h"

#import "InboxViewController.h"
#import "OutboxViewController.h"
#import "Accounts.h"
#import "SettingsViewController_iPhone.h"
#import "ISDSDefaults.h"
#import "GetOwnerInfoFromLogin.h"
#import "GetListOfReceivedMessages.h"
#import "GetListOfSentMessages.h"
#import "ProgressToolbar.h"

#define LISTS_INBOX_INDEX   0
#define LISTS_OUTBOX_INDEX  1

@implementation RootViewController_iPhone


- (void)pushInboxViewController:(ISDSAccount*)account {
	InboxViewController* inboxViewController = [[InboxViewController alloc] initWithNibName:@"BoxViewController" bundle:nil];
	inboxViewController.account = account;
	[self.navigationController pushViewController:inboxViewController animated:YES];
	[inboxViewController release];
}


- (void)pushOutboxViewController:(ISDSAccount*)account {
	OutboxViewController* outboxViewController = [[OutboxViewController alloc] initWithNibName:@"BoxViewController" bundle:nil];
	outboxViewController.account = account;
	[self.navigationController pushViewController:outboxViewController animated:YES];
	[outboxViewController release];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSUInteger index = indexPath.section;
	BOOL received = indexPath.row == 0;
	
	Accounts* accounts = [Accounts accounts];
	NSArray* activeAccounts = [accounts activeAccounts];
	ISDSAccount* account = (ISDSAccount*)[activeAccounts objectAtIndex: index];
	if (received)
		[self pushInboxViewController:account];
	else
		[self pushOutboxViewController:account];
}


- (void)pushSettingsViewController {
	SettingsViewController_iPhone* settingsViewController = [[SettingsViewController_iPhone alloc] initWithNibName:@"SettingsViewController" bundle:nil];
	[self.navigationController pushViewController:settingsViewController animated:YES];
	[settingsViewController release];
}


- (void)viewWillAppear:(BOOL)animated {
	
	[self.navigationController setToolbarHidden:FALSE animated:TRUE];
	[super viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
	
	[self.navigationController setToolbarHidden:TRUE animated:TRUE];
	[super viewWillDisappear:animated];
}


- (void)dealloc {
	[refreshButton release];
	[lastUpdate release];
	[progressToolbar release];
	[idleToolbarItems release];
    [super dealloc];
}


- (IBAction)config:(id)sender {
	
	[self pushSettingsViewController];
}

@end

