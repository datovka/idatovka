/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Estimates.m
//  ISDS
//
//  Created by Petr Hruška on 1/21/11.
//

#import "Estimates.h"
#import "Estimate.h"

@implementation Estimates

@synthesize estimates;

- (id)initFromFileWithParent:(ISDSObject*)parent_ {
    
	if ((self = [super initWithParent:parent_])) {
		
		if (![self updateFromFile]) {
			[self dealloc];
			return nil;
		}
	}
	
	return self;
}

- (id)initWithParent:(ISDSObject*)parent_ {
    
	if ((self = [super initWithParent:parent_])) {
		
		self.estimates = [NSMutableArray array];
		if (!self.estimates) {
			[self dealloc];
			return nil;
		}
	}
	
	return self;
}

- (NSDictionary*)plist {
	
	NSMutableArray* list = [NSMutableArray arrayWithCapacity:[self.estimates count]];
	if (list == nil) return nil;
	for (Estimate *e in self.estimates) {
		NSDictionary* estimatePlist = [e plist];
		if (!estimatePlist) return nil;
		[list addObject:estimatePlist];
	}
	
	return [NSDictionary dictionaryWithObject:list forKey:@"estimates"];
}

- (Estimate*)estimateForTask:(ISDSTask*)task {
	for (Estimate *e in self.estimates) {
		if ([e matchTask:task]) return e;
	}
	return nil;
}

- (NSTimeInterval)delayForTask:(ISDSTask *)task
{
    Estimate *e = [self estimateForTask:task];
	if (!e) return [Estimate defaultDelayForTask:task];
	return e.delay;
}

- (BOOL)updateDelay:(NSTimeInterval)delay forTask:(ISDSTask *)task
{	
	Estimate* e = [self estimateForTask:task];
	if (!e) {
		e = [[Estimate alloc] initWithTask:task];
		[self.estimates addObject:e];
		[e release];
	}
	if (!e) return FALSE;
	[e updateDelay:delay];
	
	NSDictionary* propList = [self plist];
	if (propList) [self writeToFile:propList];
	
	return TRUE;
}

- (BOOL)updateFromPlist:(NSDictionary*)plist {

	NSArray* list = [plist objectForKey:@"estimates"];
	if (!list) return FALSE;
	
	const NSUInteger count = [list count];
	self.estimates = [NSMutableArray arrayWithCapacity:count];
	if (!self.estimates) return FALSE;
	
	for (NSDictionary* dict in list) {
		Estimate* e = [[Estimate alloc] initWithPlist:dict];
        if (e) {
            [self.estimates addObject:e];
            [e release];
        }
	}
	return TRUE;
}

- (NSString*)dir {
	return @"";
}

- (NSString*)filename {
	return @"Estimates";
}

- (void)dealloc {
	[estimates release];
	[super dealloc];
}

@end
