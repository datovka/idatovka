/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SentMessageDownload.m
//  ISDS
//
//  Created by Petr Hruška on 8/25/10.
//

#import "SentMessageDownload.h"
#import "XMLMessageDownloadResponseElement.h"
#import "XMLReturnedMessageElement.h"
#import "XMLDmElement.h"
#import "XMLFilesElement.h"
#import "XMLEncodedContentElement.h"

#import "Attachment.h"


@implementation SentMessageDownload

@synthesize dmId;
@synthesize idSender;
@synthesize sender;
@synthesize senderAddress;
@synthesize idRecipient;
@synthesize recipient;
@synthesize recipientAddress;
@synthesize annotation;
@synthesize rawAcceptanceTime;
@synthesize acceptanceTime;
@synthesize attachments;


- (id)init {
	
	const NSUInteger elementCount = 8;
	
	NSMutableArray* elems = [NSMutableArray arrayWithCapacity:elementCount];
	if (!elems) {
		[self dealloc];
		return nil;
	}
	
	@try {
		
		[elems addObject:[[XMLMessageDownloadResponseElement alloc] init]];	
		[elems addObject:[[XMLReturnedMessageElement alloc] init]];
		[elems addObject:[[XMLDmElement alloc] init]];
		[elems addObject:[[XMLFilesElement alloc] init]];
		[elems addObject:[[XMLEncodedContentElement alloc] init]];
		
		XMLMessageItemElement* messageItem = [[XMLMessageItemElement alloc] init];
		messageItem.delegate = self;
		[elems addObject:messageItem];
		
		XMLAcceptanceTimeElement* acceptanceTimeElement = [[XMLAcceptanceTimeElement alloc] init];	
		acceptanceTimeElement.delegate = self;
		[elems addObject:acceptanceTimeElement];
		
		XMLFileElement* file = [[XMLFileElement alloc] init];
		file.delegate = self;
		[elems addObject:file];
		
	}
	
	@catch (NSException* exception) {
		[self dealloc];
		return nil;		
	}
	
	self = [super initWithCapacity:5 andElements:elems];
	return self;
}

- (void)dealloc {
	[dmId release];
	
	[idSender release];
	[sender release];
	[senderAddress release];
	
	[idRecipient release];
	[recipient release];
	[recipientAddress release];
	
	[annotation release];
	[rawAcceptanceTime release];
	[acceptanceTime release];
	
	[attachments release];
	
	[super dealloc];
}

- (void)parse:(NSData*)data {
	
	[super parse:data];
	if (self.errorDescription) return;
	
	self.acceptanceTime = parseAcceptanceTime(self.rawAcceptanceTime);
}

- (void)addAttachmentWithName:(NSString*)name andContent:(NSData*)content {
	
	if (self.attachments == nil) {
		self.attachments = [NSMutableArray array];
		if (self.attachments == nil) return; //not enough memory
	}
	
	NSArray* a = [NSArray arrayWithObjects:name, content, nil];
	if (a == nil) return;
	[self.attachments addObject:a];
}

@end
