/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSConnection.m
//  ISDS
//
//  Created by Petr Hruška on 7/8/11.
//

#import <SystemConfiguration/SystemConfiguration.h>

#import "ISDSTask.h"
#import "Accounts.h"
#import "CertificateManager.h"

#define ISDS_SUBJECT_TESTING               @"*.czebox.cz"
#define ISDS_SUBJECT_RELEASE               @"*.mojedatovaschranka.cz"
#define TASK_DELAY                         0.5


@implementation ISDSTask

@synthesize databox=_databox;
@synthesize testing=_testing;
@synthesize errorDescription=_errorDescription;
@synthesize delegate=_delegate;
@synthesize connection=_connection;
@synthesize started=_started;
@synthesize timer=timer_;

- (void)dealloc
{
    [_databox release];
    [_errorDescription release];
    [_connection release];
    [_started release];
    [super dealloc];
}

- (BOOL)networkUnavailable
{
    
    NSString* host = [self host];
    if (host == nil) return FALSE;
    const char* node = [host cStringUsingEncoding:NSASCIIStringEncoding];                       
    
    SCNetworkReachabilityRef target = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, node);
    SCNetworkReachabilityFlags flags;
    BOOL r = SCNetworkReachabilityGetFlags(target, &flags);
    if (!r) return FALSE; //we don't know
    if (flags & kSCNetworkFlagsReachable) return FALSE;
    return TRUE;
}

- (void)removeCookies
{
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    // remove all cookies (possibly set up from other accounts)
    NSArray* cookies = [cookieStorage cookies];
    for (NSHTTPCookie* cookie in cookies) {
        [cookieStorage deleteCookie:cookie];
    }
}

- (NSURLRequest*)request
{
    return nil; // abstract method
}


- (id)initWithAccount:(ISDSAccount*)account
{
    if ((self = [super init])) {
        self.databox = account.databox;
        self.testing = account.testing;
    }
    return self;
}

- (NSArray*)observationClasses
{
    return [NSArray arrayWithObject:NSStringFromClass([self class])];
}

- (ISDSAccount*)account
{
    Accounts* accounts = [Accounts accounts];
    ISDSAccount* account = [accounts accountById:self.databox andTesting:self.testing];
    return account;
}

- (NSString*)password
{
    ISDSAccount* account = [self account];
    if (!account) return nil;
    return account.password;
}

- (NSString*)host
{
    return nil;
}

- (NSString*)certSubject:(BOOL)testing
{
	return testing ? ISDS_SUBJECT_TESTING : ISDS_SUBJECT_RELEASE;
}

- (void)setErrorDescriptionForError:(NSError*)error
{
    NSString* errDescr;
	if (error.code == -1012) {
        if (self.errorDescription) {
            errDescr = self.errorDescription;
        } else {
            errDescr = NSLocalizedString(@"Cannot access databox. Please check databox settings.", @"Cannot access databox. Please check databox settings.");
        }
	} else {
        if ([self networkUnavailable])
            errDescr = NSLocalizedString(@"Network not available.", @"Network not available.");
        else
            errDescr = NSLocalizedString(@"Cannot connect to ISDS server.", @"Cannot connect to ISDS server.");
    }
    
    self.errorDescription = errDescr;
}


- (void)failWithDescription:(NSString*)errorDescription
{
    [self cancel];
    self.errorDescription = errorDescription;
    [self sendFailure];
}

- (void)start_
{
//    self.started = [NSDate date];
    
    self.timer = nil;
    NSURLRequest* r = [self request];
    if (!r) {
        self.errorDescription = NSLocalizedString(@"Not enough memory", nil);
        [self sendFailure];
        return;
    }
    
    self.connection = [NSURLConnection connectionWithRequest:r delegate:self];
    if (!self.connection) {
        self.errorDescription = NSLocalizedString(@"Not enough memory", nil);
        [self sendFailure];
        return;
    }
}

- (void)start
{
    self.started = [NSDate date];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:TASK_DELAY target:self selector:@selector(start_) userInfo:NULL repeats:NO];
}


- (void)cancel
{
    [self.timer invalidate];
    self.timer = nil;
    [self.connection cancel];
}

- (NSString*)taskDescription
{
    return @"";
}


#pragma mark - NSURLConnectionDelegate

+ (SecCertificateRef)SecTrustGetLeafCertificate:(SecTrustRef)trust
{
    SecCertificateRef   result;
    
    if (SecTrustGetCertificateCount(trust) > 0) {
        result = SecTrustGetCertificateAtIndex(trust, 0);
    } else {
        result = NULL;
    }
    return result;
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	
	NSURLProtectionSpace* protectionSpace = challenge.protectionSpace;
    //NSLog(@"authenticationMethod = %@", [protectionSpace authenticationMethod]);
    //NSLog(@"host = %@", [protectionSpace host]);
    //NSLog(@"realm = %@", [protectionSpace realm]);
	
	if ([[protectionSpace authenticationMethod] isEqual:NSURLAuthenticationMethodServerTrust]) {
        
		SecTrustRef trust = protectionSpace.serverTrust;
        BOOL trusted = TRUE;
        
        /*SecCertificateRef serverCert = [ISDSTask SecTrustGetLeafCertificate:trust];
        NSString* subjectSummary = (NSString*)SecCertificateCopySubjectSummary(serverCert);
        if (![subjectSummary isEqualToString:[self certSubject:self.testing]]) trusted = FALSE;
        [subjectSummary release];*/
        
        if (trusted) {
            OSStatus err;
            SecTrustResultType trustResult;
            CertificateManager* certificateManager = [CertificateManager defaultCertificateManager];
            CFArrayRef anchorCertificates = [certificateManager anchorCertificates];
            err = SecTrustSetAnchorCertificates(trust, anchorCertificates);
            if (err == noErr) {
                err = SecTrustEvaluate(trust, &trustResult);
                trusted = (err == noErr) && ((trustResult == kSecTrustResultProceed) || (trustResult == kSecTrustResultUnspecified));
            } else trusted = FALSE; 
        }
        
		if (trusted) {
			NSURLCredential* credential = [NSURLCredential credentialForTrust:trust];
			[[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
		} else {
            self.errorDescription = NSLocalizedString(@"Unable to verify server identity.", nil);
			[[challenge sender] cancelAuthenticationChallenge:challenge];
        }
		
	} else
		[[challenge sender] cancelAuthenticationChallenge:challenge];
}

- (void)updateEstimate
{
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:self.started];
    [[self account] updateEstimate:self withDelay:interval];
}

- (void)sendSuccesfullyDone
{
    [self updateEstimate];
    [self.delegate taskSuccessfullyDone:self];
}

- (void)sendFailure
{
    [self.delegate taskFailed:self];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [self setErrorDescriptionForError:error];
    [self sendFailure];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.errorDescription)
        [self sendFailure];
    else
        [self sendSuccesfullyDone];
}


@end
