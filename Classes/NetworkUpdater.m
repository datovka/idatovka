/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  NetworkUpdater.m
//  ISDS
//
//  Created by Petr Hruška on 1/13/11.
//

#import "NetworkUpdater.h"
#import "ISDSAccount.h"
#import "GetOwnerInfoFromLogin.h"
#import "GetListOfReceivedMessages.h"
#import "GetListOfSentMessages.h"
#import "RequestManager.h"
#import "Accounts.h"
#import "ReceivedMessageDownload.h"
#import "SignedSentMessageDownload.h"
#import "SOAPTask.h"


@implementation NetworkUpdater

- (id)init {
	
	if ((self = [super init])) {
		
		RequestManager *rm = [RequestManager requestManager];
		if (!rm) {
			[self dealloc];
			return nil;
		}
		
		[rm addObserver:self forClasses:[NSArray arrayWithObject: [SOAPTask observationClass]]];
	}
	
	return self;
}

- (void)showErrorAlertForTask:(ISDSTask*)task {
	
	Accounts* accounts = [Accounts accounts];
	ISDSAccount* account = [accounts accountById:task.databox andTesting:task.testing];
    
	NSString* title;
    SoapMethod* method = [((SOAPTask*)task) method];
    title = [method alertTitle:account];
	
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle: title
													message: task.errorDescription
												   delegate: self
										  cancelButtonTitle: NSLocalizedString(@"OK", @"OK")
										  otherButtonTitles: nil ];
	[alert show];
	[alert release];	
}

- (void)showErrorAlert:(NSString*)message {
    NSString* title = NSLocalizedString(@"Error", nil);
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle: title
													message: message
												   delegate: self
										  cancelButtonTitle: NSLocalizedString(@"OK", @"OK")
										  otherButtonTitles: nil ];
	[alert show];
	[alert release];	
}



- (void)addMethod:(SoapMethod*)method withAccount:(ISDSAccount*)account {

	if (method == nil) {
		[self showErrorAlert:NSLocalizedString(@"Not enough memory", @"Not enough memory")];
		return;
	}
	
	RequestManager *rm = [RequestManager requestManager];
	if (!rm) {
		[self showErrorAlert:NSLocalizedString(@"Not enough memory", @"Not enough memory")];
		return;
	}
    
    SOAPTask* task = [[SOAPTask alloc] initWithAccount:account];
    task.method = method;
    if (!task) {
		[self showErrorAlert:NSLocalizedString(@"Not enough memory", @"Not enough memory")];
		return;
    }
	[rm addTasks:[NSArray arrayWithObject:task] withPriority:YES];
    [task release];
}

- (void)appendTaskTo:(NSMutableArray*)tasks forMethod:(SoapMethod*)method andAccount:(ISDSAccount*)account
{
    ISDSTask* t = [[SOAPTask alloc] initWithAccount:account andMethod:method];
    [tasks addObject:t];
    [t release];    
}

- (void)appendUpdateTasks:(NSMutableArray*)tasks forAccount:(ISDSAccount*)account {
    
	SoapMethod* method;
    
	if (![account loadOwnerInfo]) {
		method = [[GetOwnerInfoFromLogin alloc] init];
        [self appendTaskTo:tasks forMethod:method andAccount:account];
        [method release];
    }
    
    method = [[GetListOfReceivedMessages alloc] init];
    [self appendTaskTo:tasks forMethod:method andAccount:account];    
    [method release];
    
    method = [[GetListOfSentMessages alloc] init];
    [self appendTaskTo:tasks forMethod:method andAccount:account];
    [method release];
}

- (BOOL)isPrepared:(ISDSAccount*)account
{
    return account.authType == ISDSAccountAuthPass || [account hasCookie];
    
}

- (NSArray*)preparedAccounts
{
    NSArray* activeAccounts = [[Accounts accounts] activeAccounts];
    NSMutableArray* accounts = [NSMutableArray arrayWithCapacity:[activeAccounts count]];
    for (ISDSAccount* a in activeAccounts) {
        if ([self isPrepared:a]) [accounts addObject:a];
    }    
    return accounts;
}

- (void)updateMessages
{
    NSArray* accounts = [self preparedAccounts];
    
    NSMutableArray* tasks = [NSMutableArray arrayWithCapacity:[accounts count] * 3];
    
    for (ISDSAccount* a in accounts) [self appendUpdateTasks:(NSMutableArray*)tasks forAccount:a];
    RequestManager* requestManager = [RequestManager requestManager];
    [requestManager addTasks:tasks withPriority:YES];
}

- (TokenViewController*)tokenViewController
{
    Accounts* accounts = [Accounts accounts];
    
    if ([accounts needToken]) {
        
        TokenViewController* tokenViewController = [[TokenViewController alloc] initWithNibName:@"TokenViewController" bundle:nil];
        [tokenViewController setupAccounts:[accounts activeAccounts]];
        tokenViewController.displayContext = ProgressDisplayContextAuto;
        return [tokenViewController autorelease];
    } 
    else
        return nil;
}

- (TokenViewController*)tokenViewControllerForAccount:(ISDSAccount *)account
{
    if ([account needsToken]) {
        
        TokenViewController* tokenViewController = [[TokenViewController alloc] initWithNibName:@"TokenViewController" bundle:nil];
        [tokenViewController setupAccounts:[NSArray arrayWithObject:account]];
        return [tokenViewController autorelease];
    } 
    else
        return nil;
}

- (void)updateOwnerInfoForAccount:(ISDSAccount*)account {
	
    if (![self isPrepared:account]) return;
	GetOwnerInfoFromLogin *method = [[GetOwnerInfoFromLogin alloc] init];
	[self addMethod:method withAccount:account];
}

- (void)downloadFullMessage:(ISDSMessage*)message withAccount:(ISDSAccount*)account {

	if (message.incoming) {
		ReceivedMessageDownload* method = [[ReceivedMessageDownload alloc] initWithMessageId:message.msgId];
		[self addMethod:method withAccount:account];
	} else {
		SignedSentMessageDownload* method = [[SignedSentMessageDownload alloc] initWithMessageId:message.msgId];
		[self addMethod:method withAccount:account];
	}
}


+ (NetworkUpdater*) networkUpdater {
	
	static NetworkUpdater* updater = nil;
	if (!updater) updater = [[NetworkUpdater alloc] init];
	return updater;
}

- (void)statusUpdate:(enum RMStatus)status forTask:(ISDSTask*)task {
	
    if (![task isKindOfClass:[SOAPTask class]]) return;
    SoapMethod* method = [(SOAPTask*)task method];
    
	if (status == RMDone) {
		Accounts* accounts = [Accounts accounts];
		ISDSAccount* account = [accounts accountById:task.databox andTesting:task.testing];

		if ([method isKindOfClass:[GetOwnerInfoFromLogin class]]) {
			
			[account updateOwnerInfo:(GetOwnerInfoFromLogin*)method];
			
		} else if ([method isKindOfClass:[GetListOfReceivedMessages class]]) {
			
			[account updateReceivedMessageList:(GetListOfReceivedMessages*)method];
			
		} else if ([method isKindOfClass:[GetListOfSentMessages class]]) {
		
			[account updateSentMessageList:(GetListOfSentMessages*)method];
			
		} else if ([method isKindOfClass:[ReceivedMessageDownload class]]) {
			
			[account updateFromReceivedMessage:(ReceivedMessageDownload*)method];
			
		} else if ([method isKindOfClass:[SignedSentMessageDownload class]]) {
			
			[account updateFromSentMessage:(SignedSentMessageDownload*)method];
			
		}
	} else if (status == RMFailed) {
        [self showErrorAlertForTask:task];
    }
}

- (void)progressUpdate:(float)progress estimate:(NSTimeInterval)estimate elapsed:(NSTimeInterval)elapsed {
//	NSLog(@"progress=%.2f, estimate=%.1f", progress, estimate);
}

@end
