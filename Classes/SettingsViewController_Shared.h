/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SettingsViewController.h
//  ISDS
//
//  Created by Petr Hruška on 7/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwitchCell.h"
#import "ISDSAccount.h"
#import "RequestManager.h"
#import "ProgressToolbar.h"
#import "AccountViewController_Shared.h"

@interface SettingsViewController_Shared : UITableViewController 
	<UIActionSheetDelegate, SwitchCellDelegate, ISDSAccountObserver, RequestManagerObserver> {

	SwitchCell* autoUpdateCell;
	NSArray* observedClasses;
	ProgressToolbar* progressToolbar;
	NSUInteger pendingTasks;
}

@property (nonatomic, retain) IBOutlet SwitchCell* autoUpdateCell;
@property (nonatomic, retain) NSArray* observedClasses;
@property (nonatomic, retain) ProgressToolbar* progressToolbar;
@property (nonatomic, assign) NSUInteger pendingTasks;

- (void)switchCell:(SwitchCell*)cell switchedTo:(BOOL)value;
- (void)cleanCache;
- (AccountViewController_Shared*)addAccountViewController;
- (AccountViewController_Shared*)editAccountViewController:(ISDSAccount*)account;
- (void)createToolbar;


@end
