/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  OwnerInfo.h
//  ISDS
//
//  Created by Petr Hruška on 7/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISDSObject.h"

@class GetOwnerInfoFromLogin;

@interface OwnerInfo : ISDSObject {

	NSString* firstName;
	NSString* lastName;
	NSString* firmName;
}

@property (nonatomic, retain) NSString* firstName;
@property (nonatomic, retain) NSString* lastName;
@property (nonatomic, retain) NSString* firmName;

- (id) initWithParent:(ISDSObject*)parent_ andMethod:(GetOwnerInfoFromLogin*)method;
- (id) initFromFileWithParent:(ISDSObject*)parent_;
- (BOOL)updateFromMethod:(GetOwnerInfoFromLogin*)method;
- (void)clean;

@end
