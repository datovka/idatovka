/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  EnvelopeData.h
//  ISDS
//
//  Created by Petr Hruška on 8/19/10.
//

#import <Foundation/Foundation.h>


@interface EnvelopeData : NSObject {

	NSString* msgId;
	
	NSString* sender;
	NSString* senderAddress;
	NSString* recipient;
	NSString* recipientAddress;
	NSString* annotation;
	NSString* rawAcceptanceTime;
	NSDate* acceptanceTime;
}

@property (nonatomic, retain) NSString* msgId;

@property (nonatomic, retain) NSString* sender;
@property (nonatomic, retain) NSString* senderAddress;
@property (nonatomic, retain) NSString* recipient;
@property (nonatomic, retain) NSString* recipientAddress;
@property (nonatomic, retain) NSString* annotation;
@property (nonatomic, retain) NSString* rawAcceptanceTime;
@property (nonatomic, retain) NSDate* acceptanceTime;

@end
