/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  TokenCell.m
//  ISDS
//
//  Created by Petr Hruška on 7/4/11.
//

#import "TokenCell.h"

#define BASIC_CELL_HEIGHT          46
#define ERROR_VIEW_PADDING         8
#define ERROR_VIEW_WIDTH           (300 - 2*ERROR_VIEW_PADDING)
#define ERROR_FONT_SIZE            14

@implementation TokenCell

@synthesize boxNameLabel=_boxNameLabel;
@synthesize messageLabel=_messageLabel;
@synthesize tokenText=_tokenText;
@synthesize sendButton=_sendButton;
@synthesize errorView=errorView_;

- (void)dealloc
{
    [_boxNameLabel release];
    [_messageLabel release];
    [_tokenText release];
    [_sendButton release];
    [errorView_ release];
    [super dealloc];
}

- (NSString*)boxName
{
    return self.boxNameLabel.text;
}

- (void)setBoxName:(NSString *)boxName
{
    self.boxNameLabel.text = boxName;
}

- (NSString*)message
{
    return self.messageLabel.text;
}

- (void)setMessage:(NSString *)message
{
    self.messageLabel.text = message;
}

- (NSString*)token
{
    return self.tokenText.text;
}

- (void)setToken:(NSString *)token
{
    self.tokenText.text = token;
}

- (void)setStatus:(enum OtpStatusType)status
{
    self.sendButton.hidden = YES;
    self.tokenText.hidden = YES;
    self.messageLabel.hidden = YES;
    
    switch (status) {
        case OtpStatusShouldSendSms:
            self.sendButton.hidden = NO;
            break;
        case OtpStatusEnterToken:
            self.tokenText.hidden = NO;
            break;
        case OtpStatusRequestingSms:
        case OtpStatusRequestingCookie:
        case OtpStatusHasCookie:
            self.messageLabel.hidden = NO;
            break;
    }
}

- (void)setErrorDescription:(NSString *)errorDescription
{
    self.errorView.font = [UIFont systemFontOfSize:ERROR_FONT_SIZE];
    self.errorView.text = errorDescription;
    self.errorView.hidden = errorDescription == nil || [errorDescription length] == 0;
    if (!self.errorView.hidden) {
        CGRect frame = self.errorView.frame;
        frame.size.height = self.errorView.contentSize.height;
        self.errorView.frame = frame;
    }
}

- (NSString*)errorDescription
{
    return self.errorView.text;
}

- (void)setupTag:(NSUInteger)tag
{
    self.tag = tag;
    self.tokenText.tag = tag;
    self.sendButton.tag = tag;
}

- (void)setFocus
{
    [self.tokenText becomeFirstResponder];
}

+ (CGFloat)textHeightForErrorDescription:(NSString*)errorDescription
{
    CGSize textViewSize = CGSizeMake(ERROR_VIEW_WIDTH, CGFLOAT_MAX);
    CGSize textSize = [errorDescription sizeWithFont:[UIFont systemFontOfSize:ERROR_FONT_SIZE] constrainedToSize:textViewSize];
    return textSize.height;
}

+ (CGFloat)cellHeightForErrorDescription:(NSString*)errorDescription
{
    CGFloat height = BASIC_CELL_HEIGHT;
    
    if (errorDescription != nil && [errorDescription length] != 0) {
        height += [TokenCell textHeightForErrorDescription:errorDescription] + 2 * ERROR_VIEW_PADDING;
    }
    
    return height;
}

@end
