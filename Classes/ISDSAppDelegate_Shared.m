/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSAppDelegate.m
//  ISDS
//
//  Created by Petr Hruška on 4/14/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "ISDSAppDelegate_Shared.h"
#import "NetworkUpdater.h"
#import "Accounts.h"
#import "ISDSDefaults.h"

#define PIN_VERIFICATION_TIMEOUT    300.0

@implementation ISDSAppDelegate_Shared

@synthesize window=_window;
@synthesize securityWindow = _securityWindow;
@synthesize mimeTypes = _mimeTypes;
@synthesize rootViewController = _rootViewController;
@synthesize pinViewController = _pinViewController;
@synthesize lastPinVerification=_lastPinVerification;


#pragma mark -
#pragma mark Application lifecycle

- (void)loadMimeTypes {
	
	NSString *rootPath = [[NSBundle mainBundle ] bundlePath];
	//NSLog(@"rootPath = %@", rootPath);
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"MimeTypes.plist"];
	
	self.mimeTypes = nil;
	NSData *data = [NSData dataWithContentsOfFile:plistPath];
	if (!data) return;
	NSString* error = nil;
	
	self.mimeTypes = [ NSPropertyListSerialization
					  propertyListFromData: data
					  mutabilityOption:NSPropertyListImmutable
					  format: NULL
					  errorDescription: &error];
	if (error) [error release];
}

- (void)showSecurityWindow
{
    if (!self.securityWindow) {
        [[NSBundle mainBundle] loadNibNamed:@"SecurityWindow" owner:self options:nil];
        [self initPinViewController];
        self.securityWindow.rootViewController = self.pinViewController;
        self.pinViewController.view.frame = [[UIScreen mainScreen] applicationFrame];
    }
    [self.pinViewController restart];
    
    [self.window setHidden:YES];
    [self.securityWindow makeKeyAndVisible];
}

- (void)showMainWindow
{
    [self.securityWindow setHidden:YES];
    [self.window makeKeyAndVisible];
    ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
    if (defaults.autoUpdate) [self.rootViewController setupCookiesAndScheduleUpdate];
}

- (void)applicationDidFinishLaunching:(UIApplication *)application {    
	
	[self loadMimeTypes];
	[NetworkUpdater networkUpdater];
	
    ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
	if (defaults.pin) {
		[self showSecurityWindow];
	} else {
        [self showMainWindow];
	}
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Save data if appropriate
	[[Accounts accounts] save];
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
	if (defaults.pin) {
        BOOL longAgo = TRUE;
        if (self.lastPinVerification) {
            NSTimeInterval ago = -[self.lastPinVerification timeIntervalSinceNow];
            if (ago < PIN_VERIFICATION_TIMEOUT) longAgo = FALSE; 
        }

        if (longAgo) [self showSecurityWindow];
        
    } else if (defaults.autoUpdate)
        [self.rootViewController setupCookiesAndScheduleUpdate];
}

- (BOOL)iPad
{
	return FALSE;
}

- (NSString*)appendArchitecture:(NSString*)nibName {
	return nibName;
}

- (void)initPinViewController
{
    NSLog(@"Abstract method call");
}


#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	
	[_window release];
    [_securityWindow release];
	[_mimeTypes release];
	[_rootViewController release];
	[_pinViewController release];
    [_lastPinVerification release];
	
	[super dealloc];
}

- (void) applicationDidReceiveMemoryWarning {
	[[Accounts accounts] releaseCache];	
}

+ (ISDSAppDelegate_Shared*)theDelegate
{
    return (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
}


#pragma mark - PinViewControllerDelegate

- (void)pinVerified
{
    self.lastPinVerification = [NSDate date];
    [self showMainWindow];
}


+ (BOOL)ios7OrLater {
    
    NSString* currentVersion = [[UIDevice currentDevice] systemVersion];
    return [currentVersion compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending;
}

@end

