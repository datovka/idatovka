/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SwitchCell.h
//  ISDS
//
//  Created by Petr Hruška on 10/15/10.
//

#import <Foundation/Foundation.h>

@class SwitchCell;

@protocol SwitchCellDelegate

- (void)switchCell:(SwitchCell*)cell switchedTo:(BOOL)value;

@end

@interface SwitchCell : UITableViewCell {

	id<SwitchCellDelegate> delegate;
    BOOL _on;
}

@property (nonatomic, assign) IBOutlet id<SwitchCellDelegate> delegate;
@property (nonatomic, assign) BOOL on;
@property (nonatomic, retain) IBOutlet UISwitch* switcher;

- (void)setOn:(BOOL)value animated:(BOOL)animated;


@end
