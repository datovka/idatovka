/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SoapMethod.h
//  ISDS
//
//  Created by Petr Hruška on 8/18/10.
//

#import <Foundation/Foundation.h>
#import "XMLMessage.h"
#import "XMLDbStatusCodeElement.h"
#import "XMLDbStatusMessageElement.h"
#import "XMLDmStatusCodeElement.h"
#import "XMLDmStatusMessageElement.h"

@class ISDSAccount;

@interface SoapMethod : XMLMessage <XMLDbStatusCodeElementDelegate,
    XMLDbStatusMessageElementDelegate,
    XMLDmStatusCodeElementDelegate,
    XMLDmStatusMessageElementDelegate> {

	NSString* statusCode;
	NSString* statusMessage;
}

@property (nonatomic, retain) NSString* statusCode;
@property (nonatomic, retain) NSString* statusMessage;
@property (nonatomic, retain) NSDate* started;

- (NSData*)soapRequest;
+ (NSString*)requestPattern;
- (NSString*)path; // for example @"/DS/dz"

- (BOOL)duplicite:(SoapMethod*)method;
- (NSString*)alertTitle:(ISDSAccount*)account;
+ (NSString*)observationClass;

@end
