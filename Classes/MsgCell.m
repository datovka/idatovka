/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  MsgCell.m
//  ISDS
//
//  Created by Petr Hruška on 6/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "MsgCell.h"


@implementation MsgCell

- (void)dealloc {
	[_srLabel release];
	[_unreadIndicator release];
	[_annotationLabel release];
	[_dateLabel release];
    [super dealloc];
}


- (void)setupFromMessage:(ISDSMessage*)message {
	
	self.srLabel.text = message.incoming ? message.sender : message.recipient;
	
	if (message.read)
		self.unreadIndicator.image = nil;
	else
		self.unreadIndicator.image = [UIImage imageNamed: @"new.png"];
    
	self.annotationLabel.text = message.annotation;
    self.dateLabel.text = [message friendlyShortDateTime];	
}



@end
