/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  DeactivatePinViewController.h
//  ISDS
//
//  Created by Petr Hruška on 10/21/10.
//

#import <UIKit/UIKit.h>
#import "PinEditViewController.h"


@interface DeactivatePinViewController : UIViewController<PinEditViewControllerDelegate> {

	UIView *pinEditView;
	PinEditViewController* pinEditViewController;

}

@property (nonatomic, retain) IBOutlet UIView *pinEditView;
@property (nonatomic, retain) PinEditViewController *pinEditViewController;

@end
