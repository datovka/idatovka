/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  PINViewController_Shared.m
//  ISDS
//
//  Created by Petr Hruška on 10/14/10.
//

#import "PinViewController_Shared.h"
#import "PinEditViewController.h"
#import "ISDSDefaults.h"
#import "ISDSAppDelegate_iPad.h"


@implementation PinViewController_Shared

@synthesize delegate;

@synthesize pinEditView;
@synthesize pinEditViewController;


- (id)initWithNibName:(NSString*)nibName bundle:(NSBundle*)nibBundle {
	ISDSAppDelegate_Shared* appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
	return [super initWithNibName:[appDelegate appendArchitecture:nibName] bundle:nibBundle];
}


- (void)viewDidLoad {

    [super viewDidLoad];

	self.pinEditViewController = [[PinEditViewController alloc] initWithNibName:@"PinEditViewController" bundle:nil];
	self.pinEditViewController.view.frame = self.pinEditView.frame;
	self.pinEditViewController.caption = NSLocalizedString(@"Enter Your PIN", @"Enter Your PIN caption");
	[self.view addSubview:pinEditViewController.view];
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.pinEditViewController viewWillAppear:animated];
	self.pinEditViewController.delegate = self;
}

- (void)restart
{
    [self.pinEditViewController cleanPin];
    self.pinEditViewController.caption = NSLocalizedString(@"Enter Your PIN", @"Enter Your PIN caption");
    
    if ([PinEditViewController failureCount]) {
    
        [self.pinEditViewController hideKeyboard];
        self.pinEditViewController.advice = NSLocalizedString(@"Last entered PIN was invalid, please wait.", nil);
		[NSTimer scheduledTimerWithTimeInterval:[PinEditViewController pinTimeout] target:self selector:@selector(enterAgain:) userInfo:nil repeats:NO];
    } else {
        
        [self.pinEditViewController showKeyboard];
    }
}

- (void)lastDigitEntered:(id)sender {
	
	ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
	if ([self.pinEditViewController.pin isEqualToString:defaults.pin]) {
		
        [PinEditViewController resetFailureCount];
		[self.pinEditViewController hideKeyboard];
		[self.delegate pinVerified];        
        
	} else {
		
		self.pinEditViewController.advice = NSLocalizedString(@"Invalid PIN, please wait.", "Invalid PIN wait challenge");
		[self.pinEditViewController cleanPin];
		[self.pinEditViewController hideKeyboard];
        [PinEditViewController incrementFailureCount];
		[NSTimer scheduledTimerWithTimeInterval:[PinEditViewController pinTimeout] target:self selector:@selector(enterAgain:) userInfo:nil repeats:NO];
	}
}


- (void)enterAgain:(NSTimer*)timer {
	self.pinEditViewController.advice = NSLocalizedString(@"Invalid PIN, please try again.", "Invalid PIN re-enter challenge");
	[self.pinEditViewController showKeyboard];
}


- (void)dealloc {

	[_icon release];
	[pinEditView release];
	[pinEditViewController release];
	
    [super dealloc];
}

- (void)viewDidUnload
{
    [self setIcon:nil];
    [super viewDidUnload];
}

@end
