/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLElement.m
//  InputTest
//
//  Created by Petr Hruška on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "XMLElement.h"


@implementation XMLElement


- (BOOL) matchNamespace:(NSString*)namespaceURI element:(NSString*)elementName andAttributes:(NSDictionary*)attributes {
	return YES;
}

- (BOOL)matchNamespace:(NSString*)namespaceURI andElement:(NSString*)elementName {
	return [self matchNamespace:namespaceURI element:elementName andAttributes:nil];
}


- (BOOL)canPush:(XMLElement*)parent {
	return YES;
}

- (void)appendChunk:(NSString*)string {
}

- (void)push:(XMLElement*)parent {
}

- (void)pop {
}


@end
