/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
    //
//  DeactivatePinViewController.m
//  ISDS
//
//  Created by Petr Hruška on 10/21/10.
//

#import "DeactivatePinViewController.h"
#import "PinEditViewController.h"
#import "ISDSDefaults.h"

@implementation DeactivatePinViewController

@synthesize pinEditView;
@synthesize pinEditViewController;


- (void)viewDidLoad {
    [super viewDidLoad];
	
	self.title = NSLocalizedString(@"Deactivate PIN", @"Deactivate PIN caption");
	self.pinEditViewController = [[PinEditViewController alloc] initWithNibName:@"PinEditViewController" bundle:nil];
	self.pinEditViewController.view.frame = self.pinEditView.frame;
	self.pinEditViewController.caption = NSLocalizedString(@"Enter PIN", @"Enter PIN caption");
	[self.view addSubview:pinEditViewController.view];	
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self.pinEditViewController viewWillAppear:animated];
	self.pinEditViewController.delegate = self;
}


- (void)dealloc {
	
	[pinEditView release];
	[pinEditViewController release];
	
    [super dealloc];
}


- (void)enterAgain:(NSTimer*)timer {
	self.pinEditViewController.advice = NSLocalizedString(@"Invalid PIN, please try again.", "Invalid PIN re-enter challenge");
	[self.pinEditViewController showKeyboard];
}


- (void)lastDigitEntered:(id)sender {
	ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
	//NSLog(@"%@\n", defaults.pin);
	if ([defaults.pin isEqualToString:self.pinEditViewController.pin]) {
		defaults.pin = nil;
        [PinEditViewController resetFailureCount];
		[self.navigationController popViewControllerAnimated:YES];
	} else {
		[self.pinEditViewController cleanPin];
		self.pinEditViewController.advice = NSLocalizedString(@"Invalid PIN, please wait.", "Invalid PIN wait challenge");
		[self.pinEditViewController hideKeyboard];
        [PinEditViewController incrementFailureCount];
		[NSTimer scheduledTimerWithTimeInterval:[PinEditViewController pinTimeout] target:self selector:@selector(enterAgain:) userInfo:nil repeats:NO];
	}
}


@end
