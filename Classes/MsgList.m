/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  MsgList.m
//  ISDS
//
//  Created by Petr Hruška on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

/*
 
 messages a index nejsou nikdy nil
 
 
 */

#import "MsgList.h"
#import "ISDSMessage.h"
#import "AccountCache.h"
#import "ReceivedMessageDownload.h"
#import "SignedSentMessageDownload.h"

@implementation MsgList

@synthesize name;
@synthesize incoming;
@synthesize messages;
@synthesize unread;
@synthesize index;

- (void)callObservers {
	
	AccountCache* ac = (AccountCache*)self.parent;
	[ac callListObservers:self];
}

- (void)callMsgObservers:(ISDSMessage*)msg {
	AccountCache* ac = (AccountCache*)self.parent;
	[ac callMsgObservers:msg];
}


- (NSDictionary*)plist {

	NSArray* msgs = [self.messages allKeys];
	if (!msgs) return nil;

	NSNumber* objUnread = [NSNumber numberWithUnsignedInt:self.unread];
	if (!objUnread) return nil;

	return [NSDictionary dictionaryWithObjectsAndKeys:
				msgs, @"messages",
				objUnread, @"unread",
				nil
			];
}

- (void)dealloc {
	
	[messages release];
	[name release];
	[index release];
	[super dealloc];
}

+ (NSMutableArray*)createIndex:(NSDictionary*)new_messages {
	NSArray* values = [new_messages allValues];
	if (!values) return nil;
	NSMutableArray* new_index = [NSMutableArray arrayWithArray:values];
	if (!new_index) return nil;
	[new_index sortUsingSelector:@selector(compareAcceptanceTime:)];
	return new_index;
}

- (BOOL)loadFromFile {
	[self updateFromFile];
	if (!self.messages) return FALSE;
	
	NSArray* msgIds = [self.messages allKeys];
	if (!msgIds) return FALSE;
	
	for (NSString* msgId in msgIds) {
		ISDSMessage* msg = [[ISDSMessage alloc] initFromFileWithParent:self andId:msgId];
		if (!msg) {
            return FALSE;
        }
		[self.messages setObject:msg forKey:msgId];
        [msg release];
	}
    self.index = [MsgList createIndex:self.messages];
	
	return self.index != nil;
}

- (BOOL)updateFromMessages:(GetListOfMessages*)method {
	
	NSMutableDictionary* new_messages = [NSMutableDictionary dictionaryWithCapacity:[method.list count]];
	if (!new_messages) return FALSE;
	
	NSUInteger new_unread = 0;
	
	// fill dict with new version of messages
	for (EnvelopeData* envelope in method.list) {
		
		ISDSMessage* msg = [self.messages objectForKey:envelope.msgId];
		if (!msg) {
			/* new message */
			msg = [[ISDSMessage alloc] initWithParent:self fromEnvelope:envelope];
			if (!msg) return FALSE;
			if (!msg.read) new_unread++;			
			[new_messages setObject:msg forKey:envelope.msgId];
			[msg release];
		} else {
			/* old message */
			if (msg && ![msg updateFromEnvelope:envelope]) return FALSE; /* some important message could be skipped */
			if (!msg.read) new_unread++;			
			[new_messages setObject:msg forKey:envelope.msgId];
		}
	}
	
	NSMutableArray* new_index = [MsgList createIndex:new_messages];
	if (!new_index) return FALSE;
	
	self.messages = new_messages;
	self.unread = new_unread;
	self.index = new_index;
	
	// write to file
	NSDictionary* plist = [self plist];
	if (plist) [self writeToFile:plist];
	
	return TRUE;
}

- (id)initFromFileWithParent:(ISDSObject*)parent_ name:(NSString*)name_ incoming:(BOOL)incoming_ {
	
	if ((self = [super initWithParent:parent_])) {
		self.name = name_;
		self.incoming = incoming_;
		if (![self loadFromFile]) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (id)initWithSentMessages:(GetListOfSentMessages*)method andParent:(ISDSObject*)parent_
					  name:(NSString*)name_ incoming:(BOOL)incoming_ {
	if ((self = [super initWithParent:parent_])) {
		self.name = name_;
		self.incoming = incoming_;
		if (![self updateFromMessages:method]) {
			[self dealloc];
			return nil;
		}
	}
	return self;	
}

- (id)initWithReceivedMessages:(GetListOfReceivedMessages*)method andParent:(ISDSObject*)parent_
						  name:(NSString*)name_ incoming:(BOOL)incoming_ {
	if ((self = [super initWithParent:parent_])) {
		self.name = name_;
		self.incoming = incoming_;
		if (![self updateFromMessages:method]) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}


- (NSUInteger)count {
	return [self.messages count];
}

- (ISDSMessage*)message:(NSString*)msgId {	
	return [self.messages objectForKey:msgId];
}

- (ISDSMessage*)messageAtIndex:(NSUInteger)i {
	return [self.index objectAtIndex:i];
}

- (BOOL)updateFromPlist:(NSDictionary*)plist {
	
	if (!plist) return FALSE;
	NSArray* list = [plist objectForKey:@"messages"];
	if (!list) return FALSE;
	NSNumber* unreadNumber = [plist objectForKey:@"unread"];
	if (!unreadNumber) return FALSE;
	
	NSUInteger count = [list count];
	NSMutableDictionary* msgs = [NSMutableDictionary dictionaryWithCapacity:count];
	if (!msgs) return FALSE;
	for (NSString* msgId in list) [msgs setObject:[NSNull null] forKey:msgId]; 

	self.unread = [unreadNumber unsignedIntegerValue];
	self.messages = msgs;
	self.index = nil;
	
	return TRUE;
}

- (BOOL)updateFromReceivedMessages:(GetListOfReceivedMessages*)method {
	BOOL r = [self updateFromMessages:method];
	[self callObservers];
	return r;
}

- (BOOL)updateFromSentMessages:(GetListOfSentMessages*)method {
	BOOL r = [self updateFromMessages:method];
	[self callObservers];
	return r;
}

- (BOOL)updateFromReceivedMessage:(ReceivedMessageDownload*)method {
	ISDSMessage* msg = [self.messages objectForKey:method.dmId];
	return [msg updateFromReceivedMessage:method];
}

- (BOOL)updateFromSentMessage:(SignedSentMessageDownload*)method {
	ISDSMessage* msg = [self.messages objectForKey:method.msgId];
	return [msg updateFromSentMessage:method.message];
}

- (NSString*)dir {
	return self.name;
}

- (NSString*)filename {
	return @"MsgList";
}

- (BOOL)decrementUnread {
	if (self.unread <= 0) return FALSE;
	self.unread = self.unread - 1;
	
	// try to save
	NSDictionary* plist = [self plist];
	if (plist) [self writeToFile:plist];
	
	[self callObservers];
	return TRUE;
}

- (void)clean {
	[self removeFileAndDir];
}
	
@end





