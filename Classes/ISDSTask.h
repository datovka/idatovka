/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSConnection.h
//  ISDS
//
//  Created by Petr Hruška on 7/8/11.
//

#import <Foundation/Foundation.h>
#import "ISDSAccount.h"

@class ISDSTask;

@protocol ISDSTaskDelegate

- (void)taskSuccessfullyDone:(ISDSTask*)task;
- (void)taskFailed:(ISDSTask*)task;
@end


@interface ISDSTask : NSObject<NSURLConnectionDelegate> {
    
}

/* account identification (the account can be removed and it's password can be changed while waiting in a queue) */
@property (nonatomic, retain) NSString* databox;
@property (nonatomic) BOOL testing;

@property (nonatomic, retain) NSString* errorDescription;

@property (nonatomic, assign) NSObject<ISDSTaskDelegate>* delegate;

@property (nonatomic, retain) NSURLConnection* connection;
@property (nonatomic, retain) NSDate* started;
@property (nonatomic, retain) NSTimer* timer;

- (void)removeCookies;
- (NSURLRequest*)request;
- (id)initWithAccount:(ISDSAccount*)account;

/* misc info functions for subclasses */
- (NSArray*)observationClasses;
- (NSString*)password;
- (NSString*)host;
- (ISDSAccount*)account;
- (NSString*)certSubject:(BOOL)testing;
- (BOOL)networkUnavailable;
- (void)sendSuccesfullyDone;
- (void)sendFailure;
- (NSString*)taskDescription;

- (void)setErrorDescriptionForError:(NSError*)error;
- (void)failWithDescription:(NSString*)errorDescription;

- (void)start;
- (void)cancel;

@end
