/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  GetOwnerInfoFromLogin.m
//  ISDS
//
//  Created by Petr Hruška on 8/18/10.
//

#import "GetOwnerInfoFromLogin.h"
#import "ISDSAccount.h"

#import "XMLSoapEnvelopeElement.h"
#import "XMLBodyElement.h"
#import "XMLGetOwnerInfoFromLoginResponseElement.h"
#import "XMLDbStatusElement.h"
#import "XMLDbStatusCodeElement.h"
#import "XMLDbStatusMessageElement.h"
#import "XMLOwnerInfoElement.h"
#import "XMLOwnerInfoItemElement.h"


@implementation GetOwnerInfoFromLogin

@synthesize firstName;
@synthesize lastName;
@synthesize firmName;


- (id)init {
	
	const NSUInteger elementCount = 8;
	
	NSMutableArray* elems = [NSMutableArray arrayWithCapacity:elementCount];
	if (!elems) {
		[self dealloc];
		return nil;
	}
	
	@try {
		
		[elems addObject:[[XMLSoapEnvelopeElement alloc] init]];
		[elems addObject:[[XMLBodyElement alloc] init]];
		[elems addObject:[[XMLGetOwnerInfoFromLoginResponseElement alloc] init]];
		[elems addObject:[[XMLOwnerInfoElement alloc] init]];
		[elems addObject:[[XMLDbStatusElement alloc] init]];

		XMLOwnerInfoItemElement* ownerInfoItem = [[XMLOwnerInfoItemElement alloc] init];
		ownerInfoItem.delegate = self;
		[elems addObject:ownerInfoItem];
		
		XMLDbStatusCodeElement* statusCodeElement = [[XMLDbStatusCodeElement alloc] init];
		statusCodeElement.delegate = self;
		[elems addObject: statusCodeElement];
		
		XMLDbStatusMessageElement* statusMessageElement = [[XMLDbStatusMessageElement alloc] init];
		statusMessageElement.delegate = self;
		[elems addObject:statusMessageElement];
		
		
	}
	
	@catch (NSException* exception) {
		[self dealloc];
		return nil;		
	}
	
	self = [super initWithCapacity:5 andElements:elems];
	return self;
}


- (void)dealloc {
	[firstName release];
	[lastName release];
	[super dealloc];
}

- (void)parse:(NSData*)data {
	
	[super parse: data];
	if (self.errorDescription) return;
}

- (NSData*) soapRequest {
	NSString* pattern = [SoapMethod requestPattern];
	NSString* req = @"<ns1:GetOwnerInfoFromLogin><dbDummy></dbDummy></ns1:GetOwnerInfoFromLogin>";
	
	NSString* s = [NSString stringWithFormat: pattern, req];
	NSData* d = [s dataUsingEncoding:NSUTF8StringEncoding];
	return d;
}

- (NSString*)path {
	return  @"/DS/DsManage";
}

- (NSString*)alertTitle:(ISDSAccount*)account {
	NSString* r;
	if (account) {
		NSString* fmt = NSLocalizedString(@"Error while determining databox %@ name", @"Alert title with box name");
		r = [NSString stringWithFormat:fmt, [account boxName]];
	} else 
		r = NSLocalizedString(@"Error while determining databox name", @"Alert title");
	
	return r;
}


@end
