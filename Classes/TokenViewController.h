/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  TokenViewController.h
//  ISDS
//
//  Created by Petr Hruška on 6/30/11.
//

#import <UIKit/UIKit.h>
#import "TokenCell.h"
#import "OtpTask.h"
#import "ProgressViewController.h"

@class TokenViewController;

@protocol TokenViewControllerDelegate

- (void)tokenViewControllerDone:(TokenViewController*)tokenViewController;

@end

@interface TokenViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, OtpTaskDelegate, UITextFieldDelegate, UIActionSheetDelegate, OtpStatusDelegate> {
    
    NSArray* progressToolbarItems_;
}

@property (nonatomic, retain) IBOutlet TokenCell* cell;
@property (nonatomic, retain) IBOutlet UITableView* tableView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem* doneButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem* cancelButton;
@property (nonatomic, retain) NSMutableArray* data;
@property (nonatomic, retain) UITextField* editing;
@property (nonatomic, assign) id<TokenViewControllerDelegate> delegate;
@property (nonatomic, assign) NSUInteger pendingRequests;
@property (nonatomic, retain) IBOutlet ProgressViewController* progressViewController;
@property (nonatomic, readonly) NSArray* progressToolbarItems;
@property (nonatomic, retain) NSArray* initialTasks;
@property (nonatomic, assign) UIViewController* whereToPop;
@property (nonatomic, assign) BOOL popAnimated;
@property (nonatomic, retain) NSString* prompt;
@property (nonatomic, retain) UIActionSheet* actionSheet;
@property (nonatomic, assign) enum ProgressDisplayContext displayContext;


- (void)setupAccounts:(NSArray*)accounts;

- (IBAction)sendSms:(id)sender;
- (IBAction)doneTouched;
- (IBAction)cancelTouched;

@end
