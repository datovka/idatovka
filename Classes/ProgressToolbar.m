/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ProgressToolbar.m
//  ISDS
//
//  Created by Petr Hruška on 1/27/11.
//

#import "ProgressToolbar.h"
#import "ISDSAppDelegate_Shared.h"

@implementation ProgressToolbar

@synthesize label;
@synthesize bar;
@synthesize items;
@synthesize activityIndicator=_activityIndicator;
@synthesize progressView=_progressView;


- (id)initWithText:(NSString*)text andFrame:(CGRect)frame {
	if ((self = [super init])) {
		
		self.progressView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
        self.progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
		self.label = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 260, 12)];
		[self.label release];
		self.label.text = text;
		self.label.backgroundColor = [UIColor clearColor];
		self.label.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
        self.label.textColor = [ISDSAppDelegate_Shared ios7OrLater] ? [UIColor darkTextColor] : [UIColor whiteColor];
		[self.progressView addSubview:self.label];
        
		self.bar = [[UIProgressView alloc] initWithFrame:CGRectMake(25, 15, 260, 10)];
		[self.progressView addSubview:self.bar];
		[self.bar release];
        self.bar.autoresizingMask = UIViewAutoresizingFlexibleWidth;

        UIActivityIndicatorViewStyle activityIndicatorStyle = [ISDSAppDelegate_Shared ios7OrLater] ? UIActivityIndicatorViewStyleGray : UIActivityIndicatorViewStyleWhite;
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:activityIndicatorStyle];
        [self.activityIndicator release];
		self.activityIndicator.frame = CGRectMake(-5, 2, 25, 25);
		self.activityIndicator.hidden = NO;
		[self.activityIndicator startAnimating];
		[self.progressView addSubview:self.activityIndicator];
        
        if (!CGRectIsNull(frame)) self.progressView.frame = frame;
        
		UIBarButtonItem* toolbarItem = [[UIBarButtonItem alloc] initWithCustomView:self.progressView];
		[self.progressView release];
        
		self.items = [NSArray arrayWithObject: toolbarItem];
		[toolbarItem release];
    }
	
	return self;
}

- (void)setFrame:(CGRect)frame
{
    CGRect f = self.progressView.frame;
    f.size.width = frame.size.width;
    self.progressView.frame = f;
}

- (id)initWithText:(NSString*)text {

    return [self initWithText:text andFrame:CGRectNull];
}


- (void)willShowInSplitView
{
    if ([ISDSAppDelegate_Shared ios7OrLater]) return;
    self.label.textColor = [UIColor darkGrayColor];
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
}


- (void)willHideInSplitView
{
    if ([ISDSAppDelegate_Shared ios7OrLater]) return;
    self.label.textColor = [UIColor whiteColor];
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
}


- (void)dealloc
{
	
	[label release];
	[bar release];
	[items release];
    [_activityIndicator release];
    [_progressView release];
    
	[super dealloc];
}

@end
