/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SoapMethod.m
//  ISDS
//
//  Created by Petr Hruška on 8/18/10.
//

#import "SoapMethod.h"
#import "ISDSAccount.h"
#import "SOAPTask.h"

@implementation SoapMethod
@synthesize statusCode;
@synthesize statusMessage;
@synthesize started;


- (NSData*) soapRequest {
	return nil; //this is an abstract method
}

+ (NSString*)requestPattern {
	
	return
	@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
	"<SOAP-ENV:Envelope" 
	" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\""
	" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\""
	" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
	" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\""
	" xmlns:ns1=\"http://isds.czechpoint.cz/v20\">"
	"<SOAP-ENV:Body>"
	//"<ns1:DummyOperation>CZ.NIC iDatovka</ns1:DummyOperation>"
	//"<ns1:SignedSentMessageDownload><dmID>343499</dmID></ns1:SignedSentMessageDownload>"	
	"%@"
	"</SOAP-ENV:Body>"
	"</SOAP-ENV:Envelope>";
}

- (NSString*)path {
	return nil; // abstract method
}

- (BOOL)duplicite:(SoapMethod*)method {
	return [self class] == [method class]; // do not enqueue same class
}

- (void)dealloc {
	[statusCode release];
	[statusMessage release];
	[started release];
	[super dealloc];
}

- (NSString*)alertTitle:(ISDSAccount*)account {
	NSString* r;
	if (account) {
		NSString* fmt = NSLocalizedString(@"Network error while accessing databox %@", @"Alert title with box name");
		r = [NSString stringWithFormat:fmt, [account boxName]];
	} else 
		r = NSLocalizedString(@"Network error", @"Alert title");
	
	return r;
}

+ (NSString*)observationClass
{
    return [NSString stringWithFormat:@"%@.%@.", NSStringFromClass([SOAPTask class]), NSStringFromClass([self class])];
}

@end
