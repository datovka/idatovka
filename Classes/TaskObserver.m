/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  TaskObserver.m
//  ISDS
//
//  Created by Petr Hruška on 1/13/11.
//

#import "TaskObserver.h"


@implementation TaskObserver

@synthesize observedClasses=_observedClasses;
@synthesize observer=_observer;
@synthesize activated=_activated;

- (void)dealloc {
	
	[_observedClasses release];
	[_activated release];
	[super dealloc];
}


- (id)initWithObserver:(id<RequestManagerObserver>)observer forClasses:(NSArray *)observedClasses
{	
	if ((self = [super init])) {
        self.observedClasses = observedClasses;
		self.observer = observer;
	}
	return self;
}

- (BOOL)match:(ISDSTask*)task {
    NSArray* taskClasses = [task observationClasses];
    for (NSString* s in self.observedClasses) {
        for (NSString* taskClass in taskClasses) {
            if ([taskClass hasPrefix:s]) return YES;
        }
    }
	return NO;
}

- (BOOL)matchObservedClasses:(NSArray*)classes {
	return [self.observedClasses isEqualToArray:classes];
}

@end
