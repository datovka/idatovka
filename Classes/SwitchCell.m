/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SwitchCell.m
//  ISDS
//
//  Created by Petr Hruška on 10/15/10.
//

#import "SwitchCell.h"


@implementation SwitchCell

@synthesize switcher=_switcher;
@synthesize delegate;

- (void)setOn:(BOOL)value animated:(BOOL)animated {
	
    _on = value;
	[self.switcher setOn:value animated:animated];
}

- (void)setOn:(BOOL)value {

	_on = value;
	self.switcher.on = value;
}

- (BOOL)on {
	return _on;
}

- (IBAction)switcherValueChanged:(id)sender {
    _on = self.switcher.on;
	[self.delegate switchCell:self switchedTo:_on];
}

- (void)awakeFromNib {
    [super awakeFromNib];
	[self.switcher addTarget:self action:@selector(switcherValueChanged:) forControlEvents:UIControlEventValueChanged];
    _on = self.switcher.on;
}

- (void)viewDidUnload 
{
    self.switcher = nil;
}

- (void)dealloc {
    [_switcher release];
    [super dealloc];
}

@end
