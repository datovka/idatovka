/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Accounts.m
//  ISDS
//
//  Created by Petr Hruška on 7/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Accounts.h"

#define ACCOUNTS_FILE @"Accounts.plist"

static Accounts* globalAccounts;

@implementation Accounts

@synthesize anAccountRemoved;
@synthesize observers=_observers;


- (void)addAccount:(ISDSAccount*) account {
	[accounts addObject: account];

    // call observers
    for (id<AccountsObserver> observer in self.observers) {
        [observer listChanged:AccountsObserverAccountAdded because:account];
    }
}

- (void)removeAccount:(ISDSAccount*)account {
	[accounts removeObject:account];
	self.anAccountRemoved = TRUE;

    // call observers
    for (id<AccountsObserver> observer in self.observers) {
        [observer listChanged:AccountsObserverAccountRemoved because:account];
    }
}

- (ISDSAccount*)accountAtIndex:(NSUInteger)index {
	return (ISDSAccount*)[accounts objectAtIndex: index];
}

- (ISDSAccount*)accountById:(NSString*)databox andTesting:(BOOL)testing {
	for (ISDSAccount* account in accounts)
		if ([account.databox isEqualToString:databox] && account.testing == testing) return account;
	return nil;
}


+ (NSString*)getFullFilename:(NSString*)filename {

	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
//	NSLog(@"documents directory=\"%@\"\n", documentsDirectory);
	return [documentsDirectory stringByAppendingPathComponent:filename];
}


- (BOOL)setupFromFile:(NSString*)filename {
	NSArray* storage = [ NSMutableArray arrayWithContentsOfFile: filename];
	if (storage == nil) { //first execution after installation
		accounts = [NSMutableArray arrayWithCapacity: 0];
		if (accounts == nil) return FALSE;
		[accounts retain];
		return TRUE;
	}
	
	accounts = [NSMutableArray arrayWithCapacity: [storage count]];
	if (!accounts) return FALSE;
	[accounts retain];
	for (id rec in storage) {
		
		ISDSAccount* account = [[ISDSAccount alloc] initFromSecureStorageAndPlist:(NSObject*)rec];
		[accounts addObject: account];
        [account release];
	}
	
	return TRUE;
}

- (BOOL)saveToFile:(NSString*)filename {

	NSUInteger count = [accounts count];
	
	NSMutableArray* storage = [ NSMutableArray arrayWithCapacity: count];
	if (storage == nil) return NO;
	
	for (int index = 0; index < count; index++) {
		ISDSAccount* account = (ISDSAccount*)[accounts objectAtIndex: index];
			NSDictionary* plist = [account plist];
		if (plist == nil) return NO;
		[storage addObject: plist];
	}
	
	return [storage writeToFile: filename atomically:YES];
}

- (void)savePasswords {
	NSUInteger count = [accounts count];

	for (int index = 0; index < count; index++) {
		ISDSAccount* account = (ISDSAccount*)[accounts objectAtIndex: index];
		[account savePassword];
	}
}

- (void)cleanChanged {
	NSUInteger count = [accounts count];
	
	for (int index = 0; index < count; index++) {
		ISDSAccount* account = (ISDSAccount*)[accounts objectAtIndex: index];
		account.configChanged = NO;
	}
}

- (NSUInteger)count {
	return [accounts count];
}

- (id)init {
	
	if ([super init] == nil) return nil;
	
	self.anAccountRemoved = FALSE;
	
	BOOL r = [self setupFromFile:[Accounts getFullFilename:ACCOUNTS_FILE]];
	if (!r) {
		[self dealloc];
		return nil;
	}
	return self;
}

- (void)save {
	[self saveToFile:[Accounts getFullFilename:ACCOUNTS_FILE]];
	[self savePasswords];
	[self cleanChanged];
}

- (void)cleanCache {
    
	for (ISDSAccount* account in accounts) {
		[account cleanCache];
	}
    
    // call observers
    for (id<AccountsObserver> observer in self.observers) {
        [observer cacheCleaned];
    }

}

- (void)releaseCache {
	for (ISDSAccount* account in accounts) {
		[account releaseCache];
	}	
}

- (BOOL)configChanged {
	for (ISDSAccount* account in accounts) {
		if (account.configChanged) return TRUE;
	}
	
	return FALSE;
}


- (void) saveAsNeeded {
	if (self.configChanged || self.anAccountRemoved) [self save];
	self.anAccountRemoved = FALSE;
}

- (NSUInteger)activeCount {
	
	NSUInteger count = 0;
	for (ISDSAccount* account in accounts) {
		if (account.active) count++;
	}
	return count;
}

- (NSArray*)activeAccounts {
	
	NSUInteger count = [self activeCount];
	NSMutableArray *r = [NSMutableArray arrayWithCapacity:count];
	for (ISDSAccount* account in accounts) {
		if (account.active) [r addObject:account];
	}
	
	return r;
}

- (NSInteger)activeAccountIndex:(ISDSAccount*)account {
	
	NSUInteger index = 0;
	for (ISDSAccount* a in accounts) if (a.active) {
		if (a == account) return index;
		index++;
	}
	
	return -1;
}

- (BOOL)needToken
{
    for (ISDSAccount* a in accounts)
        if ([a needsToken]) return YES;
    return NO;
}

- (NSDate*)lastUpdate {
	
	NSDate* t = nil;
	NSArray* activeAccounts = [self activeAccounts];
	for (ISDSAccount* a in activeAccounts) {
		if (t == nil)
			t = a.lastReceivedUpdate;
		else
			t = [t laterDate: a.lastReceivedUpdate];
	}
	return t;
}

- (void)dealloc {
	[accounts release];
    [_observers release];
	[super dealloc];
}

+ (Accounts*)accounts {
	
	if (!globalAccounts) globalAccounts = [[Accounts alloc] init];
	return globalAccounts;
}

- (void)addAccountObserver:(id<ISDSAccountObserver>)observer {
	for (ISDSAccount* a in accounts) {
		[a addObserver:observer];
	}
}

- (void)removeAccountObserver:(id<ISDSAccountObserver>)observer {
	for (ISDSAccount* a in accounts) {
		[a removeObserver:observer];
	}
}

- (void)addListObserver:(id<MsgListObserver>)observer {
	
	for (ISDSAccount* a in accounts) {
		[a addReceivedListObserver:observer];
		[a addSentListObserver:observer];
	}
}

- (void)removeListObserver:(id<MsgListObserver>)observer {
	
	for (ISDSAccount* a in accounts) {
		[a removeReceivedListObserver:observer];
		[a removeSentListObserver:observer];
	}	
}


- (void)addAccountsObserver:(id<AccountsObserver>)observer
{
    if (!self.observers)
        self.observers = [NSMutableArray arrayWithObject:observer];
    else
        [self.observers addObject:observer];
}


- (void)removeAccountsObserver:(id<AccountsObserver>)observer
{
    [self.observers removeObject:observer];
}


@end
