/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  MsgList.h
//  ISDS
//
//  Created by Petr Hruška on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISDSMessage.h"
#import "ISDSObject.h"
#import "GetListOfSentMessages.h"
#import "GetListOfReceivedMessages.h"


/*
 
 Format dat na disku:
 
 Asociativni pole, jehoz jediny klic messages obsahuje pole s jednoznacnymi
 identifikatory zprav.
 
 Ve vlastnosti messages je asociativni pole, kde klic je identifikator zpravy
 na portale ISDS a hodnota je reference na objekt typu Message (validni nebo
 nevalidni podle pritomnosti na disku) nebo null.
 
 */

@class MsgList;

@class ReceivedMessageDownload;
@class SignedSentMessageDownload;

@interface MsgList : ISDSObject {

	NSString* name;
	BOOL incoming;
	NSMutableDictionary* messages;
	NSUInteger unread;
	
	NSMutableArray* index;
}

@property (nonatomic, retain) NSString* name;
@property (nonatomic, assign) BOOL incoming;
@property (nonatomic, retain) NSMutableDictionary* messages;
@property (nonatomic, assign) NSUInteger unread;
@property (nonatomic, readonly) NSUInteger count;

@property (nonatomic, retain) NSMutableArray* index;

- (id)initFromFileWithParent:(ISDSObject*)parent name:(NSString*)name incoming:(BOOL)incoming;
- (id)initWithSentMessages:(GetListOfSentMessages*)method andParent:(ISDSObject*)parent name:(NSString*)name incoming:(BOOL)incoming;
- (id)initWithReceivedMessages:(GetListOfReceivedMessages*)method andParent:(ISDSObject*)parent name:(NSString*)name incoming:(BOOL)incoming;

- (BOOL)updateFromSentMessages:(GetListOfSentMessages*)method;
- (BOOL)updateFromReceivedMessages:(GetListOfReceivedMessages*)method;
- (BOOL)updateFromReceivedMessage:(ReceivedMessageDownload*)method;
- (BOOL)updateFromSentMessage:(SignedSentMessageDownload*)method;
- (ISDSMessage*)messageAtIndex:(NSUInteger)i;
- (BOOL)decrementUnread;
- (void)callMsgObservers:(ISDSMessage*)msg;

- (NSUInteger)count;
- (ISDSMessage*)message:(NSString*)msgId;


@end


