/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Attachment.m
//  ISDS
//
//  Created by Petr Hruška on 5/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Attachment.h"
#import "ISDSAppDelegate_Shared.h"


@implementation Attachment

@synthesize data;
@synthesize name;

- (id)initWithName:(NSString*)name_ andContent:(NSData*)data_ {
	if ((self = [super init])) {
		self.name = name_;
		self.data = data_;
	}
	return self;
}

- (void)dealloc {
	[ data release ];
	[ name release ];
	[ super dealloc ];
}

- (NSString*)mimeType {

	ISDSAppDelegate_Shared *isdsAppDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication ] delegate ];
	NSDictionary *mimeTypes = isdsAppDelegate.mimeTypes;
	NSString *fileExtension = [[self.name pathExtension] lowercaseString];
	
	NSString* t = [ mimeTypes objectForKey: fileExtension ];
	if (t == nil)
		return @"application/octet-stream";
	else
		return t;
}


@end
