/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLAccumulatorElement.m
//  InputTest
//
//  Created by Petr Hruška on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "XMLAccumulatorElement.h"


@implementation XMLAccumulatorElement

@synthesize accumulator;


- (BOOL) matchNamespace:(NSString*)namespaceURI element:(NSString*)elementName andAttributes:(NSDictionary*)attributes {
	return NO;
}

- (BOOL)canPush:(XMLElement*)parent {
	return [super canPush:parent] && self.accumulator == nil;
}


- (void) appendChunk:(NSString*)string {
	
	if (self.accumulator)
		[self.accumulator appendString:string];
	else {
		NSMutableString* mutableString = [NSMutableString stringWithString:string];
		self.accumulator = mutableString;
	}
}

- (void)push {
	
	if (self.accumulator) {
		NSException* e = [NSException
						  exceptionWithName:@"MultiplePushException"
						  reason:@"XMLAccumulatorElement object was pushed more than once."
						  userInfo:nil];
		@throw e;
	}
	
	self.accumulator = [NSMutableString stringWithString:@""];
}

- (void)pop {
	self.accumulator = nil;
	[super pop];
}



@end
