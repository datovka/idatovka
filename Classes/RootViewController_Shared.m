/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  RootViewController_Shared.m
//  ISDS
//
//  Created by Petr Hruška on 2/28/11.
//

#import "RootViewController_Shared.h"
#import "Accounts.h"
#import "NetworkUpdater.h"
#import "getOwnerInfoFromLogin.h"
#import "ISDSDefaults.h"
#import "SOAPTask.h"
#import "ISDSAppDelegate_Shared.h"

@implementation RootViewController_Shared

@synthesize cell;
@synthesize configButton;
@synthesize refreshButton;
@synthesize lastUpdate;
@synthesize progressToolbar;
@synthesize idleToolbarItems;
@synthesize pendingTasks;
@synthesize observedClasses;
@synthesize boxesTableView;

#pragma mark -
#pragma mark Auxillary methods

- (DataboxCell*)getDataboxCell:(UITableView *)tableView {
	
    static NSString *CellIdentifier = @"Cell";
	
    DataboxCell *databoxCell = (DataboxCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.cell == nil) {
		[[NSBundle mainBundle] loadNibNamed:@"DataboxCell" owner:self options:nil];
		databoxCell = self.cell;
		[[databoxCell retain] autorelease];
		self.cell = nil;
    }
	
	return databoxCell;
}

- (void)scheduleUpdate
{
    NetworkUpdater* networkUpdater = [NetworkUpdater networkUpdater];
    [networkUpdater updateMessages];
}

- (void)setupCookiesAndScheduleUpdate
{
    UIViewController* topViewController = [self.navigationController topViewController];
    if ([topViewController isKindOfClass:[TokenViewController class]]) return;
    
    TokenViewController* tokenViewController = [[NetworkUpdater networkUpdater] tokenViewController];
    if (tokenViewController) {
        self.title = NSLocalizedString(@"Accounts", @"Accounts view title");
        tokenViewController.delegate = self;
        [self.navigationController pushViewController:tokenViewController animated:TRUE];
    } 
    else {
        [self scheduleUpdate];
    }
}


- (void)createProgressToolbarItems {
	
	if (!self.progressToolbar)
		self.progressToolbar = [[[ProgressToolbar alloc] initWithText:@""] autorelease];
}


- (void)setupProgressToolbarItems {
	
	[self createProgressToolbarItems];
	
	if (self.toolbarItems != self.progressToolbar.items)
		[self setToolbarItems:self.progressToolbar.items animated:YES];
}


- (NSString*)friendlyLastUpdateDate {
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	
	Accounts* accounts = [Accounts accounts];
	NSDate* date = [accounts lastUpdate];
	
	NSString* tmp;
	if (date)
		tmp = [dateFormatter stringFromDate:date];
	else
		tmp = @"-";
	
	[dateFormatter release];
	
	return tmp;
}


- (void)setLastUpdate {
	NSString* friendlyDate = [self friendlyLastUpdateDate];
	self.lastUpdate.text = [NSString
							stringWithFormat:NSLocalizedString(@"last update: %@", @"Last update format string"), friendlyDate];
}

- (void)setupIdleToolbarItems {
	
	if (!self.idleToolbarItems) {
		UIBarButtonItem* lastUpdateItem = [[UIBarButtonItem alloc] initWithCustomView:self.lastUpdate];
		self.idleToolbarItems = [NSArray arrayWithObjects: self.refreshButton, lastUpdateItem, nil];
		[lastUpdateItem release];
	}
	if (self.toolbarItems != self.idleToolbarItems) {
		[self setToolbarItems:self.idleToolbarItems animated:YES];
	}
	
	// refresh value
	[self setLastUpdate];
}

- (void)registerTaskObserver {
	RequestManager *rm = [RequestManager requestManager];
	if (!self.observedClasses) {
		self.observedClasses = [NSArray arrayWithObjects:
								[GetOwnerInfoFromLogin observationClass],	
								[GetListOfSentMessages observationClass],
								[GetListOfReceivedMessages observationClass], 
								nil];
	}
	
	[rm addObserver:self forClasses:self.observedClasses];
}


- (void)unregisterTaskObserver {
	RequestManager *rm = [RequestManager requestManager];
	[rm removeObserver:self forClasses:self.observedClasses];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self registerTaskObserver];
}

- (void)setLabelForMethod:(SoapMethod*)method {
	
	[self createProgressToolbarItems];
	
	if ([method isKindOfClass:[GetOwnerInfoFromLogin class]]) {		
		self.progressToolbar.label.text = NSLocalizedString(@"Determining account name", @"Determining account name");
	} else if ([method isKindOfClass:[GetListOfReceivedMessages class]]) {
		self.progressToolbar.label.text = NSLocalizedString(@"Downloading list of received messages", @"Downloading list of received messages");
	} else if ([method isKindOfClass:[GetListOfSentMessages class]]) {
		self.progressToolbar.label.text = NSLocalizedString(@"Downloading list of sent messages", @"Downloading list of sent messages");
	}
}


#pragma mark -
#pragma mark UITableViewController methods

- (void)viewDidLoad {
    
	self.title = NSLocalizedString(@"Accounts", @"Accounts view title");
	self.configButton.accessibilityLabel = NSLocalizedString(@"Application Settings", nil);
	self.navigationItem.leftBarButtonItem = self.configButton;
    
    if ([ISDSAppDelegate_Shared ios7OrLater]) {
        self.lastUpdate.textColor = [UIColor darkTextColor];
    }

    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[self.navigationController setToolbarHidden:FALSE];
	if (!self.pendingTasks) {
		[self setupIdleToolbarItems];
	}
	
	Accounts* accounts = [Accounts accounts];
	[accounts addAccountObserver:self];
	[accounts addListObserver:self];
    [accounts addAccountsObserver:self];
	[self.boxesTableView reloadData];
}


- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	Accounts *accounts = [Accounts accounts];
    [accounts removeAccountsObserver:self];
	[accounts removeAccountObserver:self];
	[accounts removeListObserver:self];
}

#pragma mark -
#pragma mark UITableViewDelegate methods


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 46;
}


#pragma mark -
#pragma mark UITableViewDataSource methods
	
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	Accounts* accounts = [Accounts accounts];
	return [accounts activeCount];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSUInteger index = indexPath.section;
	BOOL received = indexPath.row == 0;
	
	DataboxCell *databoxCell = [self getDataboxCell:tableView];
	
	Accounts* accounts = [Accounts accounts];
	NSArray* activeAccounts = [accounts activeAccounts];
	ISDSAccount* account = (ISDSAccount*)[activeAccounts objectAtIndex: index];
	databoxCell.caption.text = received ?
	NSLocalizedString(@"Received messages", @"Received messages")
	: NSLocalizedString(@"Sent messages", @"Sent messages");
	
	NSInteger count = received ? [account unreadReceivedCount] : [account unreadSentCount];
	
	if (count < 0) {
		databoxCell.detail.text = @"?";
	} else {
		databoxCell.detail.text = [NSString stringWithFormat: @"%ld", (long)count];
	}
	
    return databoxCell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	Accounts* accounts = [Accounts accounts];
	NSArray* activeAccounts = [accounts activeAccounts];
	ISDSAccount* account = (ISDSAccount*)[activeAccounts objectAtIndex: section];
	
	return [account boxName];
}

#pragma mark -
#pragma mark PinViewControllerDelegate methods

- (void)pinVerified {
	[self dismissModalViewControllerAnimated:TRUE];
	ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
	if (defaults.autoUpdate) [self setupCookiesAndScheduleUpdate];
}

#pragma mark -
#pragma mark ISDSAccountObserver methods

- (void)account:(ISDSAccount*)account changedBecause:(ISDSAccountChangeReason)reasonFlags {

    if (reasonFlags & (ISDSAccountChangeReasonActive | ISDSAccountChangeReasonInactive)) {
        [self.boxesTableView reloadData];
    } else if (reasonFlags & ISDSAccountChangeReasonBoxName) {
        Accounts* accounts = [Accounts accounts];
        NSInteger index = [accounts activeAccountIndex:account];
        if (index < 0) return;
        NSIndexSet* indexSet = [NSIndexSet indexSetWithIndex:index];
        [self.boxesTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark -
#pragma mark MsgListObserver methods

- (void)listChanged:(MsgList*)list withAccount:(ISDSAccount*)account {
	
	Accounts *accounts = [Accounts accounts];
	
	NSInteger section = [accounts activeAccountIndex:account];
	if (section < 0) return;
	
	NSUInteger indexes[2];
	indexes[0] = section;
	indexes[1] = list.incoming ? 0 : 1;
	NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
	
	[self.boxesTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -
#pragma mark AccountsObserver methods

- (void)cacheCleaned
{
    [self setLastUpdate];
}

- (void)listChanged:(enum AccountsObserverAction)reason because:(ISDSAccount*)account
{
    switch (reason) {
        case AccountsObserverAccountAdded:
            [account addObserver:self];
            [account addSentListObserver:self];
            [account addReceivedListObserver:self];
            break;
        case AccountsObserverAccountRemoved:
            [account removeReceivedListObserver:self];
            [account removeSentListObserver:self];
            [account removeObserver:self];
            break;
    }
    
    [self.boxesTableView reloadData];
}

#pragma mark -
#pragma mark RequestManagerObserver methods

- (void)progressUpdate:(float)progress estimate:(NSTimeInterval)estimate elapsed:(NSTimeInterval)elapsed {
	
	if (estimate > PROGRESSBAR_THRESHOLD || elapsed > PROGRESSBAR_THRESHOLD) [self setupProgressToolbarItems];
	self.progressToolbar.bar.progress = progress;
}


- (void)statusUpdate:(enum RMStatus)status forTask:(ISDSTask*)task {
	
	//	NSLog(@"StatusUpdate: %d, %x, pendingTasks=%d", status, task, self.pendingTasks);
	
	switch (status) {
		case RMQueued:
			self.pendingTasks = self.pendingTasks + 1;
			break;
		case RMStarted:
			[self setLabelForMethod:[(SOAPTask*)task method]];
			break;
		case RMDone:
		case RMCancelled:
		case RMUnregistered:
        case RMFailed:
            self.pendingTasks = self.pendingTasks - 1;
            if (!self.pendingTasks) [self setupIdleToolbarItems];
			break;
	}
}

#pragma mark -
#pragma mark IBActions

- (IBAction)refresh:(id)sender {
	
	[self setupCookiesAndScheduleUpdate];
}

#pragma mark -
#pragma mark dealloc

- (void)dealloc {
    [self unregisterTaskObserver];
	[configButton release];
	[refreshButton release];
	[lastUpdate release];
	[progressToolbar release];
	[idleToolbarItems release];
	[observedClasses release];
	[boxesTableView release];
	
    [super dealloc];
}

#pragma mark - TokenViewControllerDelegate

- (void)tokenViewControllerDone:(TokenViewController*)tokenViewController
{
    [self scheduleUpdate];
}


@end
