/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AccountCache.m
//  ISDS
//
//  Created by Petr Hruška on 7/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AccountCache.h"
#import "ISDSAccount.h"
#import "Estimate.h"

@implementation AccountCache

@synthesize account;

@synthesize lastReceivedUpdate;

@synthesize ownerInfo;
@synthesize received;
@synthesize sent;
@synthesize estimates;


// private

- (void)callAccountObservers:(ISDSAccountChangeReason)reasonFlags
{
	[self.account callObservers:reasonFlags];
}

- (void)callListObservers:(MsgList*)list {
	[self.account callListObservers:list];
}

- (void)callMsgObservers:(ISDSMessage*)msg {
	[self.account callMsgObservers:msg];
}

- (BOOL)loadSent {
	if (self.sent == (MsgList*)[NSNull null])
		self.sent = [[MsgList alloc] initFromFileWithParent:self name:@"Sent/" incoming:NO];
	return self.sent != nil;
}

- (BOOL)loadReceived {
	if (self.received == (MsgList*)[NSNull null]) {
		self.received = [[MsgList alloc] initFromFileWithParent:self name:@"Received/" incoming:YES];
    }
    
	return self.received != nil;
}

- (BOOL)loadOwnerInfo {
	if (self.ownerInfo == (OwnerInfo*)[NSNull null])
		self.ownerInfo = [[OwnerInfo alloc] initFromFileWithParent:self];
	return self.ownerInfo != nil;		
}

- (BOOL)loadEstimates {
	if (self.estimates == (Estimates*)[NSNull null])
		self.estimates = [[Estimates alloc] initFromFileWithParent:self];
	return self.estimates != nil;		
}

// public

- (id)initWithAccount:(ISDSAccount*)account_ {
	
	if ((self = [super initWithParent:nil])) {
		
		self.account = account_;
		self.ownerInfo = (OwnerInfo*)[NSNull null];
		self.received = (MsgList*)[NSNull null];
		self.sent = (MsgList*)[NSNull null];		
		self.estimates = (Estimates*)[NSNull null];
	}
	
	return self;
}

- (id)initFromFileWithAccount:(ISDSAccount*)account_ {
	
	if ((self = [self initWithAccount:account_])) {
		if (![self updateFromFile]) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (void)clean {
	
	self.ownerInfo = (OwnerInfo*)[NSNull null];
	self.received = (MsgList*)[NSNull null];
	self.sent = (MsgList*)[NSNull null];
	self.estimates = (Estimates*)[NSNull null];
	[self removeFileAndDir];
	[self callAccountObservers:ISDSAccountChangeReasonBoxName];
}

- (BOOL)updateOwnerInfo:(GetOwnerInfoFromLogin*)method {
	
	if ([self loadOwnerInfo])
		return [self.ownerInfo updateFromMethod:method];
	else {
		self.ownerInfo = [[OwnerInfo alloc] initWithParent:self andMethod:method];
		if (self.ownerInfo) {
            // tahle metoda se volala uz z konstruktoru self.ownerInfo, ale
            // v te dobe byla property porad jeste nil, takze se nic nezmenilo
            [self callAccountObservers:ISDSAccountChangeReasonBoxName];
			return TRUE;
		} else 
			return FALSE;
	}
}

- (NSInteger)unreadSentCount {
	if (![self loadSent]) return -1;
	return self.sent.unread;
}

- (NSInteger)unreadReceivedCount {
	if (![self loadReceived]) return -1;
	return self.received.unread;
}

- (void) dealloc {
	[account release];
	
	[lastReceivedUpdate release];
	
	[sent release];
	[received release];
	[ownerInfo release];
	[estimates release];

	[super dealloc];
}

- (BOOL)updateFromPlist:(NSDictionary*)plist {
	self.lastReceivedUpdate = [plist objectForKey:@"lastReceivedUpdate"];
	return TRUE;
}

- (NSString*)dir {
	if (!self.account.databox || [self.account.databox isEqualToString:@""])
		return nil;
	NSString* filename = [NSString stringWithFormat: @"AccountCache-%@/", self.account.databox];
	if (!filename) return nil;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	if (!paths) return nil;
	NSString *documentsDirectory = [paths objectAtIndex:0];
	if (!documentsDirectory) return nil;
	return [NSString stringWithFormat:@"%@/%@", documentsDirectory, filename];	
}

- (NSString*)filename {
	return @"Cache.plist";
}

- (BOOL)updateSentMessageList:(GetListOfSentMessages*)method {
	BOOL r;
	if ([self loadSent]) {
		r = [self.sent updateFromSentMessages: method];
	} else {
		self.sent = [[MsgList alloc] initWithSentMessages:method andParent:self name:@"Sent/" incoming:NO];
		r = self.sent != nil;
	}
	
	if (r) {
		self.lastReceivedUpdate = [NSDate date];
		NSDictionary* plist = [NSDictionary 
							   dictionaryWithObject:self.lastReceivedUpdate forKey:@"lastReceivedUpdate"];
		[self writeToFile:plist];
		[self callListObservers:self.sent];
	}
	return r;
}

- (BOOL)updateReceivedMessageList:(GetListOfReceivedMessages*)method {

	BOOL r;
	if ([self loadReceived]) {
		r = [self.received updateFromReceivedMessages: method];
	} else {
		self.received = [[MsgList alloc] initWithReceivedMessages:method andParent:self name:@"Received/" incoming:YES];
		r = self.received != nil;
	}
	
	if (r) {
		self.lastReceivedUpdate = [NSDate date];
		NSDictionary* plist = [NSDictionary 
							   dictionaryWithObject:self.lastReceivedUpdate forKey:@"lastReceivedUpdate"];
		[self writeToFile:plist];
		[self callListObservers:self.received];
	}
	
	return r;
}

- (BOOL)updateEstimate:(ISDSTask*)task withDelay:(NSTimeInterval)delay {
	
	if (![self loadEstimates]) {
		self.estimates = [[Estimates alloc] initWithParent:self];
		if (!self.estimates) return FALSE;
	}
	
	return [self.estimates updateDelay:delay forTask:task];
}

- (BOOL)updateFromReceivedMessage:(ReceivedMessageDownload*)method {
	
	if (![self loadReceived]) {
		return FALSE;
	}
	
	return [self.received updateFromReceivedMessage:method];
}

- (BOOL)updateFromSentMessage:(SignedSentMessageDownload*)method {
	
	if (![self loadSent]) {
		return FALSE;
	}
	
	return [self.sent updateFromSentMessage:method];
}

- (NSTimeInterval)estimate:(ISDSTask*)task {
	if (![self loadEstimates]) {
		self.estimates = [[Estimates alloc] initWithParent:self];
		if (!self.estimates) return [Estimate defaultDelayForTask:task];
	}
	
	return [self.estimates delayForTask:task];
}

@end
