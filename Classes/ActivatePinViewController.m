/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ActivatePINViewController.m
//  ISDS
//
//  Created by Petr Hruška on 10/18/10.
//

#import "ActivatePinViewController.h"
#import "ISDSDefaults.h"

@implementation ActivatePinViewController

@synthesize pinEditNavigationController;
@synthesize pinEditView;
@synthesize pin;

- (void)viewDidLoad {
    [super viewDidLoad];
	
	PinEditViewController *pinEditViewController = [[PinEditViewController alloc] initWithNibName:@"PinEditViewController" bundle:nil];
    pinEditViewController.caption = NSLocalizedString(@"Enter PIN", @"Enter PIN caption");
	self.pinEditNavigationController = [[UINavigationController alloc] initWithRootViewController:pinEditViewController];
    [pinEditViewController release];
	[self.view addSubview:self.pinEditNavigationController.view];
    [self.pinEditNavigationController release];
	self.pinEditNavigationController.view.frame = self.pinEditView.frame;
	[self.pinEditNavigationController setNavigationBarHidden:TRUE];
	self.pinEditNavigationController.delegate = self;

	self.title = NSLocalizedString(@"Activate PIN", @"Activate PIN caption");
	self.pin = nil;
}

- (void)dealloc {
	[pinEditNavigationController release];
	[pinEditView release];
	[pin release];
    [super dealloc];
}

- (void)lastDigitEntered:(id)sender {

	PinEditViewController* pinEditViewController = (PinEditViewController*)[self.pinEditNavigationController topViewController];
	if (self.pin == nil) {
		// first form, save pin
		self.pin = pinEditViewController.pin;
	
		// push second form
		PinEditViewController* nextPinEditViewController = [[PinEditViewController alloc] initWithNibName:@"PinEditViewController" bundle:nil];
		[self.pinEditNavigationController pushViewController:nextPinEditViewController animated:YES];
		nextPinEditViewController.caption = NSLocalizedString(@"Re-enter PIN", @"Re-enter PIN caption");
		[nextPinEditViewController release];
	} else {
		if ([pinEditViewController.pin isEqualToString:self.pin]) {
			
			// pins are equal, we are done
			ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
			defaults.pin = self.pin;		
			[self.navigationController popViewControllerAnimated:TRUE];
			
		} else {
			
			// reject pins and start again
			self.pin = nil;
			[self.pinEditNavigationController popToRootViewControllerAnimated:YES];
			PinEditViewController* pinEditViewController = (PinEditViewController*)[self.pinEditNavigationController topViewController];
			[pinEditViewController cleanPin];
			pinEditViewController.advice = NSLocalizedString(@"Second PIN does not match, please enter pins again.", @"Second PIN wrong advice");
		}	
	}
					
}


- (void)navigationController:(UINavigationController *)navigationController
	  willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
	
	PinEditViewController* pinEditViewController = (PinEditViewController*)viewController;
	pinEditViewController.delegate = self;
	[pinEditViewController viewWillAppear:animated];
}

@end
