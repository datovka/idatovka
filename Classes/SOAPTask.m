/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSConnection.m
//  InputTest
//
//  Created by Petr Hruška on 7/28/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SOAPTask.h"
#import "SignedSentMessageDownload.h"
#import "ISDSAccount.h"
#import "CertificateManager.h"

#define ISDS_WS1_HOST_TESTING               @"ws1.czebox.cz"
#define ISDS_WS1_HOST_RELEASE               @"ws1.mojedatovaschranka.cz"
#define ISDS_OTP_HOST_TESTING               @"www.czebox.cz"
#define ISDS_OTP_HOST_RELEASE               @"www.mojedatovaschranka.cz"

@implementation SOAPTask

@synthesize data=_data;
@synthesize method=_method;
//@synthesize response=_response;

- (NSString*)host
{
    ISDSAccount* account = [self account];
    if (!account || account.authType == ISDSAccountAuthPass)
        return self.testing ? ISDS_WS1_HOST_TESTING : ISDS_WS1_HOST_RELEASE;
    else
        return self.testing ? ISDS_OTP_HOST_TESTING : ISDS_OTP_HOST_RELEASE;
}

- (id)initWithAccount:(ISDSAccount*)account andMethod:(SoapMethod*)method {
	
	if ((self = [super initWithAccount:account])) {
        self.method = method;
	}
	return self;
}

- (void)dealloc {
	[_data release];
	[_method release];
	[super dealloc];
}

+ (NSString*)observationClass
{
    return NSStringFromClass([self class]);
}

- (NSArray*)observationClasses
{
    return [NSArray arrayWithObjects:[SOAPTask observationClass], [[self.method class] observationClass], nil];
}                                                               
                                                               

- (NSURLRequest*)request {
	
    self.errorDescription = nil;
	
	NSString* host = [self host];
	self.method = _method;
    
    ISDSAccount* account = [self account];
    NSString* path;
    switch (account.authType) {
        case ISDSAccountAuthPass:
            path = [self.method path];
            break;
            
        case ISDSAccountAuthHotp:
        case ISDSAccountAuthTotp:
            path = [NSString stringWithFormat:@"/apps%@", [self.method path]];
            break;
    }
    
	NSURL* url = [[NSURL alloc] initWithScheme:@"https" host:host path:path];
	
	NSMutableURLRequest* req = [NSMutableURLRequest 
						 requestWithURL:url
						 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
						 timeoutInterval:60
						 ];
    [url release];
	[req setHTTPMethod:@"POST"];
    
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    if (account.otpCookie) [cookieStorage setCookie:account.otpCookie];

		
	NSData* reqData = [self.method soapRequest];
	[req setHTTPBody: reqData];

	return req;
}

#pragma mark - USURLConenction delegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"didFailWithError: domain=%@", error.domain);
	NSLog(@"                  code=%ld", (long)error.code);
	for (id key in error.userInfo) {
		NSLog(@"                  key=%@, value=%@", key, [error.userInfo objectForKey:key]);
	}

	[self setErrorDescriptionForError:error]; 
    [self.delegate taskFailed:self];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	/* DEBUG */
	//NSString* text = [[NSString alloc] initWithData:self.data encoding:NSISOLatin2StringEncoding];
	//NSLog(@"%@", text);
	//[text release];
	
	[self.method parse:self.data];
    
	if (self.method.errorDescription) {
        self.errorDescription = self.method.errorDescription;
        [self sendFailure];
	} else {
        [self sendSuccesfullyDone];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)chunk {

	[self.data appendData:chunk];
}

- (NSURLRequest *)connection:(NSURLConnection *)connection 
			 willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse {
    
	return request;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	
	NSURLProtectionSpace* protectionSpace = challenge.protectionSpace;
	
    if ([[protectionSpace authenticationMethod] isEqual:NSURLAuthenticationMethodHTTPBasic]
        || [[protectionSpace authenticationMethod] isEqual:NSURLAuthenticationMethodServerTrust]
        || [[protectionSpace authenticationMethod] isEqual:NSURLAuthenticationMethodDefault]) {
        
		if ([challenge previousFailureCount] == 0) {
			NSURLCredential* credential = [NSURLCredential
										   credentialWithUser:self.databox
										   password:[self password]
										   persistence:NSURLCredentialPersistenceNone
										   ];
			[[challenge sender] useCredential:credential forAuthenticationChallenge:challenge];
		} else {
            [[challenge sender] cancelAuthenticationChallenge:challenge];
		}
	} else {
        [super connection:connection didReceiveAuthenticationChallenge:challenge];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)_response {
    
	NSHTTPURLResponse* response = (NSHTTPURLResponse*)_response;
    
    if ([response statusCode] != 200) {
		// NSLog(@"connectionDidfinishLoading: code=%d", [self.response statusCode]);
		self.errorDescription = NSLocalizedString(@"Server rejected request.", @"Server rejected request.");
        [self cancel];
        [self sendFailure];
	} else {
        
        if ([response expectedContentLength] != NSURLResponseUnknownLength) {
            self.data = [NSMutableData dataWithCapacity:[response expectedContentLength]];
        } else {
            self.data = [NSMutableData data];
        }
        
        if (!self.data) {
            self.errorDescription = NSLocalizedString(@"Not enough memory", nil);
            [self cancel];
            [self sendFailure];
        }
    }
}

@end
