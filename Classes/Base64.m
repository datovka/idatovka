/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Base64.m
//  InputTest
//
//  Created by Petr Hruška on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Base64.h"


@implementation Base64

int charIndex(char c) {
	if (c >= 'A' && c <= 'Z') return c - 'A';
	if (c >= 'a' && c <= 'z') return (c - 'a') + 26;
	if (c >= '0' && c <= '9') return (c - '0') + 52;
	if (c == '+') return 62;
	if (c == '/') return 63;
	
	return -1;
}

+ (NSData*)decode:(NSString*)text {
	
	const char* stringBytes = [text cStringUsingEncoding:NSASCIIStringEncoding];
	NSUInteger maxLength = 3 * ((strlen(stringBytes) + 3) / 4);
	NSMutableData* data = [NSMutableData dataWithLength:maxLength];
	char* buffer = [data mutableBytes];
	
	const char* c = stringBytes;
	unsigned int i = 0;
	unsigned int j = 0;
	unsigned long x = 0;
	while (*c) {
		int v = charIndex(*c);
		if (v >= 0) {
			x = x << 6;
			x += (unsigned int)v;
			j++;
			if (j == 4) {
				buffer[i + 0] = (x >> 16) & 0xff;
				buffer[i + 1] = (x >> 8) & 0xff;
				buffer[i + 2] = x & 0xff;
				i += 3;
				j = 0;
				x = 0;
			}
			
		}
		c++;
	}
	
	int len = i + (j * 6) / 8;
	if (j) {
		x = x << (4 - j) * 6;
		if (i + 0 < len) buffer[i + 0] = (x >> 16) & 0xff;
		if (i + 1 < len) buffer[i + 1] = (x >> 8) & 0xff;
	}
	
	[data setLength:len];
	return data;
}


+ (NSString*)encodeData:(NSData*)data
{
    
    const char* src = [data bytes];
    const char* chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    const NSUInteger length = [data length];
    
    NSUInteger triplets = (length + 2) / 3;
    char* dst = malloc(triplets * 4);
    if (!dst) return nil;
    
    NSUInteger B = 0;
    NSUInteger b = 0;
    NSUInteger j = 0;
    for (NSUInteger i = 0; i < length; i++) {
        B = (B << 8) + src[i];
        b += 8;
        while (b >= 6) {
            NSUInteger index = (B >> (b - 6)) & 0x03f;
            dst[j++] = chars[index];
            b -= 6;
        }
    }
    
    if (b > 0) {
        B = B << (6 - b);
        NSUInteger index = B & 0x03f;
        dst[j++] = chars[index];
    }

    while (j < 4 * triplets) {
        dst[j++] = '=';
    }
    
    NSString* r = [[NSString alloc] initWithBytes:dst length:4 * triplets encoding:NSUTF8StringEncoding];
    free(dst);
    return [r autorelease];
}


+ (NSString*)encodeString:(NSString*)string
{
    NSData* data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return [Base64 encodeData:data];
}


@end
