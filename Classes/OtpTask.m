/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  OtpTask.m
//  ISDS
//
//  Created by Petr Hruška on 7/6/11.
//

#import "OtpTask.h"
#import "CertificateManager.h"
#import "Base64.h"

#define ISDS_ENV_TESTING    @"www.czebox.cz"
#define ISDS_ENV_RELEASE    @"www.mojedatovaschranka.cz"

@implementation OtpTask

@synthesize status=_status;
@synthesize otpDelegate=_otpDelegate;

- (void)dealloc
{
    [_status release];
    [super dealloc];
}

- (id)initWithOtpStatus:(OtpStatus*)status
{
    if ((self = [super initWithAccount:status.account])) {
        self.status = status;
    }
    
    return self;
}

- (NSString*)host
{
    return self.testing ? ISDS_ENV_TESTING : ISDS_ENV_RELEASE;
}

- (NSString*)base64Message:(NSHTTPURLResponse*)response withDefault:(NSString*)defaultMessage
{
    NSString* messageKey = [[response allHeaderFields] objectForKey:@"X-Response-Message-Code"];
    if (!messageKey)
        return defaultMessage;
    return NSLocalizedString(messageKey, nil);
}


- (NSString*)urlString
{
    return nil; // abstract method    
}


- (NSString*)otpPassword
{
    return nil; // abstract method
}


- (NSURLRequest*)request
{
    NSString* urlString = [self urlString];
    if (!urlString) return nil;
    
    NSURL* url = [NSURL URLWithString:urlString];
    if (!url) return nil;
    
    NSMutableURLRequest* r = [NSMutableURLRequest requestWithURL:url];
    if (!r) return nil;
    [r setHTTPMethod:@"POST"];
    NSString* credentials = [Base64 encodeString:[NSString stringWithFormat:@"%@:%@", self.databox, [self otpPassword]]];
    NSString* authorizationValue = [NSString stringWithFormat:@"Basic %@", credentials];
    [r addValue:authorizationValue forHTTPHeaderField:@"Authorization"];
    [self removeCookies];
    
    return r;
}

#pragma mark - NSURLConnection delegate

- (void)sendResult
{
    if (self.errorDescription) {
        [self sendFailure];
        [self.otpDelegate taskFailed:self];
    } else {
        [self sendSuccesfullyDone];
        [self.otpDelegate taskSuccessfullyDone:self];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [self setErrorDescriptionForError:error];
    [self sendResult];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self sendResult];
}

- (NSURLRequest *)connection:(NSURLConnection *)connection 
			 willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse {
    
    // fake redirect
    if (!redirectResponse) return request;
    
	return nil;
}

@end
