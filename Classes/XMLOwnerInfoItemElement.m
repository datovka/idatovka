/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLOwnerInfoItemElement.m
//  ISDS
//
//  Created by Petr Hruška on 8/18/10.
//

#import "XMLOwnerInfoItemElement.h"
#import "XMLOwnerInfoElement.h"


@implementation XMLOwnerInfoItemElement

@synthesize delegate;
@synthesize elementName;


- (BOOL) matchNamespace:(NSString*)namespaceURI element:(NSString*)_elementName andAttributes:(NSDictionary*)attributes {
	
	if (![namespaceURI isEqualToString: @"http://isds.czechpoint.cz/v20"]) return FALSE;
	
	self.elementName = _elementName;
	
	if ([_elementName isEqualToString: @"pnFirstName"]) return TRUE;
	if ([_elementName isEqualToString: @"pnLastName"]) return TRUE;
	if ([_elementName isEqualToString: @"firmName"]) return TRUE;
	
	self.elementName = nil;
	return FALSE;
}

- (void)pop {
	if (FALSE);
	else if ([self.elementName isEqualToString: @"pnFirstName"]) [self.delegate setFirstName:self.accumulator];	
	else if ([self.elementName isEqualToString: @"pnLastName"]) [self.delegate setLastName:self.accumulator];	
	else if ([self.elementName isEqualToString: @"firmName"]) [self.delegate setFirmName:self.accumulator];	
	
	[super pop];
}

- (void)dealloc {
	[elementName release];
	[super dealloc];
}

@end
