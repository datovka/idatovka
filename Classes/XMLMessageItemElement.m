/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLIdElement.m
//  InputTest
//
//  Created by Petr Hruška on 8/5/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "XMLMessageItemElement.h"
#import "XMLDmElement.h"
#import "XMLDmRecordElement.h"


@implementation XMLMessageItemElement

@synthesize delegate;
@synthesize elementName;

- (BOOL) matchNamespace:(NSString*)namespaceURI element:(NSString*)_elementName andAttributes:(NSDictionary*)attributes {

	if (![namespaceURI isEqualToString: @"http://isds.czechpoint.cz/v20"]) return FALSE;
	
	self.elementName = _elementName;

	if ([_elementName isEqualToString: @"dmID"]) return TRUE;
	
	if ([_elementName isEqualToString: @"dbIDSender"]) return TRUE;
	if ([_elementName isEqualToString: @"dmSender"]) return TRUE;
	if ([_elementName isEqualToString: @"dmSenderAddress"]) return TRUE;

	if ([_elementName isEqualToString: @"dbIDRecipient"]) return TRUE;
	if ([_elementName isEqualToString: @"dmRecipient"]) return TRUE;
	if ([_elementName isEqualToString: @"dmRecipientAddress"]) return TRUE;
	
	if ([_elementName isEqualToString: @"dmAnnotation"])
		return TRUE;

	self.elementName = nil;
	return FALSE;
}

- (void)pop {
	if (FALSE);
	else if ([self.elementName isEqualToString: @"dmID"]) [self.delegate setDmId:self.accumulator];	
	
	else if ([self.elementName isEqualToString: @"dbIDSender"]) [self.delegate setIdSender:self.accumulator];	
	else if ([self.elementName isEqualToString: @"dmSender"]) [self.delegate setSender:self.accumulator];	
	else if ([self.elementName isEqualToString: @"dmSenderAddress"]) [self.delegate setSenderAddress:self.accumulator];
	
	else if ([self.elementName isEqualToString: @"dbIDRecipient"]) [self.delegate setIdRecipient:self.accumulator];	
	else if ([self.elementName isEqualToString: @"dmRecipient"]) [self.delegate setRecipient:self.accumulator];	
	else if ([self.elementName isEqualToString: @"dmRecipientAddress"]) [self.delegate setRecipientAddress:self.accumulator];

	else if ([self.elementName isEqualToString: @"dmAnnotation"]) [self.delegate setAnnotation:self.accumulator];
	
	[super pop];
}

- (void)dealloc {
	[elementName release];
	[super dealloc];
}


@end
