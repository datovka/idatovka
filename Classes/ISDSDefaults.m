/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSDefaults.m
//  ISDS
//
//  Created by Petr Hruška on 10/14/10.
//

#import "ISDSDefaults.h"


@implementation ISDSDefaults

@synthesize defaults;

- (id)init {
	
	if ((self = [super init])) {
		
		self.defaults = [NSUserDefaults standardUserDefaults];
	}
	
	return self;
}

- (NSString*)pin {
	return [self.defaults stringForKey:@"pin"];
}

- (void)setPin:(NSString*)value {
	[self.defaults setObject:value forKey:@"pin"];
    [self.defaults synchronize];
}

- (BOOL)autoUpdate {
	return [self.defaults boolForKey:@"autoUpdate"];
}

- (void)setAutoUpdate:(BOOL)value {
	[self.defaults setBool:value forKey:@"autoUpdate"];
    [self.defaults synchronize];
}

- (NSInteger)pinFailureCount
{
    return [self.defaults integerForKey:@"pinFailureCount"];
}

- (void)setPinFailureCount:(NSInteger)pinFailureCount
{
	[self.defaults setInteger:pinFailureCount forKey:@"pinFailureCount"];
    [self.defaults synchronize];
}

+ (ISDSDefaults*)sharedDefaults
{
    static ISDSDefaults* sharedDefaults = nil;
    if (sharedDefaults == nil) sharedDefaults = [ISDSDefaults new];        
    return sharedDefaults;
}


@end
