/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  CertificateManager.m
//  ISDS
//
//  Created by Petr Hruška on 6/9/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CertificateManager.h"

#define CERTS_FILE @"AnchorCertificates.plist"


@implementation CertificateManager


+ (CertificateManager*)defaultCertificateManager {
    static CertificateManager* manager = nil;
    if (!manager) manager = [[CertificateManager alloc] init];
    return manager;
}

- (id)init {
		
	if ((self = [super init])) {
		NSString *rootPath = [[NSBundle mainBundle ] bundlePath];
		NSString *filePath = [rootPath stringByAppendingPathComponent:CERTS_FILE];
		
		NSArray* storage = [ NSMutableArray arrayWithContentsOfFile: filePath];

		_anchorCertificates = CFArrayCreateMutable(kCFAllocatorDefault, [storage count], &kCFTypeArrayCallBacks);
		
		if (storage) for (id data in storage) {
			
			SecCertificateRef cert = SecCertificateCreateWithData(NULL, (CFDataRef) data);
			
			CFArrayAppendValue(_anchorCertificates, cert);
			
			CFRelease(cert);
		}
	}

	return self;
}
/*

- (void)addCertificate:(SecCertificateRef)cert {
	
	CFStringRef descr = SecCertificateCopySubjectSummary(cert);
	CFDictionaryAddValue(_anchorCertificates, cert, descr);
	CFRelease(descr);
}


- (void)cleanAll {
	CFDictionaryRemoveAllValues(_anchorCertificates);
}


- (void)save {
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:CERTS_FILE];
	
	NSMutableArray *storage = [ NSMutableArray arrayWithCapacity: CFDictionaryGetCount(_anchorCertificates)];

	for (id cert in (NSDictionary*)_anchorCertificates) {

		[storage addObject:(id)SecCertificateCopyData((SecCertificateRef)cert)];
	}
	
	[ storage writeToFile:filePath atomically:FALSE ];
}
*/
- (CFArrayRef)anchorCertificates
{
    return _anchorCertificates;
}



- (void) dealloc {
	CFRelease(_anchorCertificates);
	[super dealloc];
}




@end
