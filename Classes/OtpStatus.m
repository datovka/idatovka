/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  OtpStatus.m
//  ISDS
//
//  Created by Petr Hruška on 7/6/11.
//

#import "OtpStatus.h"

@implementation OtpStatus

@synthesize account=_account;
@synthesize token=_token;
@synthesize task=_task;
@synthesize errorDescription=_errorDescription;
@synthesize shouldHaveCookie=_shouldHaveCookie;
@synthesize timer=_timer;
@synthesize delegate=_delegate;

- (void)dealloc
{
    [_account release];
    [_token release];
    [_task release];
    [_errorDescription release];
    [super dealloc];
}

- (void)stopTimer
{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}


- (enum OtpStatusType)status
{
    return _status;
}


- (void)cookieExpired:(NSTimer*)timer
{
    self.timer = nil;
    self.account.otpCookie = nil;
    self.account.otpCookieExpiration = nil;
    self.status = (self.account.authType == ISDSAccountAuthTotp) ? OtpStatusShouldSendSms : OtpStatusEnterToken;
    [self.delegate cookieExpiredForStatus:self];
}


- (void)setStatus:(enum OtpStatusType)status
{
    if (status != _status) {
        
        _status = status;

        if (status == OtpStatusHasCookie) {
        NSTimeInterval interval = [self.account.otpCookieExpiration timeIntervalSinceNow];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(cookieExpired:) userInfo:nil repeats:NO];
        } else {
            [self stopTimer];
        }
    }
}


- (id)initWithAccount:(ISDSAccount *)account
{
    if ((self = [super init])) {
        
        // assigned value is used when settings self.status to OtpStatusHasCookie
        self.account = account;
        if ([account hasCookie]) {
            self.status = OtpStatusHasCookie;
        } else {
            self.status = (self.account.authType == ISDSAccountAuthTotp) ? OtpStatusShouldSendSms : OtpStatusEnterToken;
        }
        self.shouldHaveCookie = YES;
    }
    
    return self;
}

- (BOOL)tokenReady
{
    return (self.status == OtpStatusEnterToken) && ([self.token length] != 0);
}

@end
