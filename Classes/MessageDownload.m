/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  MessageDownload.m
//  ISDS
//
//  Created by Petr Hruška on 9/2/10.
//
#import "MessageDownload.h"

NSDate* parseAcceptanceTime(NSString* raw)
{
	if (raw == nil) return nil;
	// date
	const NSUInteger dateLen = 10; //2010-08-09
	if ([raw length] < dateLen) return nil;
	NSString* date = [raw substringToIndex:dateLen];
	
	// time
	NSRange timeRange;
	timeRange.location = dateLen + 1; //2010-08-09T
	timeRange.length = 8; //12:34:56
	int locationAfterSeconds = timeRange.location + timeRange.length;
	if ([raw length] < locationAfterSeconds) return nil;
	NSString* time = [raw substringWithRange:timeRange];
	
	int i = locationAfterSeconds;
	
	// find start of timezone
	NSString* tzHours;
	NSString* tzMinutes;
	const char* s = [raw cStringUsingEncoding:NSUTF8StringEncoding];
	const NSUInteger len = strlen(s);
	
	// 12:34:56.354+HH:MM
	//         ^
	while (i < len && s[i] != '-' && s[i] != '+' && s[i] != 'z' && s[i] != 'Z') {		
		i++;
	}
	
	
	if (i == len) {
		
		tzHours = @"+20";
		tzMinutes = @"00";
	} else if (s[i] == 'Z' || s[i] == 'z') {
		
		tzHours = @"+00";
		tzMinutes = @"00";
	} else {
		
		// timezone sign and hours
		NSRange tzHoursRange;
		// timezone minutes
		NSRange tzMinutesRange;
		
		tzHoursRange.location = i;
		tzHoursRange.length = 3;
		
		tzMinutesRange.location = tzHoursRange.location + tzHoursRange.length + 1;
		tzMinutesRange.length = 2;
		
		if (tzMinutesRange.location + tzMinutesRange.length > [raw length]) return nil;
		tzHours = [raw substringWithRange:tzHoursRange];
		tzMinutes = [raw substringWithRange:tzMinutesRange];
	}
	
	NSString* string = [NSString stringWithFormat: @"%@ %@ %@%@", date, time, tzHours, tzMinutes];
	
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZZZ"]; //@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
	NSDate* retval = [dateFormat dateFromString:string];  
	[dateFormat release];
	return retval;
	//	self.acceptanceTime = [NSDate dateWithString:string];
}

