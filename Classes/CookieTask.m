/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  CookieTask.m
//  ISDS
//
//  Created by Petr Hruška on 8/8/11.
//

#import "CookieTask.h"


@implementation CookieTask

- (NSString*)otpPassword
{
    return [NSString stringWithFormat:@"%@%@", [self password], self.status.token];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)_response {
	NSHTTPURLResponse* response = (NSHTTPURLResponse*)_response;
    NSLog(@"CookieTask: didReceiveResponse: response code = %ld", (long)[response statusCode]);
    if ([response statusCode] == 302) {
        NSArray* cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[response allHeaderFields] forURL:[response URL]];
        for (NSHTTPCookie* cookie in cookies) {
            
            if ([[cookie name] isEqualToString:@"IPCZ-X-COOKIE"]) {
                self.status.account.otpCookie = cookie;
                self.status.account.otpCookieExpiration = [[NSDate date] dateByAddingTimeInterval:COOKIE_LIFETIME];
            }
        }
    } else {
        self.errorDescription = [self base64Message:response withDefault:NSLocalizedString(@"Error while processing server response", nil)];
    }
}

- (NSString*)taskDescription
{
    return NSLocalizedString(@"Verifying authorization code", nil);
}

@end
