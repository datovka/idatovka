/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ProgressViewController.m
//  ISDS
//
//  Created by Petr Hruška on 8/10/11.
//

#import "ProgressViewController.h"
#import "OtpTask.h"
#import "ISDSAppDelegate_Shared.h"

#define PROGRESSBAR_GAP            ((CGFloat)15.0)

@implementation ProgressViewController

@synthesize progressView=progressView_;
@synthesize currentJobLabel=currentjobLabel_;
@synthesize errorCountLabel=errorCountLabel_;
@synthesize activityIndicator=_activityIndicator;

- (void)dealloc
{
    [progressView_ release];
    [currentjobLabel_ release];
    [_activityIndicator release];
    [super dealloc];
}

- (NSArray*)observedClasses
{
    return [NSArray arrayWithObjects:@"SmsTask", @"HotpCookieTask", @"TotpCookieTask", nil];
}

- (void)registerObserver
{
    [[RequestManager requestManager] addObserver:self forClasses:[self observedClasses]];
}

- (void)unregisterObserver
{
    [[RequestManager requestManager] removeObserver:self forClasses:[self observedClasses]];
}

- (void)setupErrorCountLabel
{
    if (self.errorCount)
        self.errorCountLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Error count: %d", nil), self.errorCount];
    else
        self.errorCountLabel.text = @"";   
}

- (void)viewDidLoad
{
    self.currentJobLabel.text = self.currentJob;
    self.progressView.progress = self.progress;
    [self setDisplayContext: _context];
    [self setupErrorCountLabel];
}

- (void)viewDidUnload
{
    self.currentJobLabel = nil;
    self.progressView = nil;
    self.errorCountLabel = nil;
    self.activityIndicator = nil;
}

- (void)setProgress:(float)progress
{
    progress_ = progress;
    self.progressView.progress = progress;
}

- (float)progress
{
    return progress_;
}

- (void)setCurrentJob:(NSString *)currentJob
{
    if (currentJob == currentJob_) return;
    [currentJob_ release];
    currentJob_ = [currentJob retain];
    self.currentJobLabel.text = currentJob;
}

- (NSString*)currentJob
{
    return currentJob_;
}

- (void)setErrorCount:(NSUInteger)errorCount
{
    errorCount_=errorCount;
    [self setupErrorCountLabel];
}

- (NSUInteger)errorCount
{
    return errorCount_;
}

- (void)setProgressViewWidth:(CGFloat)width
{
    CGRect frame = self.view.frame;
    frame.size.width = width - PROGRESSBAR_GAP;
    self.view.frame = frame;
}

- (void)setDisplayContext:(enum ProgressDisplayContext)context
{
    ISDSAppDelegate_Shared* appDelegate;
    UIColor* color = nil;
    _context = context;
    switch (context) {
        case ProgressDisplayContextPopover:
            self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
            color = [UIColor whiteColor];
            break;
        case ProgressDisplayContext_iPad:
            self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            color = [UIColor darkTextColor];
            break;
        case ProgressDisplayContext_iPhone:
            self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
            color = [UIColor lightTextColor];
            break;
        case ProgressDisplayContextDefault:
        case ProgressDisplayContextAuto:
            appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
            if ([appDelegate iPad]) {
                self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
                color = [UIColor darkTextColor];
            } else {
                self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
                color = [UIColor lightTextColor];
            }
            break;
    }
    self.currentJobLabel.textColor = color;
    self.errorCountLabel.textColor = color;
}

#pragma mark - RequestManagerObserver

- (void)statusUpdate:(enum RMStatus)status forTask:(ISDSTask*)task
{
    //NSLog(@"Progress status %d", status);
    switch (status) {
        case RMStarted:
            self.currentJob = [task taskDescription];
            break;
        case RMDone:
        case RMCancelled:
        case RMFailed:
        case RMUnregistered:
            self.currentJob = @"";
            break;
        default:
            break;
    }

}


- (void)progressUpdate:(float)progress estimate:(NSTimeInterval)estimate elapsed:(NSTimeInterval)elapsed
{
//    NSLog(@"%0.2f", progress);
    self.progress = progress;
}

@end
