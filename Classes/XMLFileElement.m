/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLFileElement.m
//  ISDS
//
//  Created by Petr Hruška on 8/11/10.
//

#import "XMLFileElement.h"
#import "XMLFilesElement.h"


@implementation XMLFileElement

@synthesize delegate;
@synthesize name;
@synthesize content;

- (BOOL) matchNamespace:(NSString*)namespaceURI andElement:(NSString*)elementName {
	return
		[namespaceURI isEqualToString: @"http://isds.czechpoint.cz/v20"] && 
		[elementName isEqualToString: @"dmFile"];
}

- (BOOL) matchNamespace:(NSString*)namespaceURI element:(NSString*)elementName andAttributes:(NSDictionary*)attributes {
	
	if (![self matchNamespace:namespaceURI andElement:elementName]) return NO;
		
	NSString* metaType = [attributes objectForKey:@"dmFileMetaType"];
	if (metaType == nil) return NO;
	if (![metaType isEqualToString:@"main"] && ![metaType isEqualToString:@"enclosure"]) return NO;
	
	NSString* fileDescr = [attributes objectForKey:@"dmFileDescr"];
	if (!fileDescr) return NO;
	
	self.name = fileDescr;
	
	return YES;
}

- (void)pop {
	[self.delegate addAttachmentWithName:self.name andContent:(NSData*)self.content];
	[super pop];
}

@end
