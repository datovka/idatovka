/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Message.h
//  ISDS
//
//  Created by Petr Hruška on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISDSObject.h"
#import "EnvelopeData.h"
#import "Attachments.h"

@class SentMessageDownload;
@class ReceivedMessageDownload;

@interface ISDSMessage : ISDSObject {

	NSString* msgId;
		
	NSString* sender;
	NSString* senderAddress;
	NSString* recipient;
	NSString* recipientAddress;
	NSString* annotation;
	NSDate* acceptanceTime;
	
	BOOL read;
	
	Attachments* attachments;
	
}

@property (nonatomic, retain) NSString *msgId;
@property (nonatomic, readonly) BOOL incoming;

@property (nonatomic, retain) NSString *sender;
@property (nonatomic, retain) NSString *senderAddress;
@property (nonatomic, retain) NSString *recipient;
@property (nonatomic, retain) NSString *recipientAddress;
@property (nonatomic, retain) NSString *annotation;
@property (nonatomic, retain) NSDate *acceptanceTime;

@property (nonatomic, retain) Attachments *attachments;

@property (nonatomic, assign) BOOL read;

- (id)initFromFileWithParent:(ISDSObject*)parent andId:(NSString*)msgId;
- (id)initWithParent:(ISDSObject*)parent fromEnvelope:(EnvelopeData*)envelope;
- (NSComparisonResult) compareAcceptanceTime:(ISDSMessage *)msg;

- (BOOL)loadAttachments;
- (void)markRead;
- (BOOL)updateFromEnvelope:(EnvelopeData*)envelope;
- (BOOL)updateFromSentMessage:(SentMessageDownload*)message;
- (BOOL)updateFromReceivedMessage:(ReceivedMessageDownload*)message;
- (NSString*) friendlyShortDateTime;
- (NSString*) friendlyDateTime;

// attachments proxy
- (NSUInteger)attachmentCount;


@end
