/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  GetListOfReceivedMessages.h
//  ISDS
//
//  Created by Petr Hruška on 8/26/10.
//

#import <Foundation/Foundation.h>
#import "GetListOfMessages.h"


@interface GetListOfReceivedMessages : GetListOfMessages {

}

@end
