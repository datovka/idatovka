/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSAccount.h
//  ISDS
//
//  Created by Petr Hruška on 7/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReceivedMessageDownload.h"
#import "SignedSentMessageDownload.h"
#import "GetOwnerInfoFromLogin.h"
#import "MsgList.h"

#define COOKIE_LIFETIME     (30*60)

typedef NSUInteger ISDSAccountChangeReason;

#define ISDSAccountChangeReasonActive     0x01
#define ISDSAccountChangeReasonInactive   0x02
#define ISDSAccountChangeReasonBoxName    0x04
#define ISDSAccountChangeReasonOther      0x00

enum ISDSAccountAuthType {
  
    ISDSAccountAuthPass = 0,
    ISDSAccountAuthHotp = 1,
    ISDSAccountAuthTotp = 2,
};

@class AccountCache;
@class ISDSTask;

@protocol ISDSAccountObserver

- (void)account:(ISDSAccount*)account changedBecause:(ISDSAccountChangeReason)reasonFlags;

@end

@protocol MsgListObserver

- (void)listChanged:(MsgList*)list withAccount:(ISDSAccount*)account;

@end

@protocol MsgObserver

- (void)messageChanged:(ISDSMessage*)message withAccount:(ISDSAccount*)account;

@end

@interface ISDSAccount : NSObject {

	NSString* password;
	NSString* databox;
	BOOL testing;
	BOOL active;
	BOOL useAlias;
	NSString* alias;
    enum ISDSAccountAuthType _authType;
	
	BOOL configChanged;
	
	AccountCache* cache;
	
	NSMutableArray *observers;
	NSMutableArray *receivedObservers;
	NSMutableArray *sentObservers;
	NSMutableArray *messageObservers;
}

@property (nonatomic, retain) NSString* password;
@property (nonatomic, retain) NSString* databox;
@property (nonatomic, assign) BOOL testing;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, assign) BOOL useAlias;
@property (nonatomic, retain) NSString* alias;
@property (nonatomic, readonly) NSDate* lastReceivedUpdate;
@property (nonatomic) enum ISDSAccountAuthType authType;
@property (nonatomic, retain) NSHTTPCookie* otpCookie;
@property (nonatomic, retain) NSDate* otpCookieExpiration;

@property (nonatomic) BOOL configChanged;

@property (nonatomic, retain) AccountCache* cache;
@property (nonatomic, retain) NSMutableArray* observers;
@property (nonatomic, retain) NSMutableArray* receivedObservers;
@property (nonatomic, retain) NSMutableArray* sentObservers;
@property (nonatomic, retain) NSMutableArray* messageObservers;

// saving config
- (NSDictionary*)plist;
- (void)savePassword;

// initialization
- (id)initFromSecureStorageAndPlist:(NSObject*)tuple;
- (id)init;

// misc
- (NSString*)boxName;
- (NSString*)alias;
- (BOOL)useAlias;
- (NSInteger)unreadSentCount;
- (NSInteger)unreadReceivedCount;
- (BOOL)cleanCache;
- (BOOL)loadCache;
- (BOOL)hasCookie;
- (BOOL)needsToken;

//cache proxies
- (void)releaseCache;
- (BOOL)updateOwnerInfo:(GetOwnerInfoFromLogin*)method;
- (BOOL)loadOwnerInfo;
- (BOOL)updateSentMessageList:(GetListOfSentMessages*)method;
- (BOOL)updateReceivedMessageList:(GetListOfReceivedMessages*)method;
- (BOOL)updateEstimate:(ISDSTask*)task withDelay:(NSTimeInterval)delay;
- (NSTimeInterval)estimate:(ISDSTask*)task;
- (BOOL)updateFromReceivedMessage:(ReceivedMessageDownload*)method;
- (BOOL)updateFromSentMessage:(SignedSentMessageDownload*)method;

// account change
- (void)addObserver:(id<ISDSAccountObserver>)observer;
- (void)removeObserver:(id<ISDSAccountObserver>)observer;
- (void)callObservers:(ISDSAccountChangeReason)reasonFlags;

- (void)addReceivedListObserver:(id<MsgListObserver>)observer;
- (void)removeReceivedListObserver:(id<MsgListObserver>)observer;
- (void)addSentListObserver:(id<MsgListObserver>)observer;
- (void)removeSentListObserver:(id<MsgListObserver>)observer;
- (void)callListObservers:(MsgList*)list;

- (void)addMsgObserver:(id<MsgObserver>)observer;
- (void)removeMsgObserver:(id<MsgObserver>)observer;
- (void)callMsgObservers:(ISDSMessage*)msg;

@end
