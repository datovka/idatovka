/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  OutboxViewController.m
//  ISDS
//
//  Created by Petr Hruška on 6/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "OutboxViewController.h"
#import "AccountCache.h"
#import "MsgList.h"

@implementation OutboxViewController

- (NSString*)boxType {
	return NSLocalizedString(@"Outbox", @"Outbox");
}


- (MsgList*) getMsgList {
	if (![self.account loadCache]) return nil;
	if (![self.account.cache loadSent]) return nil;
	return self.account.cache.sent;	
}


- (void)dealloc {
    [super dealloc];
}


-(void)registerObserver {
    [super registerObserver];
	[self.account addSentListObserver:self];
}


-(void)unregisterObserver {
    [super unregisterObserver];
	[self.account removeSentListObserver:self];
}



@end


