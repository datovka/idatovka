/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  DER.h
//  InputTest
//
//  Created by Petr Hruška on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef unsigned char u8;

@interface DER : NSObject {

	NSData* msg;
	NSIndexPath* index;
	NSIndexPath* indexOfInterest;

}

@property (nonatomic, retain) NSData* msg;
@property (nonatomic, retain) NSIndexPath* index;
@property (nonatomic, retain) NSIndexPath* indexOfInterest;

- (int)parse:(const u8*)bytes from:(int)offset to:(int)limit;

- (id)initWithData:(NSData*)data;

@end
