/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  GetListOfReceivedMessages.m
//  ISDS
//
//  Created by Petr Hruška on 8/26/10.
//

#import "GetListOfReceivedMessages.h"
#import "XMLGetListOfReceivedMessagesResponseElement.h"


@implementation GetListOfReceivedMessages

- (id)init {
	
	if ((self = [super init])) {

		@try {		
			[self.elements addObject:[[XMLGetListOfReceivedMessagesResponseElement alloc] init]];
		}
	
		@catch (NSException* exception) {
			//   STRANGE COMPILE ERROR!	@catch (NSInvalidArgumentException* exception) {
			/* argument of addObject was probably nil */
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (NSData*) soapRequest {
	NSString* pattern = [SoapMethod requestPattern];
	NSString* req = @"<ns1:GetListOfReceivedMessages><dmStatusFilter>-1</dmStatusFilter></ns1:GetListOfReceivedMessages>";
	
	NSString* s = [NSString stringWithFormat: pattern, req];
	NSData* d = [s dataUsingEncoding:NSUTF8StringEncoding];
	return d;
}


@end
