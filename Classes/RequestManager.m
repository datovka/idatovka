/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  RequestManager.m
//  ISDS
//
//  Created by Petr Hruška on 1/12/11.
//

#import "RequestManager.h"
#import "TaskObserver.h"
#import "Accounts.h"

#define TIMER_INTERVAL  0.05
#define EPSILON         (TIMER_INTERVAL / 1000)


@implementation RequestManager

@synthesize queue=_queue;
@synthesize observers=_observers;
@synthesize timer=_timer;

- (void)dealloc {
	[_queue release];
    [_observers release];
    [_timer release];
	[super dealloc];
}

- (id)init {
    
	if ((self = [super init])) {
		
		self.queue = [[NSMutableArray alloc] init];
		[self.queue release];
		
		self.observers = [[NSMutableArray alloc] init];
		[self.observers release];
		
		if (!self.queue || !self.observers) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (void)addObservers:(NSMutableArray*)list forTask:(ISDSTask*)task {

	for (TaskObserver* observer in self.observers)
		if ([observer match:task]) [list addObject:observer.observer];
}


- (NSTimeInterval)timeEstimate:(ISDSTask*)task
{
	ISDSAccount* account = [task account];
	if (!account) return 0;
	
	NSTimeInterval estimate = [account estimate:task];
	
	if (!task.started) return estimate;
	
	NSTimeInterval elapsed = -[task.started timeIntervalSinceNow];
	estimate -= elapsed;
	
	if (estimate < 0) return 0;
	
	return estimate;
}

- (void)sendProgressUpdateForObserver:(TaskObserver*)observer
{
    NSTimeInterval maxEstimate = 0;

    NSTimeInterval estimate = 0;
    
    BOOL hasTask = NO;

    for (ISDSTask* task in self.queue) {
    
        // adding nonnegative value
        estimate += [self timeEstimate:task];
        if ([observer match:task]) {
            maxEstimate = estimate;
            hasTask = YES;
        }
    }
    
    if (!hasTask) {
        observer.activated = nil;
        return;
    }
    
    // substract time elapsed since creation
    // elapsed is zero, if there
    if (!observer.activated) observer.activated = [NSDate date];
    NSTimeInterval elapsed = -[observer.activated timeIntervalSinceNow];
    if (maxEstimate + elapsed < EPSILON) return;
    float progress = elapsed / (maxEstimate + elapsed);
    [observer.observer progressUpdate:progress estimate:maxEstimate elapsed:elapsed];
    
}

- (void)sendProgressUpdate {

	NSArray* tmpObservers = [NSArray arrayWithArray:self.observers];
	for (TaskObserver* observer in tmpObservers) 
        [self sendProgressUpdateForObserver:observer];
}

// timer management

- (void)timerTick:(NSTimer*)timer {
	[self sendProgressUpdate];
}

- (void)startTimer {
	self.timer = [NSTimer scheduledTimerWithTimeInterval:TIMER_INTERVAL target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
}

- (void)stopTimer {
	[self.timer invalidate];
}

- (void)sendStatusUpdate:(enum RMStatus)status forTask:(ISDSTask*)task {
    
	NSMutableArray* list = [NSMutableArray arrayWithCapacity:[self.observers count]];
	[self addObservers:list forTask:task];
	for (id<RequestManagerObserver> observer in list) {
		[observer statusUpdate:status forTask:task];
	}
}

- (void)sendStatusUpdate:(enum RMStatus)status forObserver:(TaskObserver*)to {
	
	NSMutableArray* list = [[NSMutableArray alloc] initWithCapacity:[self.queue count]];
	if (!list) return;
	
	for (ISDSTask* task in self.queue) {
		if ([to match:task]) [list addObject:task];
	}
	
	for (ISDSTask* task in list) {
		[to.observer statusUpdate:status forTask:task];
	}
	
	[list release];

}

- (void)startTask {
	
	if (![self.queue count]) return;
    
	ISDSTask* t = [self.queue objectAtIndex:0];
    [self sendStatusUpdate:RMStarted forTask:t];
    [self startTimer];
    	
	ISDSAccount* account = [t account];
	
    if (account == nil) {
		// account disappeared
        [self taskFailed:t];
        return;
	}
    
    t.delegate = self;
    [t start];
}

- (void)addTasks:(NSArray*)tasks withPriority:(BOOL)priority
{
    BOOL empty = [self.queue count] == 0;
    if (priority) {
        NSRange range;
        range.location = empty ? 0 : 1;
        range.length = [tasks count];
        NSIndexSet* indexes = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.queue insertObjects:tasks atIndexes:indexes];
    } else {
        [self.queue addObjectsFromArray:tasks];
    }
    for (ISDSTask* task in tasks) [self sendStatusUpdate:RMQueued forTask:task];
    if (empty) [self startTask];
	[self sendProgressUpdate];
}

- (void)removeTask:(ISDSTask*)task
{
    NSUInteger index = [self.queue indexOfObject:task];
    if (index == NSNotFound) return;
    if (index == 0) {
        [task cancel];
        [self sendStatusUpdate:RMCancelled forTask:task];
    } else {
        [self sendStatusUpdate:RMUnregistered forTask:task];
    }
    
    [self.queue removeObject:task];
}


// observers management

- (void)addObserver:(id<RequestManagerObserver>)observer forClasses:(NSArray*)classes {
	
	TaskObserver* to = [[TaskObserver alloc] initWithObserver:observer forClasses:classes];

	[self.observers addObject:to];
	[self sendStatusUpdate:RMQueued forObserver:to];
	if ([self.queue count] >= 1) {
		ISDSTask* t = [self.queue objectAtIndex:0];
        if ([to match:t]) [observer statusUpdate:RMStarted forTask:t];
	}
}

- (void)removeObserver:(id<RequestManagerObserver>)observer forClasses:(NSArray*)classes {

	for (TaskObserver* to in self.observers) {
		if (to.observer == observer && [to matchObservedClasses:classes]) {
			[self.observers removeObject:to];
			[self sendStatusUpdate:RMUnregistered forObserver:to];
			break;
		}
	}
}

+ (RequestManager*)requestManager {
	static RequestManager* rm = nil;
	
	if (!rm) rm = [[RequestManager alloc] init];
	return rm;
}

#pragma mark - ISDSTaskDelegate methods

- (void)taskSuccessfullyDone:(ISDSTask*)task
{	
    [self stopTimer];
	[self sendStatusUpdate:RMDone forTask:task];
	[self.queue removeObject:task];
	[self startTask];
	[self sendProgressUpdate];
}

- (void)taskFailed:(ISDSTask*)task
{
    [self stopTimer];
	[self sendStatusUpdate:RMFailed forTask:task];
	[self.queue removeObject:task];
	[self startTask];
	[self sendProgressUpdate];
}


@end
