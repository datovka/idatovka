/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Accounts.h
//  ISDS
//
//  Created by Petr Hruška on 7/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ISDSAccount.h"

enum AccountsObserverAction {
    AccountsObserverAccountAdded = 1,
    AccountsObserverAccountRemoved = 2,
};

@protocol AccountsObserver

- (void)cacheCleaned;
- (void)listChanged:(enum AccountsObserverAction)reason because:(ISDSAccount*)account;

@end

@interface Accounts : NSObject {

	NSMutableArray* accounts;
	BOOL anAccountRemoved;
}

@property (nonatomic, assign) BOOL anAccountRemoved;
@property (nonatomic, retain) NSMutableArray* observers;

- (void)addAccount:(ISDSAccount*)account;
- (void)removeAccount:(ISDSAccount*)account;
- (ISDSAccount*)accountAtIndex:(NSUInteger)index;
- (ISDSAccount*)accountById:(NSString*)databox andTesting:(BOOL)testing;
- (NSUInteger)count;
- (id)init;
- (void)save;
- (void)cleanCache;
- (void)releaseCache;

- (BOOL)configChanged;
- (void)saveAsNeeded;

- (NSUInteger)activeCount;
- (NSArray*)activeAccounts;
- (NSInteger)activeAccountIndex:(ISDSAccount*)account;
- (BOOL)needToken;

- (NSDate*)lastUpdate;

+ (Accounts*) accounts;

- (void)addAccountObserver:(id<ISDSAccountObserver>)observer;
- (void)removeAccountObserver:(id<ISDSAccountObserver>)observer;

- (void)addListObserver:(id<MsgListObserver>)observer;
- (void)removeListObserver:(id<MsgListObserver>)observer;

- (void)addAccountsObserver:(id<AccountsObserver>)observer;
- (void)removeAccountsObserver:(id<AccountsObserver>)observer;

@end
