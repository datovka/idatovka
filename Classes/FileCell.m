/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  FileCell.m
//  ISDS
//
//  Created by Petr Hruška on 11/3/10.
//

#import "FileCell.h"


@implementation FileCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIImage* basicImage = [UIImage imageNamed:@"Attachment.png"];
    
    self.backgroundImageView.image = [basicImage stretchableImageWithLeftCapWidth:10 topCapHeight:0];
}

- (NSString*)fileName {
	return self.fileNameLabel.text;
}

- (void)setFileName:(NSString*)value {
	self.fileNameLabel.text = value;
}

- (NSUInteger)fileSize {
	return fileSize;
}

- (void)setFileSize:(NSUInteger)value {
	NSUInteger KB = (value + 1023) / 1024;
	self.fileSizeLabel.text = [NSString stringWithFormat:@"%lu KB", (unsigned long)KB];
	fileSize = value;
}

- (UIImage*)fileIcon {
	return self.fileIconView.image;
}

- (void)setFileIcon:(UIImage*)value {
	self.fileIconView.image = value;
}

- (void)dealloc {
	[_fileNameLabel release];
	[_fileSizeLabel release];
    [_backgroundImageView release];
    [super dealloc];
}


@end
