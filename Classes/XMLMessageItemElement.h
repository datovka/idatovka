/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLIdElement.h
//  InputTest
//
//  Created by Petr Hruška on 8/5/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLAccumulatorElement.h"


@protocol XMLMessageItemElementDelegate

- (void)setDmId:(NSString*)value;

- (void)setIdSender:(NSString*)value;
- (void)setSender:(NSString*)value;
- (void)setSenderAddress:(NSString*)value;

- (void)setIdRecipient:(NSString*)value;
- (void)setRecipient:(NSString*)value;
- (void)setRecipientAddress:(NSString*)value;

- (void)setAnnotation:(NSString*)value;

@end

@interface XMLMessageItemElement : XMLAccumulatorElement {

	id<XMLMessageItemElementDelegate> delegate;
	NSString* elementName;
}

@property (nonatomic, assign) id<XMLMessageItemElementDelegate> delegate;
@property (nonatomic, retain) NSString* elementName;

@end
