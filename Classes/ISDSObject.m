/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSObject.m
//  ISDS
//
//  Created by Petr Hruška on 8/18/10.
//

#import "ISDSObject.h"

@implementation ISDSObject

@synthesize parent;

// private methods

- (NSMutableString*)appendDir:(NSMutableString*)r {
	
	// append directories of parents
	if (self.parent) r = [self.parent appendDir:r];
	
	// append our own directory
	NSString* d = [self dir];
	[r appendString:d];
	
	return r;
}

- (NSInteger) dirLength {

	NSUInteger len = 0;
	ISDSObject* x = self;
	
	while (x) {
		NSString* d = [x dir];
		if (!d) return -1;
		len += [d length];
		x = x.parent;
	}
	
	return len;
}

- (NSString*)getFullFilename {
	
	NSString* f = [self filename];
	if (!f) return nil;
	
	// determine length
	NSInteger len = [self dirLength];
	if (len < 0) return nil;
	len += [f length];
	
	// allocate
	NSMutableString* r = [NSMutableString stringWithCapacity:len];
	if (!r) return nil;
	
	// append all directories
	r = [self appendDir:r];
	
	// append filename
	[r appendString: f];
	//NSLog(@"%@", r);
	return r;
}

- (NSString*)getDir {
	NSInteger len = [self dirLength];
	if (len < 0) return nil;
	
	// allocate
	NSMutableString* r = [NSMutableString stringWithCapacity:len];
	if (!r) return nil;	

	// append all directories
	r = [self appendDir:r];
	
	return r;
}

- (NSDictionary*)plistFromFile {
	
	NSString* f = [self getFullFilename];
	if (!f) return nil;

	NSDictionary* plist = [NSDictionary dictionaryWithContentsOfFile:f];
	return plist;
}

- (BOOL)updateFromFile {
	
	NSDictionary* plist = [self plistFromFile];
	
	return [self updateFromPlist:plist];
}

- (id)initWithParent:(ISDSObject*)_parent {
	if ((self = [super init])) {
		self.parent = _parent;
	}
	return self;
}

- (BOOL)mkDirs {
	NSFileManager* mgr = [NSFileManager defaultManager];
	
	NSString* dir = [self getDir];
	if (!dir) return FALSE;
	return [mgr createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:NULL];
}

- (BOOL)writeToFile:(NSDictionary*)plist {
	
	if (![self mkDirs]) return FALSE;

	NSString* f = [self getFullFilename];
	if (!f) return FALSE;
	//NSLog(@"%@", f);
	BOOL B = [plist writeToFile:f atomically:TRUE];
	if (!B) {
        return FALSE;
    }
	
	return TRUE;
}

- (BOOL)updateFromPlist:(NSDictionary*)plist {
	return FALSE; // abstract method
}

// directory for children
- (NSString*)dir {
	return nil; // abstract method
}

// file with data of the object
- (NSString*)filename {
	return nil; // abstract method
}

- (BOOL)removeFileAndDir {
	
	NSFileManager* mgr = [NSFileManager defaultManager];
	BOOL B = TRUE;

	NSString* f = [self getFullFilename];
	if (f) {
		if (![mgr removeItemAtPath:f error:NULL]) B = FALSE;
	}
	
	NSString* d = [self getDir];
	if (d) {
		if (![mgr removeItemAtPath:d error:NULL]) B = FALSE;
	}
	
	return B;
}

@end
