/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  FileCell.h
//  ISDS
//
//  Created by Petr Hruška on 11/3/10.
//

#import <UIKit/UIKit.h>


@interface FileCell : UITableViewCell {

	NSUInteger fileSize;
}

@property (nonatomic, retain) IBOutlet UILabel* fileNameLabel;
@property (nonatomic, retain) IBOutlet UILabel* fileSizeLabel;
@property (nonatomic, retain) IBOutlet UIImageView* fileIconView;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, copy) NSString* fileName;
@property (nonatomic, assign) NSUInteger fileSize;
@property (nonatomic, assign) UIImage* fileIcon;

@end
