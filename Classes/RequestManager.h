/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  RequestManager.h
//  ISDS
//
//  Created by Petr Hruška on 1/12/11.
//

#import <Foundation/Foundation.h>
#import "ISDSTask.h"

enum RMStatus {
	
	RMQueued,
	RMStarted,
	RMDone,
	RMCancelled,
	RMFailed,
	RMUnregistered,
};

@protocol RequestManagerObserver

- (void)statusUpdate:(enum RMStatus)status forTask:(ISDSTask*)task;
- (void)progressUpdate:(float)progress estimate:(NSTimeInterval)estimate elapsed:(NSTimeInterval)elapsed;
@end


@interface RequestManager : NSObject<ISDSTaskDelegate> {

}

@property (nonatomic, retain) NSMutableArray* queue; // ISDSTasks
@property (nonatomic, retain) NSMutableArray* observers; // TaskObservers
@property (nonatomic, retain) NSTimer* timer;

// return task with a method of same type, possibly another former queued instace, or nil in case of error
- (void)addTasks:(NSArray*)tasks withPriority:(BOOL)priority;
- (void)removeTask:(ISDSTask*)task;

- (void)addObserver:(id<RequestManagerObserver>)observer forClasses:(NSArray*)classes;
- (void)removeObserver:(id<RequestManagerObserver>)observer forClasses:(NSArray*)classes;

+ (RequestManager*)requestManager;


@end
