/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Attachments.h
//  ISDS
//
//  Created by Petr Hruška on 9/1/10.
//

#import <Foundation/Foundation.h>
#import "ISDSObject.h"
#import "Attachment.h"


@interface Attachments : ISDSObject {

	NSMutableArray* list;
}

@property (nonatomic,retain) NSMutableArray* list;


- (id)initFromFileWithParent:(ISDSObject*)parent;
- (id)initWithParent:(ISDSObject*)parent fromAttachments:(NSMutableArray*)attachments;
- (BOOL)update:(NSMutableArray*)attachments;
- (Attachment*)attachmentAtIndex:(NSUInteger)index;
- (NSUInteger)count;
- (void)clean;


@end
