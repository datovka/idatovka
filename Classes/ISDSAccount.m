/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSAccount.m
//  ISDS
//
//  Created by Petr Hruška on 7/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ISDSAccount.h"
#import "Estimate.h"
#import "AccountCache.h"

@implementation ISDSAccount

@synthesize configChanged;
@synthesize cache;
@synthesize observers;
@synthesize receivedObservers;
@synthesize sentObservers;
@synthesize messageObservers;
@synthesize otpCookie=_otpCookie;
@synthesize otpCookieExpiration=_otpCookieExpiration;

- (void)dealloc {
	
	[databox release];
	[password release];
	[cache release];
	[alias release];
	[observers release];
	[receivedObservers release];
	[sentObservers release];
	[messageObservers release];
    [_otpCookie release];
    [_otpCookieExpiration release];
    
	[super dealloc];
}

- (void)callObservers:(ISDSAccountChangeReason)reasonFlags {
	NSArray* observers_ = [NSArray arrayWithArray:self.observers];
	for (id<ISDSAccountObserver> observer in observers_) [observer account:self changedBecause:reasonFlags];
}

- (BOOL)loadCache {
	if (self.cache == (AccountCache*)[NSNull null])
		self.cache = [[AccountCache alloc] initFromFileWithAccount:self];
	
	return self.cache != nil;
}

- (BOOL)hasCookie
{
    if (!self.otpCookie || !self.otpCookieExpiration) return FALSE;
    
    NSDate* now = [NSDate date];
    return [now compare:self.otpCookieExpiration] == NSOrderedAscending;
}

- (BOOL)needsToken
{
    return self.active && self.authType != ISDSAccountAuthPass && ![self hasCookie];
}


+ (NSString*)getPasswordByDatabox:(NSString*)databox {
	
	NSString* serviceId = [NSString stringWithFormat: @"ISDS-%@", databox];
	
	NSMutableDictionary *query = [[NSMutableDictionary alloc] init];    
	[query setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
	[query setObject:(id)serviceId forKey:(id)kSecAttrService];
	[query setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
	
	CFDataRef result;
	NSString* password;
	
	OSStatus status = SecItemCopyMatching((CFDictionaryRef) query, (CFTypeRef*) &result);
	if (!status) {
		password = (NSString*) CFStringCreateFromExternalRepresentation(kCFAllocatorDefault, result, kCFStringEncodingUTF8);
        [password autorelease];
	} else
		password = @"";
	[query release];
	
	return password;
}

+ (void)setPassword:(NSString*)password forDatabox:(NSString*)databox {
	
	NSString* serviceId = [NSString stringWithFormat: @"ISDS-%@", databox];
	
	OSStatus status;
	NSMutableDictionary *secParams = [[NSMutableDictionary alloc] init];    
	[secParams setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
	[secParams setObject:(id)serviceId forKey:(id)kSecAttrService];
	[secParams setObject:(id)[password dataUsingEncoding: NSUTF8StringEncoding] forKey:(id)kSecValueData];		
	status = SecItemAdd((CFDictionaryRef) secParams, NULL);
	[secParams release];
	
	if (status == errSecDuplicateItem) {
		NSMutableDictionary *query = [[NSMutableDictionary alloc] init];    
		[query setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
		[query setObject:(id)serviceId forKey:(id)kSecAttrService];
		NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];    
		[attributes setObject:(id)[password dataUsingEncoding: NSUTF8StringEncoding] forKey:(id)kSecValueData];
		SecItemUpdate((CFDictionaryRef) query, (CFDictionaryRef) attributes);
		
		[query release];
		[attributes release];
	}
}

- (NSString*)password {
	return password;
}

- (void)setPassword:(NSString *)input {

	if (password != input) {
		
		BOOL changed = ![password isEqualToString: input];
		if (changed) self.configChanged = TRUE;
		
		[password autorelease];
		password = [input retain];
		if (changed) [self callObservers:ISDSAccountChangeReasonOther];
	}
}

- (BOOL)testing {
	return testing;
}

- (void)setTesting:(BOOL) input {
	
	if (testing != input) {
		testing = input;
		self.configChanged = TRUE;
		[self callObservers:ISDSAccountChangeReasonOther];
		if (self.databox && ![self.databox isEqualToString: @""]) [self cleanCache];
	}
}

- (NSString*)databox {
	return databox;
}


- (void)setDatabox:(NSString *)input {

	if (databox != input) {

		BOOL changed = ![databox isEqualToString: input];
		if (changed) {
			self.configChanged = TRUE;
			if (databox && ![databox isEqualToString: @""]) [self cleanCache];
		}

		[databox autorelease];
		databox = [input retain];
    
		if (changed) {
            NSUInteger flags;
            if (self.useAlias) 
                flags = ISDSAccountChangeReasonOther; 
            else
                flags = ISDSAccountChangeReasonBoxName; 
            [self callObservers:flags];
        }
	}
}

- (BOOL)useAlias {
	return useAlias;
}

- (void)setUseAlias:(BOOL)value {
	if (useAlias != value) {
		self.configChanged = TRUE;
		useAlias = value;
		[self callObservers:ISDSAccountChangeReasonBoxName];
	}
}

- (NSString*)alias {
	return alias;
}

- (void)setAlias:(NSString*)value {
	
	if (alias != value) {
		
		BOOL changed = ![alias isEqualToString: value];
		if (changed) self.configChanged = TRUE;
		
		NSString* tmp = alias;
		alias = [value retain];
		[tmp release];
		if (changed) [self callObservers:ISDSAccountChangeReasonBoxName];
	}
}

- (enum ISDSAccountAuthType)authType
{
    return _authType;
}

- (void)setAuthType:(enum ISDSAccountAuthType)authType
{
    if (_authType != authType) {
        
        self.otpCookie = nil;
        self.otpCookieExpiration = nil;
		_authType = authType;
		self.configChanged = TRUE;
		[self callObservers:ISDSAccountChangeReasonOther];
    }
}

- (NSDate*)lastReceivedUpdate {
	
	if (![self loadCache]) return nil;
	return self.cache.lastReceivedUpdate;
}

- (NSDictionary*)plist {

	NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
			self.databox, @"databox",
			[NSNumber numberWithBool: self.testing], @"testing",
			[NSNumber numberWithBool: self.active], @"active",
			[NSNumber numberWithBool: self.useAlias], @"useAlias",
            [NSNumber numberWithInt: (int)self.authType], @"authType",
			nil
			];
	
	if (self.alias) [dict setObject:self.alias forKey:@"alias"];
	
	return dict;
}

- (void)savePassword {
	[ISDSAccount setPassword: self.password forDatabox:self.databox];
}

- (id)initFromSecureStorageAndPlist:(NSObject*)plist {
	
	if ([self init] == nil) return nil;
	
	if ([plist isKindOfClass:[NSArray class]]) { // this branch is for backward compatibility (no active account)
	
		NSArray* array = (NSArray*)plist;
		NSNumber* num = [array objectAtIndex:1];
		// this assignment can clean cache, but self.databox is still empty
		self.testing = [num boolValue];
	
		self.databox = [array objectAtIndex:0];
		self.active = TRUE;
		
	} else {
		
		NSDictionary* dict = (NSDictionary*)plist;
		NSNumber* num = [dict objectForKey:@"testing"];
		// this assignment can clean cache, but self.databox is still empty
		self.testing = [num boolValue];
		
		self.databox = [dict objectForKey:@"databox"];
		
		num = [dict objectForKey:@"useAlias"];
		self.useAlias = [num boolValue];
		
		self.alias = [dict objectForKey:@"alias"];
		
		num = [dict objectForKey:@"active"];
		self.active = [num boolValue];

		num = [dict objectForKey:@"authType"];
		self.authType = num ? [num intValue] : ISDSAccountAuthPass;
	}

	self.password = [ISDSAccount getPasswordByDatabox:self.databox];

	self.configChanged = FALSE;

	return self;
}

- (id)init {
		
	if ((self = [super init])) {
		
		self.cache = (AccountCache*)[NSNull null];
		self.observers = [NSMutableArray array];
		self.sentObservers = [NSMutableArray array];
		self.receivedObservers = [NSMutableArray array];
		self.messageObservers = [NSMutableArray array];
		
		if (!self.observers || !self.receivedObservers || !self.sentObservers || !self.messageObservers) {
			
			[self dealloc];
			return nil;
		}
		
		self.databox = @"";
		self.password = @"";
		self.testing = NO;
		self.active = TRUE;
		self.alias = @"";
		self.useAlias = FALSE;
		self.configChanged = FALSE;
	}
	return self;
}


- (BOOL)active {
	return active;
}

- (void)setActive:(BOOL) input {
	
	if (active != input) {
		active = input;
		self.configChanged = TRUE;
        NSUInteger flags = active ? ISDSAccountChangeReasonActive : ISDSAccountChangeReasonInactive;
		[self callObservers:flags];
	}
}


- (NSString*)boxName {

	if (self.useAlias && self.alias && ![self.alias isEqualToString:@""]) {
		return self.alias;
	}
	
	BOOL hasOwnerInfo = [self loadOwnerInfo];
	if (hasOwnerInfo) {
		OwnerInfo* ownerInfo = self.cache.ownerInfo;
		
		if (ownerInfo.firstName && ownerInfo.lastName)
			return [NSString stringWithFormat: @"%@ %@", ownerInfo.firstName, ownerInfo.lastName];
		if (ownerInfo.firstName) return ownerInfo.firstName;
		if (ownerInfo.lastName) return ownerInfo.lastName;
		if (ownerInfo.firmName) return ownerInfo.firmName;
	} else
		return self.databox;
	
	return @""; // this should not happen
}

- (NSInteger)unreadSentCount {
	if (![self loadCache]) return -1;
	return [self.cache unreadSentCount];
}

- (NSInteger)unreadReceivedCount {
	if (![self loadCache]) return -1;
	return [self.cache unreadReceivedCount];
}

- (BOOL)cleanCache {
	
	if ([databox isEqualToString: @""]) return TRUE;

    self.otpCookie = nil;
    self.otpCookieExpiration = nil;
    
	[self loadCache];

	[self.cache clean];
	self.cache = (AccountCache*)[NSNull null];
	return TRUE;
}

- (void)releaseCache {
	
	self.cache = (AccountCache*)[NSNull null];
}

- (BOOL)updateOwnerInfo:(GetOwnerInfoFromLogin*)method {
	if (![self loadCache]) return FALSE;
	return [self.cache updateOwnerInfo:method];
}

- (BOOL)loadOwnerInfo {
	if (![self loadCache]) return FALSE;
	return [self.cache loadOwnerInfo];
}

- (BOOL)updateSentMessageList:(GetListOfSentMessages*)method {
	if (![self loadCache]) return FALSE;
	return [self.cache updateSentMessageList:method];
}

- (BOOL)updateReceivedMessageList:(GetListOfReceivedMessages*)method {
	if (![self loadCache]) return FALSE;
	return [self.cache updateReceivedMessageList:method];
}

- (BOOL)updateEstimate:(ISDSTask*)task withDelay:(NSTimeInterval)delay {
	if (![self loadCache]) return FALSE;
	return [self.cache updateEstimate:task withDelay:delay];
}

- (NSTimeInterval)estimate:(ISDSTask*)task {
	if (![self loadCache]) return [Estimate defaultDelayForTask:task];
	return [self.cache estimate:task];
}

- (BOOL)updateFromReceivedMessage:(ReceivedMessageDownload*)method {
	if (![self loadCache]) return FALSE;
	return [self.cache updateFromReceivedMessage:method];
}

- (BOOL)updateFromSentMessage:(SignedSentMessageDownload*)method {
	if (![self loadCache]) return FALSE;
	return [self.cache updateFromSentMessage:method];
}

- (void)addObserver:(id<ISDSAccountObserver>)observer {
	[self.observers addObject:observer];
}

- (void)removeObserver:(id<ISDSAccountObserver>)observer {
	[self.observers removeObject:observer];
}

- (void)addReceivedListObserver:(id<MsgListObserver>)observer {
	[self.receivedObservers addObject:observer];
}

- (void)removeReceivedListObserver:(id<MsgListObserver>)observer {
	[self.receivedObservers removeObject:observer];
}

- (void)addSentListObserver:(id<MsgListObserver>)observer {
	[self.sentObservers addObject:observer];
}

- (void)removeSentListObserver:(id<MsgListObserver>)observer {
	[self.sentObservers removeObject:observer];
}

- (void)callListObservers:(MsgList*)list {
	NSMutableArray *originalObservers = list.incoming ? self.receivedObservers : self.sentObservers;
	NSArray *observers_ = [NSArray arrayWithArray:originalObservers];	
	for (id<MsgListObserver> observer in observers_) [observer listChanged:list withAccount:self];
}

- (void)addMsgObserver:(id<MsgObserver>)observer {
	
	[self.messageObservers addObject:observer];
}

- (void)removeMsgObserver:(id<MsgObserver>)observer {
	[self.messageObservers removeObject:observer];
}

- (void)callMsgObservers:(ISDSMessage*)msg {
	NSArray *observers_ = [NSArray arrayWithArray:self.messageObservers];	
	for (id<MsgObserver> observer in observers_) [observer messageChanged:msg withAccount:self];
}

@end
