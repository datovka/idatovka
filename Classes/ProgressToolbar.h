/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ProgressToolbar.h
//  ISDS
//
//  Created by Petr Hruška on 1/27/11.
//

#import <Foundation/Foundation.h>

#define PROGRESSBAR_THRESHOLD 0.2

@interface ProgressToolbar : NSObject {

	UILabel* label;
	UIProgressView* bar;
	NSArray* items;
}

@property (nonatomic, retain) UILabel* label;
@property (nonatomic, retain) UIProgressView* bar;
@property (nonatomic, retain) NSArray* items;
@property (nonatomic, retain) UIActivityIndicatorView* activityIndicator;
@property (nonatomic, retain) UIView* progressView;


- (id)initWithText:(NSString*)text andFrame:(CGRect)frame;
- (id)initWithText:(NSString*)text;
- (void)willShowInSplitView;
- (void)willHideInSplitView;
- (void)setFrame:(CGRect)frame;



@end
