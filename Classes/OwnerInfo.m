/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  OwnerInfo.m
//  ISDS
//
//  Created by Petr Hruška on 7/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "OwnerInfo.h"
#import "GetOwnerInfoFromLogin.h"
#import "AccountCache.h"

@implementation OwnerInfo

@synthesize firstName;
@synthesize lastName;
@synthesize firmName;


- (void)callObservers {
	
	if ([self.parent isKindOfClass:[AccountCache class]]) {
		AccountCache* ac = (AccountCache*)self.parent;
		[ac callAccountObservers:ISDSAccountChangeReasonBoxName];
	}
}

- (BOOL)writeToFile:(NSDictionary*)plist {
	[self callObservers];
	return [super writeToFile:plist];
}


- (NSDictionary*)plist {
	
	NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:3];
	if (dict == nil) return nil;
	if (self.firstName) [dict setObject:self.firstName forKey:@"firstName"];
	if (self.lastName) [dict setObject:self.lastName forKey:@"lastName"];
	if (self.firmName) [dict setObject:self.firmName forKey:@"firmName"];
	
	return dict;
}

- (BOOL)updateFromMethod:(GetOwnerInfoFromLogin*)method {

	// sanity check
	if (!method.firstName && !method.lastName && !method.firmName) return FALSE;
	
	// setup changed
	BOOL changed = NO;
	if (![self.firstName isEqualToString:method.firstName]) {
		self.firstName = method.firstName;
		changed = YES;
	}
		
	if (![self.lastName isEqualToString:method.lastName]) {	
		self.lastName = method.lastName;
		changed = YES;
	}
	
	if (![self.firmName isEqualToString:method.firmName]) {
		self.firmName = method.firmName;
		changed = YES;
	}
	
	if (changed) {
		// try write new data
		NSDictionary* propList = [self plist];
		if (propList) [self writeToFile:propList];
	}
	
	return TRUE;
}


- (void)dealloc {
	
	[firstName release];
	[lastName release];
	[firmName release];
	
	[super dealloc];
}

- (id) initWithParent:(ISDSObject*)parent_ andMethod:(GetOwnerInfoFromLogin*)method {
	if ((self = [super initWithParent:parent_])) {
		if (![self updateFromMethod:method]) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (id) initFromFileWithParent:(ISDSObject*)parent_ {
	
	if ((self = [super initWithParent:parent_])) {
		if (![self updateFromFile]) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (BOOL)updateFromPlist:(NSDictionary*)plist {
	
	if (!plist) return FALSE;
	NSString* first = (NSString*)[plist objectForKey:@"firstName"];
	NSString* last = (NSString*)[plist objectForKey:@"lastName"];
	NSString* firm = (NSString*)[plist objectForKey:@"firmName"];
		
	if (!first && !last && !firm) return FALSE;
	
	self.firstName = first;
	self.lastName = last;
	self.firmName = firm;
		
	return TRUE;
}

- (NSString*)dir {
	return @""; // leaf object
}

- (NSString*)filename {
	return @"OwnerInfo";
}

- (void)clean {
	[self removeFileAndDir];
	self.firstName = nil;
	self.lastName = nil;
	self.firmName = nil;
}

@end
