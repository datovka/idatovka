/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  OtpStatus.h
//  ISDS
//
//  Created by Petr Hruška on 7/6/11.
//

#import <Foundation/Foundation.h>
#import "ISDSAccount.h"

enum OtpStatusType {
    OtpStatusShouldSendSms = 1,
    OtpStatusRequestingSms = 2,
    OtpStatusEnterToken = 3,
    OtpStatusRequestingCookie = 4,
    OtpStatusHasCookie = 5,
};

@class OtpStatus;

@protocol OtpStatusDelegate <NSObject>

- (void)cookieExpiredForStatus:(OtpStatus*)status;

@end

@class OtpTask;

@interface OtpStatus : NSObject {
    
    enum OtpStatusType _status;
}

@property (nonatomic, retain) ISDSAccount* account;
@property (nonatomic) enum OtpStatusType status;
@property (nonatomic, retain) NSString* token;
@property (nonatomic, retain) OtpTask* task;
@property (nonatomic, retain) NSString* errorDescription;
@property (nonatomic, assign) NSTimer* timer;
@property (nonatomic, assign) id<OtpStatusDelegate> delegate;


/* 
 iDatovka upozorni uzivatele, pokud klikne na tlacitko hotovo a nezadal 
 vsechny tokeny. Je-li shouldHaveCookie FALSE, tak se kvuli tomuto uctu
 iDatovka nepta.
 */
@property (nonatomic, assign) BOOL shouldHaveCookie;

- (id)initWithAccount:(ISDSAccount*)account;
- (BOOL)tokenReady;
- (void)stopTimer;

@end
