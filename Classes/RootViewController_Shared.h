/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  RootViewController_Shared.h
//  ISDS
//
//  Created by Petr Hruška on 2/28/11.
//

#import <Foundation/Foundation.h>
#import "DataboxCell.h"
#import "Accounts.h"
#import "RequestManager.h"
#import "ProgressToolbar.h"
#import "TokenViewController.h"


@protocol ContentProvider

- (void)showPopoverButton:(UIBarButtonItem*)button;
- (void)hidePopoverButton;

@end

@interface RootViewController_Shared : UITableViewController<ISDSAccountObserver, RequestManagerObserver, MsgListObserver, AccountsObserver, TokenViewControllerDelegate> {

	DataboxCell* cell;
	UIBarButtonItem* configButton;
	UIBarButtonItem* refreshButton;
	UILabel* lastUpdate;
	ProgressToolbar* progressToolbar;
	NSArray* idleToolbarItems;
	NSUInteger pendingTasks;
	NSArray* observedClasses;
	UITableView* boxesTableView;
}

@property (nonatomic, retain) IBOutlet DataboxCell* cell;
@property (nonatomic, retain) IBOutlet UIBarButtonItem* configButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem* refreshButton;
@property (nonatomic, retain) IBOutlet UILabel* lastUpdate;
@property (nonatomic, retain) ProgressToolbar* progressToolbar;
@property (nonatomic, retain) NSArray* idleToolbarItems;
@property (nonatomic, assign) NSUInteger pendingTasks;
@property (nonatomic, retain) NSArray* observedClasses;
@property (nonatomic, retain) IBOutlet UITableView* boxesTableView;


- (DataboxCell*)getDataboxCell:(UITableView *)tableView;

- (IBAction) refresh:(id)sender;
- (void)setLastUpdate;
- (void)createProgressToolbarItems;
- (void)setupCookiesAndScheduleUpdate;

@end
