/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  PinEditViewController.m
//  ISDS
//
//  Created by Petr Hruška on 10/18/10.
//

#import "PinEditViewController.h"
#import "ISDSDefaults.h"
#import "ISDSAppDelegate_iPad.h"


@implementation PinEditViewController

@synthesize hiddenTextField;
@synthesize captionLabel;
@synthesize adviceLabel;
@synthesize delegate;
@synthesize keyboardVisible = _keyboardVisible;

- (void)viewDidLoad
{
    self.captionLabel.text = self.caption;
}

- (void)viewDidUnload
{
    self.hiddenTextField = nil;
    self.captionLabel = nil;
    self.adviceLabel = nil;
    [super viewDidUnload];
}

- (NSString*)caption {
	return _caption;
}

- (void)setCaption:(NSString*)value {
    if (_caption != value) {
        self.captionLabel.text = value;
        [_caption release];
        _caption = [value retain];
    }
}

- (NSString*)advice {
	return self.adviceLabel.text;
}

- (void)setAdvice:(NSString*)value {
	self.adviceLabel.text = value;
}

- (void)cleanPin {
	self.hiddenTextField.text = @"";
	for (NSUInteger i = 1; i <= 4; i++) {
		UITextField* textField = (UITextField*)[self.view viewWithTag:i];
		textField.text = @"";
	}
}

- (void)setFirstRespondent {
	[self.hiddenTextField becomeFirstResponder];
}

- (void)unsetFirstRespondent {
	[self.hiddenTextField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[self setFirstRespondent];
	[self cleanPin];
}

- (IBAction)textFieldValueChanged:(id)sender {

	NSUInteger len = [self.pin length];
	
	for (NSUInteger i = 1; i <= 4; i++) {
		UITextField* textField = (UITextField*)[self.view viewWithTag:i];
		textField.text = i <= len ? @"*" : @"";
	}
	
	if (len == 4) {
		[self.delegate lastDigitEntered:self];
	}
}


- (IBAction)textFieldEditingDidEnd:(id)sender {
    if (self.keyboardVisible) {
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showKeyboardOnTimer:) userInfo:nil repeats:NO];
    }
}

- (void)showKeyboard
{
    self.keyboardVisible = YES;
    [self setFirstRespondent];
}

- (void)showKeyboardOnTimer:(NSTimer*)timer
{
    [self showKeyboard];
}


- (void)hideKeyboard
{
    self.keyboardVisible = NO;
    [self unsetFirstRespondent];
}

- (NSString*)pin {
	
	return self.hiddenTextField.text;
}

+ (NSTimeInterval)pinTimeout
{
    ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
 
    NSInteger pinFailureCount = defaults.pinFailureCount;
    
    if (pinFailureCount > 30) pinFailureCount = 30;
    return (1 << pinFailureCount) / 4.0;
}

+ (void)incrementFailureCount
{
    ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
    defaults.pinFailureCount++;
}

+ (void)resetFailureCount
{
    ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
    
    defaults.pinFailureCount = 0;
}

+ (NSInteger)failureCount
{
    ISDSDefaults* defaults = [ISDSDefaults sharedDefaults];
    return defaults.pinFailureCount;
}

#pragma mark -
#pragma mark Object lifecycle

- (id)initWithNibName:(NSString *)nibName bundle:(NSBundle *)nibBundle {
	ISDSAppDelegate_Shared* appDelegate = (ISDSAppDelegate_Shared*)[[UIApplication sharedApplication] delegate];
	return [super initWithNibName:[appDelegate appendArchitecture:nibName] bundle:nibBundle];
}


- (void)dealloc {
	[hiddenTextField release];
	[captionLabel release];
	[adviceLabel release];
    [_caption release];
	
	[super dealloc];
}

@end
