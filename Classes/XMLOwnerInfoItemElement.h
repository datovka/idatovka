/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLOwnerInfoItemElement.h
//  ISDS
//
//  Created by Petr Hruška on 8/18/10.
//

#import <Foundation/Foundation.h>
#import "XMLAccumulatorElement.h"


@protocol XMLOwnerInfoItemElementDelegate

- (void)setFirstName:(NSString*)value;
- (void)setLastName:(NSString*)value;
- (void)setFirmName:(NSString*)value;

@end


@interface XMLOwnerInfoItemElement : XMLAccumulatorElement {

	NSString* elementName;
	id<XMLOwnerInfoItemElementDelegate> delegate;
	
}

@property (nonatomic, assign) id<XMLOwnerInfoItemElementDelegate> delegate;
@property (nonatomic, retain) NSString* elementName;

@end
