/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLEncodedContentElement.m
//  ISDS
//
//  Created by Petr Hruška on 8/11/10.
//

#import "XMLEncodedContentElement.h"
#import "XMLFileElement.h"
#import "Base64.h"

@implementation XMLEncodedContentElement

@synthesize parent;

- (BOOL) matchNamespace:(NSString*)namespaceURI element:(NSString*)elementName andAttributes:(NSDictionary*)attributes {
	return
	[namespaceURI isEqualToString: @"http://isds.czechpoint.cz/v20"] &&
	[elementName isEqualToString: @"dmEncodedContent"];
}

- (BOOL)canPush:(XMLElement*)_parent {
	if ([super canPush:_parent] && [_parent isKindOfClass:[XMLFileElement class]]) {
		self.parent = (XMLFileElement*)_parent;
		return YES;
	}
	return NO;
}

- (void)pop {
	self.parent.content = [Base64 decode:self.accumulator];
	[super pop];
}

@end
