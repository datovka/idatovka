/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  GetListOfMessages.m
//  ISDS
//
//  Created by Petr Hruška on 8/26/10.
//

#import "GetListOfMessages.h"
#import "MessageDownload.h"
#import "ISDSAccount.h"

#import "XMLSoapEnvelopeElement.h"
#import "XMLBodyElement.h"
#import "XMLDmRecordsElement.h"
#import "XMLDmRecordElement.h"
#import "XMLDmStatusElement.h"
#import "XMLDmStatusCodeElement.h"
#import "XMLDmStatusMessageElement.h"
#import "XMLMessageItemElement.h"
#import "XMLAcceptanceTimeElement.h"


@implementation GetListOfMessages

@synthesize list;
@synthesize envelope;

- (void)dealloc {
	[list release];
	[super dealloc];
}

- (id)init {
	
	self.list = [NSMutableArray array];
	if (!self.list) {
		[self dealloc];
		return nil;
	}
	
	const NSUInteger elementCount = 8;
	
	NSMutableArray* elems = [NSMutableArray arrayWithCapacity:elementCount];
	if (!elems) {
		[self dealloc];
		return nil;
	}
	
	@try {
		
		[elems addObject:[[XMLSoapEnvelopeElement alloc] init]];
		[elems addObject:[[XMLBodyElement alloc] init]];
		[elems addObject:[[XMLDmRecordsElement alloc] init]];
		[elems addObject:[[XMLDmStatusElement alloc] init]];
		
		XMLDmRecordElement* dmRecordElement = [[XMLDmRecordElement alloc] init];
		dmRecordElement.delegate = self;
		[elems addObject:dmRecordElement];

		
		XMLMessageItemElement* messageItemElement = [[XMLMessageItemElement alloc] init];
		messageItemElement.delegate = self;
		[elems addObject:messageItemElement];

		XMLAcceptanceTimeElement* acceptanceTimeElement = [[XMLAcceptanceTimeElement alloc] init];
		acceptanceTimeElement.delegate = self;
		[elems addObject:acceptanceTimeElement];
		
		XMLDmStatusCodeElement* statusCodeElement = [[XMLDmStatusCodeElement alloc] init];
		statusCodeElement.delegate = self;
		[elems addObject:statusCodeElement];
		 
		XMLDmStatusMessageElement* statusMessageElement = [[XMLDmStatusMessageElement alloc] init];
		statusMessageElement.delegate = self;
		[elems addObject:statusMessageElement];
	}
	
	@catch (NSException* exception) {
		//   STRANGE COMPILE ERROR!	@catch (NSInvalidArgumentException* exception) {
		/* argument of addObject was probably nil */
		[self dealloc];
		return nil;
	}
	
	self = [super initWithCapacity:5 andElements:elems];
	if (self == nil) {
		return self;
	}
	return self;
}

- (void)newEnvelope {
	self.envelope = [[EnvelopeData alloc] init];
	if (!self.envelope) self.errorDescription = NSLocalizedString(@"Not enough memory", @"Not enough memory");
}

- (void)envelopeDone {
	if (self.envelope) {
		self.envelope.acceptanceTime = parseAcceptanceTime(self.envelope.rawAcceptanceTime);
		[self.list addObject:self.envelope];
	}
}


- (NSString*)path {
	return  @"/DS/dx";
}

- (void)parse:(NSData*)data {
	
	[super parse:data];
}

- (NSData*) soapRequest {
	return nil;
}

- (NSString*)alertTitle:(ISDSAccount*)account {
	NSString* r;
	if (account) {
		NSString* fmt = NSLocalizedString(@"Error while downloading messages from account %@.", @"Alert title with box name");
		r = [NSString stringWithFormat:fmt, [account boxName]];
	} else 
		r = NSLocalizedString(@"Error while downloading messages", @"Alert title");
	
	return r;
}

#pragma mark -
#pragma XMLMessageItemElementDelegate methods

- (void)setDmId:(NSString*)value {
	self.envelope.msgId = value;
}

- (void)setSender:(NSString*)value {
	self.envelope.sender = value;
}

- (void)setIdSender:(NSString*)value {
}

- (void)setSenderAddress:(NSString*)value {
	self.envelope.senderAddress = value;
}

- (void)setRecipient:(NSString*)value {
	self.envelope.recipient = value;
}

- (void)setIdRecipient:(NSString*)value {
}

- (void)setRecipientAddress:(NSString*)value {
	self.envelope.recipientAddress = value;
}

- (void)setAnnotation:(NSString*)value {
	self.envelope.annotation = value;
}

- (void)setRawAcceptanceTime:(NSString*)value {
	self.envelope.rawAcceptanceTime = value;
}




@end
