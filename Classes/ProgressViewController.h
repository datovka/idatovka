/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ProgressViewController.h
//  ISDS
//
//  Created by Petr Hruška on 8/10/11.
//

#import <UIKit/UIKit.h>
#import "RequestManager.h"

enum ProgressDisplayContext {
    ProgressDisplayContextDefault = 0,
    ProgressDisplayContextPopover = 1,
    ProgressDisplayContext_iPad = 2,
    ProgressDisplayContext_iPhone = 3,
    ProgressDisplayContextAuto = 4,
};

@interface ProgressViewController : UIViewController<RequestManagerObserver> {

    float progress_;
    NSString* currentJob_;
    NSUInteger errorCount_;
    enum ProgressDisplayContext _context;
}

@property (nonatomic, assign) float progress;
@property (nonatomic, retain) NSString* currentJob;
@property (nonatomic, assign) NSUInteger errorCount;

@property (nonatomic, retain) IBOutlet UIProgressView* progressView;
@property (nonatomic, retain) IBOutlet UILabel* currentJobLabel;
@property (nonatomic, retain) IBOutlet UILabel* errorCountLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView* activityIndicator;

- (void)registerObserver;
- (void)unregisterObserver;
- (void)setProgressViewWidth:(CGFloat)width;
- (void)setDisplayContext:(enum ProgressDisplayContext)context;

@end
