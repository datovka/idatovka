/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  SmsTask.m
//  ISDS
//
//  Created by Petr Hruška on 8/8/11.
//

#import "SmsTask.h"


@implementation SmsTask

- (NSString*)urlString
{
    NSString* host = [self host];
    NSString* r = [NSString stringWithFormat:@"https://%@/as/processLogin?type=totp&sendSms=true&uri=https://%@/apps/DS/dx", host, host];
    
    return r;
}

- (NSString*)otpPassword
{
    return [self password];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)_response {
	NSHTTPURLResponse* response = (NSHTTPURLResponse*)_response;
    NSLog(@"SmsTask: didReceiveResponse: response code = %ld", (long)[response statusCode]);
    if ([response statusCode] != 302) {
        self.errorDescription = [self base64Message:response withDefault:NSLocalizedString(@"Error while processing server response", nil)];
    }
}

- (NSString*)taskDescription
{
    return NSLocalizedString(@"Requesting sms message", nil);
}


@end
