/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Estimate.h
//  ISDS
//
//  Created by Petr Hruška on 1/21/11.
//

#import <Foundation/Foundation.h>

@class ISDSTask;

@interface Estimate : NSObject {

}



@property (nonatomic, retain) NSString* task;
@property (nonatomic, assign) NSTimeInterval delay;

- (id)initWithPlist:(NSDictionary*)dict;
- (id)initWithTask:(ISDSTask*)task;
- (BOOL)matchTask:(ISDSTask*)task;
- (void)updateDelay:(NSTimeInterval)delay;
- (NSDictionary*)plist;

+ (NSTimeInterval)defaultDelayForTask:(ISDSTask*)task;

@end
