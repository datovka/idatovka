/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  RootViewController.h
//  ISDS
//
//  Created by Petr Hruška on 4/14/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "RootViewController_Shared.h"

@interface RootViewController_iPhone : RootViewController_Shared {

}

- (IBAction) config:(id)sender;


@end
