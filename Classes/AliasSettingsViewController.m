/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AliasSettingsViewController.m
//  ISDS
//
//  Created by Petr Hruška on 11/29/10.
//

#define AUTO_INDEX   0
#define ALIAS_INDEX  1

#import "AliasSettingsViewController.h"


@implementation AliasSettingsViewController

@synthesize account;

@synthesize automatically;
@synthesize aliasCell;
@synthesize aliasLabel;
@synthesize alias;

- (void)viewDidLoad {
	self.title = NSLocalizedString(@"Account name", @"AliasSettings title");
}

- (void)viewWillAppear:(BOOL)animated {
	self.automatically.delegate = self;
	
	BOOL useAlias = [self.account useAlias];
	
	self.automatically.on = !useAlias;
	self.alias.text = [self.account alias];
	
	self.aliasLabel.enabled = useAlias;
	self.alias.enabled = useAlias;

}


- (void)viewWillDisappear:(BOOL)animated {

	self.account.alias = self.alias.text;
	self.account.useAlias = !self.automatically.on;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {	
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {    
	
	switch (indexPath.row) {
		case AUTO_INDEX:
			return self.automatically;
		case ALIAS_INDEX:
			return self.aliasCell;
	}
	return nil;
}


- (void)dealloc {
	
	[account release];
	
	[automatically release];
	[aliasCell release];
	[aliasLabel release];
	[alias release];
	
    [super dealloc];
}

- (void)switchCell:(SwitchCell*)cell switchedTo:(BOOL)value {
	
	self.aliasLabel.enabled = !value;
	self.alias.enabled = !value;
}


#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
	return NO;
}

@end
