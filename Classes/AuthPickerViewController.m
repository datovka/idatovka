/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AuthPickerViewController.m
//  ISDS
//
//  Created by Petr Hruška on 7/4/11.
//

#import "AuthPickerViewController.h"

#define AUTH_PASS_INDEX     0
#define AUTH_HOTP_INDEX     1
#define AUTH_TOTP_INDEX     2

@implementation AuthPickerViewController

@synthesize account=_account;

- (void)dealloc
{
    [_account release];
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = NSLocalizedString(@"Authorization method", nil);
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AuthPickerCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        [cell autorelease];
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        
    switch (indexPath.row) {
        case AUTH_PASS_INDEX:
            cell.textLabel.text = NSLocalizedString(@"Password only", nil);
            if (self.account.authType == ISDSAccountAuthPass)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
        case AUTH_HOTP_INDEX:
            cell.textLabel.text = NSLocalizedString(@"Safety key", nil);
            if (self.account.authType == ISDSAccountAuthHotp)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
        case AUTH_TOTP_INDEX:
            cell.textLabel.text = NSLocalizedString(@"SMS code", nil);
            if (self.account.authType == ISDSAccountAuthTotp)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
    }
	
	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case AUTH_PASS_INDEX:
            self.account.authType = ISDSAccountAuthPass;
            break;
        case AUTH_HOTP_INDEX:
            self.account.authType = ISDSAccountAuthHotp;
            break;
        case AUTH_TOTP_INDEX:
            self.account.authType = ISDSAccountAuthTotp;
            break;
    }
    [tableView reloadData];    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
