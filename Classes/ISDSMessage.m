/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Message.m
//  ISDS
//
//  Created by Petr Hruška on 5/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ISDSMessage.h"
#import "Attachment.h"
#import "MsgList.h"
#import "SentMessageDownload.h"
#import "ReceivedMessageDownload.h"

@implementation ISDSMessage

@synthesize msgId;
@synthesize sender;
@synthesize senderAddress;
@synthesize recipient;
@synthesize recipientAddress;
@synthesize annotation;
@synthesize acceptanceTime;

@synthesize attachments;

@synthesize read;

- (void)callObservers {
	MsgList* parent_ = (MsgList*)self.parent;
	[parent_ callMsgObservers:self];
}

- (NSDictionary*) plist {

	NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:7];
	if (dict == nil) return nil;
	NSNumber* objRead = [NSNumber numberWithBool:self.read];
	if (objRead == nil) return nil;

	if (self.sender) [dict setObject:self.sender forKey:@"sender"];
	if (self.senderAddress) [dict setObject:self.senderAddress forKey:@"senderAddress"];
	if (self.recipient) [dict setObject:self.recipient forKey:@"recipient"];
	if (self.recipientAddress) [dict setObject:self.recipientAddress forKey:@"recipientAddress"];
	if (self.annotation) [dict setObject:self.annotation forKey:@"annotation"];
	if (self.acceptanceTime) [dict setObject:self.acceptanceTime forKey:@"acceptanceTime"];
	[dict setObject:objRead forKey:@"read"];
	
	return dict;
}

- (BOOL)loadAttachments {
	if (self.attachments == (Attachments*)[NSNull null]) 
		self.attachments = [[Attachments alloc] initFromFileWithParent:self];

	return self.attachments != nil;
}

- (BOOL)incoming {
	MsgList* list = (MsgList*)self.parent;
	return list.incoming;
}


- (void)markRead {
	
	if (self.read) return;
	
	MsgList* list = (MsgList*)parent;
	[list decrementUnread];
	self.read = TRUE;
	NSDictionary* propertyList = [self plist];
	[self writeToFile:propertyList];
}

- (void)dealloc {
	
	[ msgId release ];
	[ sender release ];
	[ senderAddress release ];
	[ recipient release ];
	[ recipientAddress release ];
	[ annotation release ];
	[ acceptanceTime release ];
	[ attachments release ];
	
	[ super dealloc ];
}

- (id)initFromFileWithParent:(ISDSObject*)parent_ andId:(NSString*)msgId_ {

	if ((self = [super initWithParent:parent_])) {
		self.msgId = msgId_;
		self.attachments = (Attachments*)[NSNull null];
		
		if (![self updateFromFile]) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (id)initWithParent:(ISDSObject*)parent_ fromEnvelope:(EnvelopeData*)envelope {
	
	if ((self = [super initWithParent:parent_])) {
		
		self.msgId = envelope.msgId;
		self.attachments = (Attachments*)[NSNull null];
		
		if (![self updateFromEnvelope:envelope]) {
			[self dealloc];
			return nil;
		}
	}
	return self;
}


- (NSComparisonResult) compareAcceptanceTime:(ISDSMessage*) msg
{
	return [ msg.acceptanceTime compare: self.acceptanceTime ];
}


- (BOOL)updateFromEnvelope:(EnvelopeData*)envelope
{
	if (![self.msgId isEqualToString:envelope.msgId]) return FALSE;
	self.sender = envelope.sender;
	self.senderAddress = envelope.senderAddress;
	self.recipient = envelope.recipient;
	self.recipientAddress = envelope.recipientAddress;
	self.annotation = envelope.annotation;
	self.acceptanceTime = envelope.acceptanceTime;

	NSDictionary* propertyList = [self plist];
	[self writeToFile:propertyList];
	[self callObservers];

	return TRUE;
}

- (BOOL)updateFromSentMessage:(SentMessageDownload*)message {

	// message id should be same
	if (![self.msgId isEqualToString:message.dmId]) return FALSE;
	
	// sender
	if (message.sender) self.sender = message.sender;
	if (message.senderAddress) self.senderAddress = message.senderAddress;
	
	// recipient
	if (message.recipient) self.recipient = message.recipient;
	if (message.recipientAddress) self.recipientAddress = message.recipientAddress;
	
	if (message.annotation) self.annotation = message.annotation;
	if (message.acceptanceTime) self.acceptanceTime = message.acceptanceTime;

	NSDictionary* propertyList = [self plist];
	[self writeToFile:propertyList];
	
	// setup attachments
	if ([self loadAttachments]) {
		if (![self.attachments update:message.attachments]) return FALSE;
	} else {
		self.attachments = [[Attachments alloc] initWithParent:self fromAttachments:message.attachments];
	}
	[self callObservers];

	return TRUE;
}

- (BOOL)updateFromReceivedMessage:(ReceivedMessageDownload*)message {

	// message id should be same
	if (![self.msgId isEqualToString:message.dmId]) return FALSE;
	
	// sender
	if (message.sender) self.sender = message.sender;
	if (message.senderAddress) self.senderAddress = message.senderAddress;
	
	// recipient
	if (message.recipient) self.recipient = message.recipient;
	if (message.recipientAddress) self.recipientAddress = message.recipientAddress;
	
	if (message.annotation) self.annotation = message.annotation;
	if (message.acceptanceTime) self.acceptanceTime = message.acceptanceTime;
	
	NSDictionary* propertyList = [self plist];
	[self writeToFile:propertyList];
	
	// setup attachments
	if ([self loadAttachments]) {
		if (![self.attachments update:message.attachments]) return FALSE;
	} else {
		self.attachments = [[Attachments alloc] initWithParent:self fromAttachments:message.attachments];
	}
	[self callObservers];
	
	return TRUE;
}

- (NSString*)friendlyShortDateTime
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
	NSString *tmp = [dateFormatter stringFromDate:self.acceptanceTime]; 
	
	[dateFormatter release];
	
	return tmp;
}

- (NSString*)friendlyDateTime
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];

    NSString *tmp = [dateFormatter stringFromDate:self.acceptanceTime]; 
	
	[dateFormatter release];
	
	return tmp;
}

- (BOOL)updateFromPlist:(NSDictionary*)plist {
	
	if (!plist) return FALSE;
	//annotation attribute is mandatory for each message
	NSString* annotation_ = [ plist objectForKey: @"annotation" ];
	self.annotation = annotation_;
	self.sender = [plist objectForKey: @"sender"];
	self.senderAddress = [plist objectForKey: @"senderAddress"];
	self.recipient = [plist objectForKey: @"recipient"];
	self.recipientAddress = [ plist objectForKey: @"recipientAddress" ];
	self.annotation = [ plist objectForKey: @"annotation" ];
	self.acceptanceTime = [ plist objectForKey: @"acceptanceTime" ];
	NSNumber* objRead = [ plist objectForKey: @"read" ];
	self.read = [objRead boolValue];
	
	return TRUE;
}

- (NSString*)dir {
	return [NSString stringWithFormat:@"Message-%@/", self.msgId];
}

- (NSString*)filename {
	return @"MessageData"; // unambiguity is ensured by dir name
}

- (NSUInteger)attachmentCount {
	
	BOOL hasAttachments = [self loadAttachments];
	if (!hasAttachments)
		// each valid message has positive number of attachments, zero means unknown count
		return 0;
	else
		return [self.attachments count];
}


@end







