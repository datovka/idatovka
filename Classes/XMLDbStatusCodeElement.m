/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLDbStatusCodeElement.m
//  ISDS
//
//  Created by Petr Hruška on 8/18/10.
//

#import "XMLDbStatusCodeElement.h"
#import "XMLDbStatusElement.h"

@implementation XMLDbStatusCodeElement

@synthesize delegate;

- (BOOL) matchNamespace:(NSString*)namespaceURI element:(NSString*)elementName andAttributes:(NSDictionary*)attributes {
	return
	[namespaceURI isEqualToString: @"http://isds.czechpoint.cz/v20"] &&
	[elementName isEqualToString: @"dbStatusCode"];
}

- (void)pop {
	[self.delegate setStatusCode:self.accumulator];
	[super pop];
}

@end
