/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Attachments.m
//  ISDS
//
//  Created by Petr Hruška on 9/1/10.
//

#import "Attachments.h"


@implementation Attachments

@synthesize list;


- (id)initFromFileWithParent:(ISDSObject*)parent_ {
	
	if ((self = [super initWithParent:parent_])) {
		if (![self updateFromFile]) {
			[self dealloc];
			return nil;
		}
	}
	return self;	
}

- (id)initWithParent:(ISDSObject*)parent_ fromAttachments:(NSMutableArray*)attachments {

	if ((self = [super initWithParent:parent_])) {
		if (![self update:attachments]) {
			[self dealloc];
			return nil;
		}
	}
	return self;	
}


- (NSDictionary*)plist {
	if (!self.list) return nil;
	
	NSMutableArray* r = [NSMutableArray arrayWithCapacity:[self.list count]];
	if (!r) return nil;
	
	for (Attachment* a in self.list) {
		NSDictionary* d = [NSDictionary dictionaryWithObjectsAndKeys:
		 a.name, @"name",
		 a.data, @"data",
		 nil];
		if (!d) return nil;
		[r addObject: d];
	}
	
	return [NSDictionary dictionaryWithObject: r forKey:@"list"];
}

- (BOOL)update:(NSMutableArray*)attachments {
	
	const NSUInteger count = [attachments count];
	NSMutableArray* newList = [NSMutableArray arrayWithCapacity:count];
	if (!newList) return FALSE;
	for (NSUInteger i = 0; i < count; i++) {
		NSArray* p = [attachments objectAtIndex:i];
		NSString* name = [p objectAtIndex:0];
		if (!name) return FALSE;
		NSData* content = [p objectAtIndex:1];
		if (!content) return FALSE;
		Attachment* a = [[Attachment alloc] initWithName:name andContent:content];
		if (!a) return FALSE;
		[newList addObject:a];
		[a release];
	}
    
	self.list = newList;

	NSDictionary* pl = [self plist];
	[self writeToFile:pl];

	return TRUE;
}

- (Attachment*)attachmentAtIndex:(NSUInteger)index {
	return [self.list objectAtIndex:index];
}

- (NSUInteger)count {
	return [self.list count];
}

// initialize from property list
- (BOOL)updateFromPlist:(NSDictionary*)plist {
	
	if (!plist) return FALSE;
	
	NSArray* attachments = [plist objectForKey:@"list"];
	if (!attachments) return FALSE;
	
	NSMutableArray* newList = [NSMutableArray arrayWithCapacity:[attachments count]];
	if (!newList) return FALSE;
	
	for (NSDictionary* d in attachments) {
		NSString* name = [d objectForKey:@"name"];
		if (!name) return FALSE;
		NSData* content = [d objectForKey:@"data"];
		if (!content) return FALSE;
		
		Attachment* a = [[Attachment alloc] initWithName:name andContent:content];
		[newList addObject:a];
		[a release];
	}
	self.list = newList;
	return TRUE;
}

- (NSString*)dir {
	return @"";
}

// file with data of the object
- (NSString*)filename {
	return @"Attachments";
}

- (void)clean {
	self.list = nil;
	[self removeFileAndDir];
}

@end
