/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  Estimate.m
//  ISDS
//
//  Created by Petr Hruška on 1/21/11.
//

#import "Estimate.h"


#define DEFAULT_DELAY 1.0
#define ESTIMATE_RIGIDITY 0.3


@implementation Estimate

@synthesize task=_task;
@synthesize delay=_delay;

- (void)dealloc {
	[_task release];
	[super dealloc];
}

- (id)initWithPlist:(NSDictionary*)dict
{
	if ((self = [super init])) {
		
		self.task = [dict objectForKey:@"task"];
        if (!self.task) {
            [self dealloc];
            return nil;
        }
		NSNumber *d = [dict objectForKey:@"delay"];
        if (!d) {
            [self dealloc];
            return nil;
        }
		self.delay = [d floatValue];
        
                         
	}
	return self;
}


- (id)initWithTask:(ISDSTask*)task
{
	if ((self = [super init])) {
		
		self.task = NSStringFromClass([task class]);
	}
	return self;
}

- (BOOL)matchTask:(ISDSTask*)task
{	
	NSString* className = NSStringFromClass([task class]);
	return [className isEqualToString:self.task];
}

- (void)updateDelay:(NSTimeInterval)delay {
	self.delay = (1 - ESTIMATE_RIGIDITY) * delay + ESTIMATE_RIGIDITY * self.delay;
}

- (NSDictionary*)plist {
	NSMutableDictionary* dict = [NSMutableDictionary dictionaryWithCapacity:2];
	if (!dict) return nil;
	
	NSNumber* d = [NSNumber numberWithFloat:self.delay];
	if (!d) return nil;
	
	[dict setObject:self.task forKey:@"task"];
	[dict setObject:d forKey:@"delay"];
	
	return dict;
}


+ (NSDictionary*) loadDefaults {
	NSString *rootPath = [[NSBundle mainBundle ] bundlePath];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Estimates.plist"];
	
	NSData *data = [NSData dataWithContentsOfFile:plistPath];
	if (!data) return nil;
	NSString* error;
	
	return [ NSPropertyListSerialization
					  propertyListFromData: data
					  mutabilityOption:NSPropertyListImmutable
					  format: NULL
					  errorDescription: &error];
	
}

+ (NSTimeInterval)defaultDelayForTask:(ISDSTask*)task {

	static NSDictionary* plist = nil;
	
	NSString* className = NSStringFromClass([task class]);
	
	if (!plist) plist = [[Estimate loadDefaults] retain];
	if (!plist) return DEFAULT_DELAY;
	
	NSNumber *delay = [plist objectForKey:className];
	if (!delay) {
        NSLog(@"Can't find default delay for class '%@'", className);
        return DEFAULT_DELAY;
    }
	return [delay floatValue];
}


@end
