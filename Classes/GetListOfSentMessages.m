/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  GetListOfSentMessages.m
//  ISDS
//
//  Created by Petr Hruška on 8/19/10.
//

#import "GetListOfSentMessages.h"
#import "XMLGetListOfSentMessagesResponseElement.h"


@implementation GetListOfSentMessages

- (id)init {
	
	if ((self = [super init])) {
		
		@try {		
			[self.elements addObject:[[XMLGetListOfSentMessagesResponseElement alloc] init]];
		}
		
		@catch (NSException* exception) {
			//   STRANGE COMPILE ERROR!	@catch (NSInvalidArgumentException* exception) {
			/* argument of addObject was probably nil */
			[self dealloc];
			return nil;
		}
	}
	return self;
}

- (NSData*) soapRequest {
	NSString* pattern = [SoapMethod requestPattern];
	NSString* req = @"<ns1:GetListOfSentMessages><dmStatusFilter>-1</dmStatusFilter></ns1:GetListOfSentMessages>";
	
	NSString* s = [NSString stringWithFormat: pattern, req];
	NSData* d = [s dataUsingEncoding:NSUTF8StringEncoding];
	return d;
}

- (NSString*)path {
	return  @"/DS/dx";
}

@end
