/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  XMLMessage.h
//  ISDS
//
//  Created by Petr Hruška on 8/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLElement.h"

@interface XMLMessage : NSObject <NSXMLParserDelegate> {
	
	NSMutableArray* stack;
	NSMutableArray* elements;
	
	XMLElement* unknownElement;
	
	NSString* errorDescription;
}

@property (nonatomic, retain) NSMutableArray* stack;
@property (nonatomic, retain) NSMutableArray* elements;
@property (nonatomic, retain) NSString* errorDescription;

- (id)initWithCapacity:(NSUInteger)capacity andElements:(NSMutableArray*)elements;
- (void)parse:(NSData*)data;
- (XMLElement*)topElement;

@end
