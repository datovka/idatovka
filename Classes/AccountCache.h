/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AccountCache.h
//  ISDS
//
//  Created by Petr Hruška on 7/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OwnerInfo.h"
#import "ISDSObject.h"
#import "Estimates.h"
#import "ISDSAccount.h"

@class ISDSAccount;
@class GetListOfSentMessages;
@class GetListOfReceivedMessages;
@class ReceivedMessageDownload;
@class SignedSentMessageDownload;
@class MsgList;

@interface AccountCache : ISDSObject {

	ISDSAccount* account;

	NSDate* lastReceivedUpdate;

	OwnerInfo* ownerInfo;
	MsgList* sent;
	MsgList* received;
	Estimates* estimates;
}

@property (nonatomic, retain) ISDSAccount* account;

@property (nonatomic, retain) NSDate* lastReceivedUpdate;

@property (nonatomic, retain) OwnerInfo* ownerInfo;
@property (nonatomic, retain) MsgList *sent;
@property (nonatomic, retain) MsgList *received;
@property (nonatomic, retain) Estimates *estimates;

// soft data (cache = owner name + messages)
- (id)initFromFileWithAccount:(ISDSAccount*)account;
- (BOOL)updateOwnerInfo:(GetOwnerInfoFromLogin*)method;

- (BOOL)loadSent;
- (BOOL)loadReceived;
- (BOOL)loadOwnerInfo;

- (BOOL)updateSentMessageList:(GetListOfSentMessages*)method;
- (BOOL)updateReceivedMessageList:(GetListOfReceivedMessages*)method;

- (BOOL)updateEstimate:(ISDSTask*)task withDelay:(NSTimeInterval)delay;
- (NSTimeInterval)estimate:(ISDSTask*)task;
- (BOOL)updateFromReceivedMessage:(ReceivedMessageDownload*)method;
- (BOOL)updateFromSentMessage:(SignedSentMessageDownload*)method;

- (void)clean;

// return -1 on error
- (NSInteger)unreadSentCount;
- (NSInteger)unreadReceivedCount;

- (void)callAccountObservers:(ISDSAccountChangeReason)reasonFlags;
- (void)callListObservers:(MsgList*)list;
- (void)callMsgObservers:(ISDSMessage*)msg;


@end
