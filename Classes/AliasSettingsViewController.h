/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AliasSettingsViewController.h
//  ISDS
//
//  Created by Petr Hruška on 11/29/10.
//

#import <UIKit/UIKit.h>
#import "ISDSAccount.h"
#import "SwitchCell.h"


@interface AliasSettingsViewController : UITableViewController<SwitchCellDelegate, UITextFieldDelegate> {

	ISDSAccount* account;

	SwitchCell* automatically;
	
	UITableViewCell* aliasCell;
	UILabel* aliasLabel;
	UITextField* alias;
}

@property (nonatomic, retain) ISDSAccount* account;

@property (nonatomic, retain) IBOutlet SwitchCell* automatically;

@property (nonatomic, retain) IBOutlet UITableViewCell* aliasCell;
@property (nonatomic, retain) IBOutlet UILabel* aliasLabel;
@property (nonatomic, retain) IBOutlet UITextField* alias;

@end
