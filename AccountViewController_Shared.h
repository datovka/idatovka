/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AccountViewController_Shared.h
//  ISDS
//
//  Created by Petr Hruška on 4/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISDSAccount.h"
#import "SwitchCell.h"
#import "TokenViewController.h"

#define ACTIVE_INDEX        0
#define DBOXNAME_INDEX      1
#define PASSWORD_INDEX      2
#define OTP_INDEX           3
#define AUTOALIAS_INDEX     4


@interface AccountViewController_Shared : UIViewController <UITextFieldDelegate, UIActionSheetDelegate, UITableViewDataSource, UITableViewDelegate, SwitchCellDelegate, TokenViewControllerDelegate> {

	SwitchCell* activeCell;
	
	UITableViewCell* databoxCell;
	UITextField* databoxTextField;

	UITableViewCell* passwordCell;
	UITextField* passwordTextField;
	
	UITableViewCell* aliasSettingsCell;

	ISDSAccount* account;

	UIActionSheet* removeActionSheet;
	UIActionSheet* existsActionSheet;
	UIActionSheet* emptyActionSheet;
	
	ISDSAccount* existingAccount;

	BOOL wantsDelete;
	
	UIButton* removeButton;
}

@property (nonatomic, retain) IBOutlet SwitchCell *activeCell;

@property (nonatomic, retain) IBOutlet UITableViewCell *databoxCell;
@property (nonatomic, retain) IBOutlet UITextField *databoxTextField;

@property (nonatomic, retain) IBOutlet UITableViewCell *passwordCell;
@property (nonatomic, retain) IBOutlet UITextField *passwordTextField;

@property (nonatomic, retain) IBOutlet UITableViewCell *aliasSettingsCell;

@property (nonatomic, retain) IBOutlet UITableViewCell *authTypeCell;
@property (nonatomic, retain) IBOutlet UILabel *authTypeLabel;

@property (nonatomic, retain) ISDSAccount *account;

@property (nonatomic, assign) BOOL wantsDelete;

@property (retain, nonatomic) IBOutlet UIBarButtonItem* doneButton;

@property (retain, nonatomic) UIActionSheet* deleteActionSheet;
@property (retain, nonatomic) UIActionSheet* existsActionSheet;
@property (retain, nonatomic) UIActionSheet* emptyActionSheet;

@property (retain, nonatomic) ISDSAccount* existingAccount;

@property (retain, nonatomic) IBOutlet UITableView* tableView;
@property (retain, nonatomic) IBOutlet UIButton* removeButton;

@property (assign, nonatomic) UIViewController* parent;


- (NSString*)databoxTitle; // abstract method

- (IBAction) deleteButtonPressed;
- (IBAction) doneButtonPressed;

- (void)shouldPopViewController;

- (void)mergeExisting;
- (void)updateAccount;
- (BOOL)getOwnerInfo;

@end