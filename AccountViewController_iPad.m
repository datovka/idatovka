/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  AccountViewController_iPad.m
//  ISDS
//
//  Created by Petr Hruška on 4/18/11.
//

#import "AccountViewController_iPad.h"

#define ALIAS_INDEX             (AUTOALIAS_INDEX + 1)
#define REMOVEBUTTON_OFFSET     (30 + 54)
#define CELL_HEIGHT             54

@implementation AccountViewController_iPad

@synthesize autoAliasCell=_autoAliasCell;
@synthesize aliasCell=_aliasCell;
@synthesize aliasTextField=_aliasTextField;


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{	
    return AUTOALIAS_INDEX + 1 + (self.account.useAlias ? 1 : 0);
}

- (void)setRemoveButtonPosition:(BOOL)animated
{
    CGRect frame = self.removeButton.frame;
    frame.origin.y = self.tableView.frame.origin.y + [self tableView:self.tableView numberOfRowsInSection:0] * CELL_HEIGHT + REMOVEBUTTON_OFFSET;
    if (animated)
        [UIView animateWithDuration:0.3 animations:^{ self.removeButton.frame = frame; }];
    else
        self.removeButton.frame = frame;
}

- (void)setupAliasCell
{
    BOOL visible = self.account.useAlias;
    [self.autoAliasCell setOn:!visible animated:TRUE];
    NSUInteger indexes[2];
    indexes[0] = 0;
    indexes[1] = ALIAS_INDEX;
    NSIndexPath* index = [NSIndexPath indexPathWithIndexes:indexes length:2];
    NSArray* aliasRow = [NSArray arrayWithObject:index];
    if (visible)
        [self.tableView insertRowsAtIndexPaths:aliasRow withRowAnimation:UITableViewRowAnimationTop];
    else
        [self.tableView deleteRowsAtIndexPaths:aliasRow withRowAnimation:UITableViewRowAnimationTop];
    
    [self setRemoveButtonPosition:YES];
}


- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{

    switch (indexPath.row) {
        case AUTOALIAS_INDEX:
            [tv deselectRowAtIndexPath:indexPath animated:YES];
            self.account.useAlias = !self.account.useAlias;
            [self setupAliasCell];
            return;
    }
    
    [super tableView:tv didSelectRowAtIndexPath:indexPath];
}


- (UITableViewCell *)tableView:(UITableView *)tableView_ cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
            
        case ALIAS_INDEX:
            return self.aliasCell;
        default:
            return [super tableView:tableView_ cellForRowAtIndexPath:indexPath];
    }
}


- (void)viewWillAppear:(BOOL)animated {
    
    self.autoAliasCell.on = !self.account.useAlias;
    self.aliasTextField.text = self.account.alias;
    [self setRemoveButtonPosition:YES];
    [super viewWillAppear:animated];
}


- (void)switchCell:(SwitchCell*)cell switchedTo:(BOOL)value
{
    if (cell == self.autoAliasCell) {

        if (self.account.useAlias != !value) {
            self.account.useAlias = !value;
            [self setupAliasCell];
        }
        
    } else {
        [super switchCell:cell switchedTo:value];
    }
}


- (IBAction)aliasEditingChanged:(id)sender
{
}


- (void)viewDidUnload 
{
    //self.autoAliasCell = nil, it's used to keep state
    self.aliasCell = nil;
    self.aliasTextField = nil;
}


- (void)dealloc
{
    [_autoAliasCell release];
    [_aliasCell release];
    [_aliasTextField release];
    [super dealloc];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return YES;
}

@end
