/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ISDSAppDelegate_iPhone.m
//  ISDS
//
//  Created by Petr Hruška on 2/17/11.
//

#import "ISDSAppDelegate_iPhone.h"
#import "PinViewController_iPhone.h"
#import <QuartzCore/QuartzCore.h>


@implementation ISDSAppDelegate_iPhone

@synthesize navigationController;

- (void)applicationDidFinishLaunching:(UIApplication *)application {    

    self.window.rootViewController = self.navigationController;
	[super applicationDidFinishLaunching:application];
}

- (NSString*)appendArchitecture:(NSString*)nibName {
	return [nibName stringByAppendingString:@"~iPhone"];
}

- (void)dealloc {
	[navigationController release];
	[super dealloc];
}

- (void)initPinViewController
{
    self.pinViewController = [[PinViewController_iPhone alloc] initWithNibName:@"PinViewController" bundle:nil];
    self.pinViewController.delegate = self;
}



@end
