/*  iDatovka - a free iOS application for accessing Datove schranky
 *  Copyright (C) 2010-2012  CZ.NIC, z.s.p.o. (http://www.nic.cz)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it either under the terms of the GNU General Public License version 3
 *  or the Apache License 2.0.
 * 
 *  Licensing of iDatovka is described in the file LICENSE in the top level
 *  directory of the source code.
 *
 */
//
//  ContentViewController.m
//  ISDS
//
//  Created by Petr Hruška on 5/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ContentViewController.h"


@implementation ContentViewController

@synthesize file;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	UIWebView* webView = (UIWebView *) self.view;
	self.title = self.file.name;
	
	NSURL *url = [ NSURL URLWithString: @"http://localhost"];
	[ webView loadData: self.file.data MIMEType:[self.file mimeType] textEncodingName:@"utf-8" baseURL: url];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return YES;
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)dealloc {
    [super dealloc];
}


@end
